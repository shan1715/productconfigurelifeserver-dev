const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
//var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var replaceall = require("replaceall");
//User Variable
const userEntityVal = require('./userEntity');
var database = require("./DatabaseProperty");
const mongoose = require('mongoose');// Added by Bhagavathy 26/08/2019 MongoDB



//GET the User information to database
router.get('/getuser', function (req, res, next) {
    console.log("Inside GET User Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET User Service : Connection Made");
    userEntityVal.collection.insertOne()
    userEntityVal.find(req.body).then(function (user) {
        mongoose.connection.close();
        // user[0].vDesc.
        var approve = [];
        approve = user[0].vDesc;

        if (user.length > 0)
            res.send(user[0].vDesc);
        else
            res.send(user);
        console.log("GET User Service : SuccessFully Inserted");
    }).catch(next);

});


//GET the User information to database
router.post('/UpdateUser', function (req, res, next) {
    console.log("Inside GET User Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET User Service : Connection Made");
    
    userEntityVal.find().then(function (user) {
        var userValues = [];
        userValues = user[0];
        if(user){
        for (var i = 0; i < userValues.vDesc.length; i++) {
                console.log(userValues.vDesc[i].username);
                if (userValues.vDesc[i].username == req.body.username) {
                    usernameValid = false;
                }


            }
            if (usernameValid) {

            userValues.vDesc.push(req.body);
               userEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: userValues.vDesc } }).then(function (userUpdate) {
            if (userUpdate.nModified == 1) {
                var userResult = [];
                userResult = userUpdate;
                res.send(userResult);
            }
       
        }).catch(next);


            }
        }
        
        
       


        // console.log("GET User Service : SuccessFully Inserted");
    }).catch(next);
// mongoose.connection.close();
});

//POST the USER information to database 
router.get('/createUser/:folderName', function (req, res, next) {
    console.log("Inside Post Life Stage PC 1 :Before Database connection");
    mongoose.connect(database.url);
    readDirFiles.read('./dist/assets/' + req.params.folderName, 'UTF-8', function (err, files) {
        if (err) {
            return console.dir(err);
        }

        userEntityVal.create(JSON.parse(files["USERVALU.json"])).then(function (aaaaa) {
            mongoose.connection.close();
            res.status(200).send(files["USERVALU.json"]);
            // res.send(aaaaa);
            // console.log("Post Life Stage PC 1: SuccessFully Inserted");
        }).catch(next);
        //  res.status(200).send(files["PROD_BENEFITS.json"]);


    });
    // var file = './dist/assets/productpublish/' + req.params.folderName + '/' + req.params.folderName + '.json';
    //     jsonfile.readFile(file, function (err, obj) {
    //     if (obj) {
    //         res.status(200).json(obj);
    //     } else {
    //         res.status(200).json(({ "output": "notok" }));
    //     }

    // })
    // console.log("Inside post Life Stage PC 1  :Connection Made");


});

//Create the User information to database
router.post('/CreateUser', function (req, res, next) {
    console.log("Inside createUser Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside createUser Service : Connection Made");
    
    userEntityVal.find().then(function (user) {
        var userValues = [];
        userValues = user[0];
        userValues.vDesc.push(req.body);
        
        userEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: userValues.vDesc } }).then(function (userUpdate) {
            if (userUpdate.nModified == 1) {
                var userResult = [];
                userResult = userUpdate;
                res.send(userResult);
            }
            else if (user.nModified == 0)
                res.send(user.status = "UPDATED SUCCESSFULLY");
        }).catch(next);


        // console.log("GET User Service : SuccessFully Inserted");
    }).catch(next);
// mongoose.connection.close();
});

module.exports = router;