
const CryptoJS = require('crypto-js');
const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
//var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var replaceall = require("replaceall");
//template Variable
const templateEntityVal = require('./templateEntity');
var database = require("./DatabaseProperty");
const mongoose = require('mongoose');// Added by Bhagavathy 26/08/2019 MongoDB



//GET the template information to database
router.get('/getTemplate', function (req, res, next) {
    console.log("Inside GET Template Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Template Service : Connection Made");
    templateEntityVal.find().then(function (template) {
        mongoose.connection.close();
        if (template.length > 0)
            res.send(template[0].vDesc);
        else
            res.send(template);
        
    }).catch(next);

});


//Create the template information to database
router.post('/CreateTemplate', function (req, res, next) {
    console.log("Inside createTemplate Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside createTemplate Service : Connection Made");
// console.log(CryptoJS.AES.encrypt(req.body.password.trim(), req.body.password.trim()).toString());
//     templateEntityVal.find().then(function (template) {
//         if (template) {
//             var templatenameValid = true;
//             for (var i = 0; i < template[0].vDesc.length; i++) {
//                 console.log(template[0].vDesc[i].templatename);
//                 if (template[0].vDesc[i].templatename == req.body.templatename) {
//                     templatenameValid = false;
//                 }
//             }
//             if (templatenameValid) {
//                 template[0].vDesc.push(req.body);

//                 templateEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: template[0].vDesc } }).then(function (templateUpdate) {
//                     if (templateUpdate.nModified == 1) {
//                         var templateResult = [];
//                         templateResult = templateUpdate;
//                         res.status(200).json({ "output": "done" });
//                     }
//                     else if (template.nModified == 0)
//                         res.status(200).json(({ "output": "notok" }));
//                 }).catch(err => res.status(200).json({ "Error": err }));
//             }
//         } else {
//             res.status(200).json(({ "output": "notok" }));
//         }

//     }).catch(err => res.status(200).json({ "Error": err }));










var moveFile = false;

waitUntil()
        .interval(500)
        .times(7)
        .condition(function () {
            return (moveFile ? true : false);
        })
        .done(function (result) {
            readDirFiles.read('./dist/assets/publish/' + req.body[0].folderId, 'UTF-8', function (err, files) {
                var fname = Object.keys(files);
                for (var i = 0; i < fname.length; i++) {
                    if (JSON.parse(files[fname[i]])[0].ProductType) {
                        var modified = JSON.parse(files[fname[i]]);
                        var modifyDate = new Date();
                        modified[0]["ModifiedDate"] = modifyDate.toLocaleString();

                        //Post Json
                        //var file = './tmp/data2.json'
                        var file = './dist/assets/publish/' + req.body[0].folderId + '/' + modified[0].TemplateName + '.json';
                        // jsonfile.writeFile(file, modified, function (err) {
                        //     //res.status(200).json(req.body);
                        // })
                        templateEntityVal.create( { vDesc: modified } ).then(function (templateUpdate) {
                            res.status(200).json({ "output": "done" });
                    })

                    } else {

                        var tFiles = [];
                        tFiles.push({ "TemplateId": req.body[0].folderId, "PageName": Object.keys(files)[i], "PageContent": JSON.parse(files[Object.keys(files)[i]]) })
                        // var file = './dist/assets/publish/' + tFiles[0].TemplateId + '/' + tFiles[0].PageName;
                        // jsonfile.writeFile(file, tFiles, function (err) {
                        //     //res.status(200).json(req.body);
                        // })
                        templateEntityVal.create( { vDesc: modified } ).then(function (templateUpdate) {
                            res.status(200).json({ "output": "done" });
                    })

                    }
                    if (fname.length - 1 == i) {
                        rmdir('./dist/assets/tmp/' + req.body[0].folderId, function (err, dirs, files) {

                            res.status(200).json({ "output": "done" });

                        });
                    }
                }

            });

        })




});


router.post('/login', function (req, res, next) {
    console.log("Inside GET Template Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Template Service : Connection Made");
    templateEntityVal.find().then(function (template) {
        mongoose.connection.close();
        if (template) {
            var available = false;
            for (var i = 0; i < template[0].vDesc.length; i++) {
                if (template[0].vDesc[i].templatename == req.body.templatename && template[0].vDesc[i].password == req.body.pwd) {
                    available = true;
                    var udate = new Date(template[0].vDesc[i].expiredate);
                    var tdate = new Date();
                    var today = new Date(tdate.toISOString().substr(0, 10))
                    if (udate < today) {
                        console.log("Expired");
                        available = false;
                    } else {
                        if (template[0].vDesc[i].status == "Enabled") {
                            res.status(200).json(template[0].vDesc[i]);
                        } else {
                            res.status(200).json({ "output": "disabled" });
                        }
                    }
                }
            }
            if (!available) {
                res.status(200).json({ "output": "fail" });
            }
        }
        else {
            res.status(200).json({ "output": "fail" });
        }
    }).catch(err => res.status(200).json({ "Error": err }));
});


router.post('/readPasswordStatus', (req, res) => {

    console.log("Inside GET Template Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Template Service : Connection Made");
    templateEntityVal.find().then(function (template) {
        // mongoose.connection.close();
        if (template) {
            var available = false;
            for (var i = 0; i < template[0].vDesc.length; i++) {
                if (template[0].vDesc[i].templatename == req.body.templatename) {
                    template[0].vDesc[i].password = req.body.password;
                    template[0].vDesc[i].statusFlag = req.body.statusFlag;
                    break;
                }
            }
            // template[0].vDesc.push(req.body);
            templateEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: template[0].vDesc }}).then(function (templateUpdate) {
                if (templateUpdate.nModified == 1) {
                    var templateResult = [];
                    templateResult = templateUpdate;
                    res.send(templateResult);
                }
                else if (template.nModified == 0)
                    res.send(template.status = "UPDATED SUCCESSFULLY");
            }).catch(err => res.status(200).json({ "Error": err }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));
});

router.post('/updateTemplate', (req, res) => {

    console.log("Inside GET Template Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Template Service : Connection Made");
    templateEntityVal.find().then(function (template) {
        
        if (template) {

            for (var i = 0; i < template[0].vDesc.length; i++) {
                if (template[0].vDesc[i].templatename == req.body.templatename) {
                    template[0].vDesc[i] = req.body;
                }
            }
            template[0].vDesc.push(req.body);
            templateEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: template[0].vDesc } }).then(function (templateUpdate) {
                if (templateUpdate.nModified == 1) {
                    var templateResult = [];
                    templateResult = templateUpdate;
                    res.send(templateResult);
                }
                else if (template.nModified == 0)
                    res.send(template.status = "UPDATED SUCCESSFULLY");
            }).catch(err => res.status(200).json({ "Error": err }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));

});


// //GET the template information to database
// router.post('/UpdateTemplate', function (req, res, next) {
//     console.log("Inside Update Template Service : Before Database connection");
//     mongoose.connect(database.url);
//     console.log("Inside Update Template Service : Connection Made");

//     templateEntityVal.find().then(function (template) {
//         var templateValues = [];
//         templateValues = template[0];
//         if (template) {
//             for (var i = 0; i < templateValues.vDesc.length; i++) {
//                 console.log(templateValues.vDesc[i].templatename);
//                 if (templateValues.vDesc[i].templatename == req.body.templatename) {
//                     templatenameValid = false;
//                 }
//             }
//             if (templatenameValid) {

//                 templateValues.vDesc.push(req.body);
//                 templateEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: templateValues.vDesc } }).then(function (templateUpdate) {
//                     if (templateUpdate.nModified == 1) {
//                         var templateResult = [];
//                         templateResult = templateUpdate;
//                         res.status(200).json({ "output": "done" });
//                     }

//                 }).catch(next);
//             }
//         } else {
//             res.status(200).json(({ "output": "notok" }));
//         }
//     }).catch(next);

// });



// //POST the template information to database 
// router.get('/createtemplate/:folderName', function (req, res, next) {
//     console.log("Inside Post Life Stage PC 1 :Before Database connection");
//     mongoose.connect(database.url);
//     readDirFiles.read('./dist/assets/' + req.params.folderName, 'UTF-8', function (err, files) {
//         if (err) {
//             return console.dir(err);
//         }

//         templateEntityVal.create(JSON.parse(files["templateVALU.json"])).then(function (aaaaa) {
//             mongoose.connection.close();
//             res.status(200).send(files["templateVALU.json"]);

//         }).catch(next);



//     });



// });

module.exports = router;