const mongoose = require('mongoose');
const Schema  = mongoose.Schema;
var lovConstants = require("./DatabaseProperty");

const userSchema = new Schema({

    vEmail:{
        type: String,
        required: true,
        default: lovConstants.vEmail
    },
	vString:{
        type: String,
        required: true,
        default: lovConstants.vString
    },
	vDesc:{
        type: JSON,
        required: true
    },
    vStatus:{
        type: String,
        required: true,
        default: lovConstants.vStatus
    },
    vLastUpdUser: {
        type : String,
        required: true,
        default: lovConstants.lastUpdateUser
    },
    vLastUpdProg:{
        type: String,
        required: true,
        default: lovConstants.lastUpdateProg
    },
    vLastUpdDate:{
        type: Date,
        required: true,
        default : new Date()
    }
});

const userSchemaSave = mongoose.model('userPC',userSchema);

module.exports = userSchemaSave;
