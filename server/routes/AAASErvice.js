const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
//var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var replaceall = require("replaceall");
//User Variable
const AAA = require('./AAA');
var database = require("./DatabaseProperty");
const mongoose = require('mongoose');// Added by Bhagavathy 26/08/2019 MongoDB

/**
 * Transaction starts here
 */

//Save the Life Stage PC 1 to database Screen 10
router.post('/postTEST',function(req,res,next){
    // console.log("Inside Post Life Stage PC 1 :Before Database connection");
    mongoose.connect(database.url);
    // console.log("Inside post Life Stage PC 1  :Connection Made");
   AAA.create(req.body).then(function(aaaaa){
        mongoose.connection.close();
        res.send(aaaaa);
        // console.log("Post Life Stage PC 1: SuccessFully Inserted");
   }).catch(next);
   
});

//Save the Life Stage PC 1 to database Screen 10
router.get('/geTEST/:folderName',function(req,res,next){
    // console.log("Inside Post Life Stage PC 1 :Before Database connection");
    mongoose.connect(database.url);
    // var mainFile = 'D:\ProjectWorkspaceAngular\ProductConfigureLifeServer\BKUP\20082019\AFTERRELEASE\QWERT2222\QWERT2222.json';
    // jsonfile.readFile(mainFile, function (err, obj) {

    //     if (obj) {
    //     }
    // })

    readDirFiles.read('./dist/assets/productpublish/' + req.params.folderName, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files["PROD_BENEFITS_TEST.json"]);
        AAA.create(JSON.parse(files["PROD_BENEFITS_TEST.json"])).then(function(aaaaa){
        mongoose.connection.close();
        // res.send(aaaaa);
        // console.log("Post Life Stage PC 1: SuccessFully Inserted");
   }).catch(next);
        //  res.status(200).send(files["PROD_BENEFITS.json"]);


    });
    // var file = './dist/assets/productpublish/' + req.params.folderName + '/' + req.params.folderName + '.json';
    //     jsonfile.readFile(file, function (err, obj) {
    //     if (obj) {
    //         res.status(200).json(obj);
    //     } else {
    //         res.status(200).json(({ "output": "notok" }));
    //     }

    // })
    // console.log("Inside post Life Stage PC 1  :Connection Made");
   
   
});


module.exports = router;