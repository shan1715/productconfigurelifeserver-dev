
const CryptoJS = require('crypto-js');
const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
//var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var replaceall = require("replaceall");
//User Variable
const userEntityVal = require('./userEntity');
var database = require("./DatabaseProperty");
const mongoose = require('mongoose');// Added by Bhagavathy 26/08/2019 MongoDB



//GET the User information to database
router.get('/getuser', function (req, res, next) {
    console.log("Inside GET User Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET User Service : Connection Made");
    userEntityVal.find().then(function (user) {
        mongoose.connection.close();
        if (user.length > 0)
            res.send(user[0].vDesc);
        else
            res.send(user);
        
    }).catch(next);

});


//Create the User information to database
router.post('/CreateUser', function (req, res, next) {
    console.log("Inside createUser Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside createUser Service : Connection Made");
console.log(CryptoJS.AES.encrypt(req.body.password.trim(), req.body.password.trim()).toString());
    userEntityVal.find().then(function (user) {
        if (user) {
            var usernameValid = true;
            for (var i = 0; i < user[0].vDesc.length; i++) {
                console.log(user[0].vDesc[i].username);
                if (user[0].vDesc[i].username == req.body.username) {
                    usernameValid = false;
                }
            }
            if (usernameValid) {
                user[0].vDesc.push(req.body);

                userEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: user[0].vDesc } }).then(function (userUpdate) {
                    if (userUpdate.nModified == 1) {
                        var userResult = [];
                        userResult = userUpdate;
                        res.status(200).json({ "output": "done" });
                    }
                    else if (user.nModified == 0)
                        res.status(200).json(({ "output": "notok" }));
                }).catch(err => res.status(200).json({ "Error": err }));
            }
        } else {
            res.status(200).json(({ "output": "notok" }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));
});


router.post('/login',function (req, res, next) {
    console.log("Inside GET User Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET User Service : Connection Made");
    userEntityVal.find().then(function (user) {
        mongoose.connection.close();
        if (user) {
            var available = false;
            for (var i = 0; i < user[0].vDesc.length; i++) {
                if (user[0].vDesc[i].username == req.body.username && user[0].vDesc[i].password == req.body.pwd) {
                    available = true;
                    var udate = new Date(user[0].vDesc[i].expiredate);
                    var tdate = new Date();
                    var today = new Date(tdate.toISOString().substr(0, 10))
                    if (udate < today) {
                        console.log("Expired");
                        available = false;
                    } else {
                        if (user[0].vDesc[i].status == "Enabled") {
                            res.status(200).json(user[0].vDesc[i]);
                        } else {
                            res.status(200).json({ "output": "disabled" });
                        }
                    }
                }
            }
            if (!available) {
                res.status(200).json({ "output": "fail" });
            }
        }
        else {
            res.status(200).json({ "output": "fail" });
        }
    }).catch(err => res.status(200).json({ "Error": err }));
});


router.post('/readPasswordStatus', (req, res) => {

    console.log("Inside GET User Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET User Service : Connection Made");
    userEntityVal.find().then(function (user) {
        // mongoose.connection.close();
        if (user) {
            var available = false;
            for (var i = 0; i < user[0].vDesc.length; i++) {
                if (user[0].vDesc[i].username == req.body.username) {
                    user[0].vDesc[i].password = req.body.password;
                    user[0].vDesc[i].statusFlag = req.body.statusFlag;
                    break;
                }
            }
            // user[0].vDesc.push(req.body);
            userEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: user[0].vDesc }}).then(function (userUpdate) {
                if (userUpdate.nModified == 1) {
                    var userResult = [];
                    userResult = userUpdate;
                    res.send(userResult);
                }
                else if (user.nModified == 0)
                    res.send(user.status = "UPDATED SUCCESSFULLY");
            }).catch(err => res.status(200).json({ "Error": err }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));
});

router.post('/updateUser', (req, res) => {

    console.log("Inside GET User Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET User Service : Connection Made");
    userEntityVal.find().then(function (user) {
        
        if (user) {

            for (var i = 0; i < user[0].vDesc.length; i++) {
                if (user[0].vDesc[i].username == req.body.username) {
                    user[0].vDesc[i] = req.body;
                }
            }
            user[0].vDesc.push(req.body);
            userEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: user[0].vDesc } }).then(function (userUpdate) {
                if (userUpdate.nModified == 1) {
                    var userResult = [];
                    userResult = userUpdate;
                    res.send(userResult);
                }
                else if (user.nModified == 0)
                    res.send(user.status = "UPDATED SUCCESSFULLY");
            }).catch(err => res.status(200).json({ "Error": err }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));

});


// //GET the User information to database
// router.post('/UpdateUser', function (req, res, next) {
//     console.log("Inside Update User Service : Before Database connection");
//     mongoose.connect(database.url);
//     console.log("Inside Update User Service : Connection Made");

//     userEntityVal.find().then(function (user) {
//         var userValues = [];
//         userValues = user[0];
//         if (user) {
//             for (var i = 0; i < userValues.vDesc.length; i++) {
//                 console.log(userValues.vDesc[i].username);
//                 if (userValues.vDesc[i].username == req.body.username) {
//                     usernameValid = false;
//                 }
//             }
//             if (usernameValid) {

//                 userValues.vDesc.push(req.body);
//                 userEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: userValues.vDesc } }).then(function (userUpdate) {
//                     if (userUpdate.nModified == 1) {
//                         var userResult = [];
//                         userResult = userUpdate;
//                         res.status(200).json({ "output": "done" });
//                     }

//                 }).catch(next);
//             }
//         } else {
//             res.status(200).json(({ "output": "notok" }));
//         }
//     }).catch(next);

// });



// //POST the USER information to database 
// router.get('/createUser/:folderName', function (req, res, next) {
//     console.log("Inside Post Life Stage PC 1 :Before Database connection");
//     mongoose.connect(database.url);
//     readDirFiles.read('./dist/assets/' + req.params.folderName, 'UTF-8', function (err, files) {
//         if (err) {
//             return console.dir(err);
//         }

//         userEntityVal.create(JSON.parse(files["USERVALU.json"])).then(function (aaaaa) {
//             mongoose.connection.close();
//             res.status(200).send(files["USERVALU.json"]);

//         }).catch(next);



//     });



// });

module.exports = router;