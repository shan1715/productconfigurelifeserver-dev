const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
//var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var replaceall = require("replaceall");
//Group Variable
const groupEntityVal = require('./groupEntity');
var database = require("./DatabaseProperty");
const mongoose = require('mongoose');// Added by Bhagavathy 26/08/2019 MongoDB



//GET the Group information to database
router.get('/getGroup', function (req, res, next) {
    console.log("Inside GET Group Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Group Service : Connection Made");
    groupEntityVal.find().then(function (group) {
        mongoose.connection.close();
        if (group.length > 0)
            res.send(group[0].vDesc);
        else
            res.send(group);

    }).catch(next);

});


//Create the Group information to database
router.post('/CreateGroup', function (req, res, next) {
    console.log("Inside createGroup Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside createGroup Service : Connection Made");

    groupEntityVal.find().then(function (group) {
        if (group) {
            if (group.length > 0) {
                var GroupnameValid = true;
                for (var i = 0; i < groupValues[0].vDesc.length; i++) {
                    console.log(groupValues[0].vDesc[i].Groupname);
                    if (groupValues[0].vDesc[i].Groupname == req.body.Groupname) {
                        GroupnameValid = false;
                    }
                }
                if (GroupnameValid) {
                    groupValues[0].vDesc.push(req.body);

                    groupEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: groupValues[0].vDesc } }).then(function (groupUpdate) {
                        if (groupUpdate.nModified == 1) {
                            var groupResult = [];
                            groupResult = groupUpdate;
                            res.status(200).json({ "output": "done" });
                        }
                        else if (Group.nModified == 0)
                            res.status(200).json(({ "output": "notok" }));
                    }).catch(err => res.status(200).json({ "Error": err }));
                }
            } else {
                groupEntityVal.create(req.body).then(function (aaaaa) {
                    mongoose.connection.close();
                    res.status(200).json(({ "output": "INSERTED SUCCESSFULLY" }));
                }).catch(err => res.status(200).json({ "INSERTION ERROR": err }));
            }
        } else {
            res.status(200).json(({ "OUTPUT": "notok" }));
        }

    }).catch(err => res.status(200).json({ "MONGODB CONNECTION": err }));
});


router.post('/login', function (req, res, next) {
    console.log("Inside GET Group Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Group Service : Connection Made");
    groupEntityVal.find().then(function (group) {
        mongoose.connection.close();
        if (group) {
            var available = false;
            for (var i = 0; i < Group[0].vDesc.length; i++) {
                if (group[0].vDesc[i].Groupname == req.body.Groupname && Group[0].vDesc[i].password == req.body.pwd) {
                    available = true;
                    var udate = new Date(group[0].vDesc[i].expiredate);
                    var tdate = new Date();
                    var today = new Date(tdate.toISOString().substr(0, 10))
                    if (udate < today) {
                        console.log("Expired");
                        available = false;
                    } else {
                        if (group[0].vDesc[i].status == "Enabled") {
                            res.status(200).json(group[0].vDesc[i]);
                        } else {
                            res.status(200).json({ "output": "disabled" });
                        }
                    }
                }
            }
            if (!available) {
                res.status(200).json({ "output": "fail" });
            }
        }
        else {
            res.status(200).json({ "output": "fail" });
        }
    }).catch(err => res.status(200).json({ "Error": err }));
});


router.post('/readPasswordStatus', (req, res) => {

    console.log("Inside GET Group Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Group Service : Connection Made");
    groupEntityVal.find().then(function (group) {
        // mongoose.connection.close();
        if (group) {
            var available = false;
            for (var i = 0; i < Group[0].vDesc.length; i++) {
                if (group[0].vDesc[i].Groupname == req.body.Groupname) {
                    Group[0].vDesc[i].password = req.body.password;
                    Group[0].vDesc[i].statusFlag = req.body.statusFlag;
                    break;
                }
            }
            // Group[0].vDesc.push(req.body);
            groupEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: Group[0].vDesc } }).then(function (groupUpdate) {
                if (groupUpdate.nModified == 1) {
                    var groupResult = [];
                    groupResult = groupUpdate;
                    res.send(groupResult);
                }
                else if (group.nModified == 0)
                    res.send(group.status = "UPDATED SUCCESSFULLY");
            }).catch(err => res.status(200).json({ "Error": err }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));
});

router.post('/updateGroup', (req, res) => {

    console.log("Inside GET Group Service : Before Database connection");
    mongoose.connect(database.url);
    console.log("Inside GET Group Service : Connection Made");
    groupEntityVal.find().then(function (group) {

        if (group) {

            for (var i = 0; i < Group[0].vDesc.length; i++) {
                if (group[0].vDesc[i].Groupname == req.body.Groupname) {
                    Group[0].vDesc[i] = req.body;
                }
            }
            Group[0].vDesc.push(req.body);
            groupEntityVal.update({ vString: "Bhagavathy" }, { $set: { vDesc: Group[0].vDesc } }).then(function (groupUpdate) {
                if (groupUpdate.nModified == 1) {
                    var groupResult = [];
                    groupResult = groupUpdate;
                    res.send(groupResult);
                }
                else if (group.nModified == 0)
                    res.send(group.status = "UPDATED SUCCESSFULLY");
            }).catch(err => res.status(200).json({ "Error": err }));
        }

    }).catch(err => res.status(200).json({ "Error": err }));

});




module.exports = router;