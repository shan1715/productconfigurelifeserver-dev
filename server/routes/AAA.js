const mongoose = require('mongoose');
const Schema  = mongoose.Schema;
var lovConstants = require("./DatabaseProperty");

const AAASchema = new Schema({

    vEmail:{
        type: String,
        required: true
    },
	vString:{
        type: String,
        required: true
    },
	vDesc:{
        type: JSON,
        required: true
    },
    vStatus:{
        type: String,
        required: true,
        default: lovConstants.vStatus
    },
    vLastUpdUser: {
        type : String,
        required: true,
        default: lovConstants.lastUpdateUser
    },
    vLastUpdProg:{
        type: String,
        required: true,
        default: lovConstants.lastUpdateProg
    },
    vLastUpdDate:{
        type: Date,
        required: true,
        default : new Date()
    }
});

const AAASchemaSave = mongoose.model('test',AAASchema);

module.exports = AAASchemaSave;
