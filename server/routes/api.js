const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
//var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var replaceall = require("replaceall");
// Created by Pisith 13/11/2019 -->
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var bcrypt = require('bcryptjs');
// Created by Pisith 13/11/2019 -->


// Created by Pisith 13/11/2019 -->
var mongoURL = "mongodb://localhost:27017/";
var dbProductConfiguration = "product_configuration_db";
const userDocument = 'users';
const groupDocument = 'groups';
// Created by Pisith 13/11/2019 -->


var upload = multer({
    dest: './dist/assets/font-image/'
});
var moment = require('moment');

dataDownloadCount = 0;
//var mkdirp = require('mkdirp');

// declare axios for making http requests
const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';


var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './dist/assets/dataUpload/');
    },
    filename: function (req, file, cb) {
    
        cb(null, file.originalname);
        /*
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
        */
    }
});
var upload = multer({ //multer settings
    storage: storage
}).single('file');

/* API path that will upload the files */
router.post('/upload', function (req, res) {
    console.log(1);
    upload(req, res, function (err) {
        console.log(req.file);
        if (err) {
            res.json({
                error_code: 1,
                err_desc: err
            });
            return;
        }
        res.json({
            error_code: 0,
            err_desc: null
        });
    });
});



// Template

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

// Unused
// Read FolderList
// router.get('/readFolders', (req, res) => {


//     console.log("EEEEEEEEEEEEEEEE")
//     readDirFiles.read('./dist/assets/publish/', 'UTF-8', function (err, filenames) {
//         if (err) return console.dir(err);
//         res.status(200).send(filenames);
//     });


// })

// Created by Pisith 27/11/2019
// Read published product from mongodb for and show and user choose in create new product in product creation dialog
router.get('/readFolders', (req, res) => {

    console.log("2222222222222")

    const objA = {}
    mongoose.connection.db.listCollections().toArray(async (err,result)=>{
        for(let i = 0 ;i<result.length;i++){
            if(result[i].name.includes("T_")){
                const objB={}
                const collectionName = result[i].name
                delete mongoose.connection.models[collectionName]
                const Model = mongoose.model(collectionName,schema,collectionName)
                const template = await Model.find({})
                const objKeys = Object.keys(template[0].data)
                for(let j=0;j<objKeys.length;j++){
                    objB[objKeys[j]+".json"]=JSON.stringify(template[0].data[objKeys[j]])
                }
                objA[collectionName.split("_")[1]]=objB
                delete mongoose.connection.models[collectionName]
            }
        }
        res.status(200).send(objA);
    })
    

    // function getCollectionNames() {
    //     return new Promise(function (resolve, reject) {
    //         MongoClient.connect(mongoURL, {
    //             useNewUrlParser: true,
    //             useUnifiedTopology: true
    //         }, (err, db) => {
    //             if (err) throw err;
    //             var dbo = db.db(dbProductConfiguration);
    //             dbo.listCollections().toArray((err, collInfos) => {
    //                 var collectionList = collInfos;
    //                 var results = [];
    //                 if (collInfos) {
    //                     collectionList.forEach(element => {
    //                         // Check type of collection by collection name ('T' = Template)
    //                         if (element.name.charAt(0) === 'T') {
    //                             results.push(element.name);
    //                         }
    //                     });
    //                     resolve(results);
    //                 } else {
    //                     reject(err);
    //                 }
    //             });
    //             db.close();
    //         });
    //     });
    // }

    // function getListOfCollection(collectionList) {
    //     return new Promise((resolve, reject) => {
    //         listOfCollections = [];
    //         collectionList.forEach(element => {
    //             let getSingleCollection = new Promise((resolve, reject) => {
    //                 MongoClient.connect(mongoURL, {
    //                     useNewUrlParser: true,
    //                     useUnifiedTopology: true
    //                 }, (err, db) => {
    //                     if (err) throw err;
    //                     var dbo = db.db(dbProductConfiguration);
    //                     dbo.collection(element).find({}).toArray((err, resultData) => {
    //                         if (resultData) {
    //                             var myJSON = JSON.stringify(resultData);
    //                             var obj = JSON.parse(myJSON.toString());
    //                             resolve(obj[0].data);
    //                         } else {
    //                             reject(err);
    //                         }
    //                         db.close();
    //                     });
    //                 });
    //             });
    //             listOfCollections.push(getSingleCollection);
    //         });
    //         resolve(listOfCollections);
    //     });
    // }

    // function convertToCollectionList(listOfCollectionPromise) {
    //     return new Promise((resolve, reject) => {
    //         Promise.all(listOfCollectionPromise).then(results => {
    //             resolve(results);
    //         })
    //     });
    // }

    // function getPublishTemplateResult(collectionObjectList) {
    //     return new Promise((resolve, reject) => {
    //         let collectionAmount = collectionObjectList.length;
    //         if (collectionAmount === 1) {
    //             resolve(collectionObjectList[0]);
    //         }
    //         if (collectionAmount > 1) {
    //             let fullText;
    //             var i;
    //             // 1. Loop for a single collection
    //             for (i = 0; i < collectionObjectList.length; i++) {
    //                 let obj = collectionObjectList[i];
    //                 // 2. Convert JSON object to JSON string
    //                 let str = JSON.stringify(obj);
    //                 // 3. For first collection => remove the last character and add to the full JSON String variable
    //                 if (i == 0) {
    //                     let first = str;
    //                     first = first.slice(0, -1);
    //                     fullText = first;
    //                 }
    //                 // 4. For next collection => remove the first character, append to the full JSON String variable and remove the last character
    //                 else {
    //                     let after = str.substr(1);
    //                     after = ',' + after;
    //                     fullText += after;
    //                     fullText = fullText.slice(0, -1);
    //                 }
    //             }
    //             // 5. append '}' to the full JSON String variable
    //             fullText += '}'
    //             // 6. Covert the full JSON String variable to a JSON Object and send
    //             let fullJson = JSON.parse(fullText);
    //             resolve(fullJson);
    //         } else {
    //             resolve({});
    //         }
    //     });
    // }
    // async function publishTemplateCollections() {
    //     try {
    //         // console.log('Template INFO');
    //         let collectionNames = await getCollectionNames();
    //         // console.log(collectionNames);
    //         let collectionListPromise = await getListOfCollection(collectionNames);
    //         // console.log(collectionListPromise);
    //         let collectionObjectList = await convertToCollectionList(collectionListPromise);
    //         // console.log(collectionObjectList);
    //         let result = await getPublishTemplateResult(collectionObjectList);
    //         // console.log(result);
    //         // res.status(200).send(filenames);
    //         res.status(200).send(result).end();
    //     } catch (err) {
    //         console.log(err);
    //     }
    // }
    // publishTemplateCollections();

})

// Created by Pisith 27/11/2019

router.post("/publishTemplate",(req,res) => {
    const {templateId,templateName} = req.body;
    var filePath = './dist/assets/tmp/' + templateId + '/' + templateName + '.json';
    jsonfile.readFile(filePath,function(err,obj){
        
    })
    res.send(templateId);
});

// Read directory fileSize
router.get('/readFiles', (req, res) => {

    console.log("FFFFFFFFFFFFFFFFF")
    readDirFiles.read('./dist/assets/publish/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);


    });
})


// Get all posts
router.post('/posts', (req, res) => {
    console.log("zzzzzzzzzzzzzzz")
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder
    //comment by darong
    // pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

        
    //     if (exists) {
    //         //Post Json
    //         //var file = './tmp/data2.json'
    //         var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
    //         jsonfile.writeFile(file, req.body, function (err) {
    //             res.status(200).json(req.body);
    //         })

    //     }

    //     else {

    //         //var folderName=Math.floor(Math.random()*1000)+1;

    //         mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
    //             if (err) {
    //                 console.log(err);
    //             } else {
    //                 //Post Json
    //                 //var file = './tmp/data2.json'
    //                 var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
    //                 jsonfile.writeFile(file, req.body, function (err) {
    //                     res.status(200).json(req.body);
    //                 })

    //             }
    //         });
    //     }

    // });
    res.status(200).json(req.body)
});

router.post('/getTemplateReview', (req, res) => {
   
    var mainFile = './dist/assets/tmp/' + req.body.TId + '/' + req.body.TName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {
       
        var result = [];
    
        if (obj) {

            for (var i = 0; i < obj[0].PageList.length; i++) {
            
                result.push({
                    "PageName": obj[0].PageList[i].name,
                    "PageReview": obj[0].PageList[i].review,
                    "ApprovedBy": obj[0].PageList[i].ApproveBy
                });

            }
            //console.log(obj[0]);
            if (err) return console.dir(err);
            res.status(200).send(result).end();

        }
    });
});

// CreateTemplate
router.post('/createTemplate', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder


    pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});







//Post Template page
router.post('/tempPage', async (req, res) => {
    console.log("4444444444444444444444")
    const collectionName = "TMP_"+req.body[0].TemplateId
    delete mongoose.connection.models[collectionName];
    const Model = mongoose.model(collectionName,schema,collectionName)

    const template = await Model.find({})

    template[0].data[req.body[0].PageName]=req.body
    await Model.updateOne({_id:template[0]._id},{data:template[0].data})
    delete mongoose.connection.models[collectionName];
    res.status(200).json(req.body);


    //<<comment by darong
    // pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

    //     if (exists) {


    //         //Post Json
    //         //var file = './tmp/data2.json'
    //         var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].PageName + ".json";
        
            
    //         jsonfile.writeFile(file, req.body, function (err) {
    //             res.status(200).json(req.body);
    //         })

    //     }

    //     else {

    //         //var folderName=Math.floor(Math.random()*1000)+1;

    //         mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
    //             if (err) {
    //                 console.log(err);
    //             } else {
    //                 //Post Json
    //                 //var file = './tmp/data2.json'
    //                 var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].PageName + ".json";
    //                 jsonfile.writeFile(file, req.body, function (err) {
    //                     res.status(200).json(req.body);
    //                 })

    //             }
    //         });
    //     }

    // });
    //>>
});

router.post('/tempPageNameInsert', async (req, res) => {

    //add by darong

    const collectionName = "TMP_" + req.body[0].TemplateId
    delete mongoose.connection.models[collectionName]
    const Model = mongoose.model(collectionName,schema,collectionName)
    const template = await Model.find({})
    
    const templateKeys = Object.keys(template[0].data)
    const obj = template[0].data[templateKeys[0]]

    if(obj){
        var ok = false
        if(!ok){
            template[0].data[templateKeys[0]][0].PageList.push({ "name": req.body[0].PageName, "review": false, "ApproveBy": "" })
            await Model.updateOne({_id:template[0]._id},{data:template[0].data})
            await Model.updateOne({_id:template[0]._id},{data:template[0].data})
            res.status(200).json({ "review": false });
        }
        delete mongoose.connection.models[collectionName]
    }else{
        res.status(200).json(({"output":"notok"}));
    }
    delete mongoose.connection.models[collectionName]

    /////////////////
    
    
    //<<comment by darong
    // var mainFile = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + '.json';
    // jsonfile.readFile(mainFile, function (err, obj) {

    //     if (obj) {
    //         var ok = false;
    //         if (obj[0].PageList.length > 0) {
    //             for (var i = 0; i < obj[0].PageList.length; i++) {

    //                 if (obj[0].PageList[i].name === req.body[0].PageName) {
    //                     obj[0].PageList[i].review = false;
    //                     ok = true;
    //                     jsonfile.writeFile(mainFile, obj, function (err) {
                            
    //                         res.status(200).json({ "review": false });

    //                     })
    //                     break;
    //                 }

    //             }
    //             if (!ok) {
                   
    //                 obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "ApproveBy": "" });
    //                 jsonfile.writeFile(mainFile, obj, function (err) {
    //                     res.status(200).json({ "review": false });
    //                 })
    //             }
    //         } else {
                
    //             obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "ApproveBy": "" });
    //             jsonfile.writeFile(mainFile, obj, function (err) {
    //                 res.status(200).json({ "review": false });
    //             })
    //         }


    //         //res.status(200).json(obj);
    //     } else {
    //         //	res.status(200).json(({"output":"notok"}));
    //     }
/////////////////>>

    })




router.post('/productPageNameInsert', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            var ok = false;
            if (obj[0].PageList.length > 0) {
                for (var i = 0; i < obj[0].PageList.length; i++) {
                    console.log(obj[0].PageList[i].name);
                    console.log(req.body[0].PageName);
                    if (obj[0].PageList[i].name === req.body[0].PageName) {
                        console.log("match");
                        obj[0].PageList[i].review = false;
                        obj[0].PageList[i].rejected = false;
                        obj[0].PageList[i].approveBy = '';
                        obj[0].PageList[i].modifiedBy = req.body[0].ModifiedBy;
                        obj[0].PageList[i].ModifiedDate = req.body[0].ModifiedDate;
                        obj[0].PageList[i].SaveValue = req.body[0].Svalue;
                        obj[0].PageList[i]["cloned"] = false;
                        ok = true;
                        jsonfile.writeFile(mainFile, obj, function (err) {
                            res.status(200).json({ "review": false });
                        })
                    }

                }
                if (!ok) {
                    obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "rejected": false, "approveBy": '', "modifiedBy": req.body[0].ModifiedBy, "ModifiedDate": req.body[0].ModifiedDate, "SaveValue": req.body[0].Svalue, "cloned": false });
                    jsonfile.writeFile(mainFile, obj, function (err) {
                        res.status(200).json({ "review": false });
                    })
                }
            } else {
                var modifyDate = new Date();
                obj[0]["ModifiedDate"] = modifyDate.toLocaleString();
                obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "rejected": false, "approveBy": '', "modifiedBy": req.body[0].ModifiedBy, "ModifiedDate": req.body[0].ModifiedDate, "SaveValue": req.body[0].Svalue, "cloned": false });
                jsonfile.writeFile(mainFile, obj, function (err) {
                    res.status(200).json({ "review": false });
                })
            }


            //res.status(200).json(obj);
        } else {
            //	res.status(200).json(({"output":"notok"}));
        }

    })

})


//Path exists
router.post('/tempPageExists', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB


    //create folder
    var file = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
    jsonfile.readFile(file, function (err, obj) {
        if (obj) {
            res.status(200).json(obj);
        } else {
            res.status(200).json(({ "output": "notok" }));
        }

    })

});


//moveTmpFiles
router.post('/moveTmpFiles', async (req, res) => {
    console.log("6666666666666666666666")
    const TMPCollection = "TMP_"+req.body[0].folderId
    const ModelTMP = mongoose.model(TMPCollection,schema,TMPCollection)
    const templateTMP = await ModelTMP.find({})
    const objKeys = Object.keys(templateTMP[0].data)
    templateTMP[0].data[objKeys[0]][0].ModifiedDate=new Date().toLocaleString()

    for(let i =1;i<objKeys.length;i++){
        let obj=[]
        obj.push({"TemplateId":req.body[0].folderId,"PageName":objKeys[i]+".json","PageContent":templateTMP[0].data[objKeys[i]]})
        templateTMP[0].data[objKeys[i]]=obj

    }

    const TCollection = "T_"+req.body[0].folderId
    const ModelT = mongoose.model(TCollection,schema,TCollection)
    await ModelT.create({data:templateTMP[0].data})
    await db.db.dropCollection(TMPCollection).catch(ex=>{})
    res.status(200).json({ "output": "done" });

    // var moveFile = false;
    // pathExists('./dist/assets/publish/' + req.body[0].folderId).then(exists => {
    //     if (exists) {
    //         moveFile = true;
    //     } else {


    //         mkdirp('./dist/assets/publish/' + req.body[0].folderId, function (err) {
    //             if (err) {
    //                 console.log(err);
    //             } else {
    //                 moveFile = true;
    //             }
    //         });
    //     }
    // });

    // waitUntil()
    //     .interval(500)
    //     .times(7)
    //     .condition(function () {
    //         return (moveFile ? true : false);
    //     })
    //     .done( async function (result) {

            

            //<<comment by darong
            // readDirFiles.read('./dist/assets/tmp/' + req.body[0].folderId, 'UTF-8',async function (err, files) {
            //     var fname = Object.keys(files);

            //     //added by darong
            //     var productPath=[];
            //     var productInfo={};
            //     var type=[];
            //     var tempId={}
            
            //     var proJson={};
            
            //     for (var i = 0; i < fname.length; i++) {
                    
            //         if (JSON.parse(files[fname[i]])[0].ProductType) {
            //             var modified = JSON.parse(files[fname[i]]);
            //             var modifyDate = new Date();
            //             modified[0]["ModifiedDate"] = modifyDate.toLocaleString();
                        
            //             //Post Json
            //             //var file = './tmp/data2.json'
            //             var file = './dist/assets/publish/' + req.body[0].folderId + '/' + modified[0].TemplateName + '.json';
                        
            //             await jsonfile.writeFile(file, modified)

            //         } else {
                        
            //             var tFiles = [];
            //             tFiles.push({ "TemplateId": req.body[0].folderId, "PageName": Object.keys(files)[i], "PageContent": JSON.parse(files[Object.keys(files)[i]]) })
            //             var file = './dist/assets/publish/' + tFiles[0].TemplateId + '/' + tFiles[0].PageName;
            //             await jsonfile.writeFile(file, tFiles).catch(err=>{})
            //             productPath.push(file)
            //             type.push(Object.keys(files)[i].split(".")[0])
            //         }


            //         if (fname.length - 1 == i) {
                    

            //             // added by Darong
            //             var templatePath = './dist/assets/tmp/' + req.body[0].folderId + '/' + modified[0].TemplateName + '.json';
            //             var t = await jsonfile.readFile(templatePath).catch(err=>{})
            //             var template = JSON.stringify(t)

            //             for(j=0;j<i;j++){
            //                 var result = await jsonfile.readFile(productPath[j]).catch(err=>{})
            //                 productInfo[type[j].toLowerCase()] = JSON.stringify(result)
            //                 proJson[type[j]] = JSON.stringify(result)
            //             }

            //             productInfo.templateId="T_"+req.body[0].folderId
            //             productInfo.template=template
            //             productInfo.templateName=modified[0].TemplateName

            //             proJson[productInfo.templateName]=template
            //             tempId[productInfo.templateId]=proJson

            //             MongoClient.connect(mongoURL,{useNewUrlParser: true,useUnifiedTopology: true}, function(err, db) {
            //                 if (err) throw err;
            //                 var dbo = db.db(dbTemplate);
            //                 dbo.collection(productInfo.templateId).insertOne({data:tempId}, function(err, res) {
            //                   if (err) throw err;
            //                   db.close();
            //                 });
            //               });


            //             ////////

            //             rmdir('./dist/assets/tmp/' + req.body[0].folderId, function (err, dirs, files) {

            //                 res.status(200).json({ "output": "done" });

            //             });
            //         }

            //     }

            // });
            //>>>>>>
        // })
});





//tmptopublishtemplate

router.post('/tmptopublishTemplate', (req, res) => {

    console.log("GGGGGGGGGGGGGGG")
    pathExists('./dist/assets/publish/' + req.body[0].TemplateId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + '.json';
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/publish/' + req.body[0].TemplateId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + '.json';
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});


//tmptopublishfiles
router.post('/tmptopublish', (req, res) => {

    console.log("HHHHHHHHHHHHHHH")
    pathExists('./dist/assets/publish/' + req.body[0].TemplateId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].PageName;
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/publish/' + req.body[0].TemplateId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].PageName;
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});



//movePublishFiles
router.post('/movePublishFiles', (req, res) => {



    pathExists('./dist/assets/tmp/' + req.body[0].folderId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName;
            jsonfile.writeFile(file, req.body[0].PageContent, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/tmp/' + req.body[0].folderId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName;
                    jsonfile.writeFile(file, req.body[0].PageContent, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });

});



//Publish
router.post('/published', (req, res) => {
    console.log("publish");



    rmdir('./dist/assets/tmp/' + req.body.TemplateId, function (err, dirs, files) {

        console.log('all files are removed');
    });


});



//copyPublishFiles
router.post('/copyPublishFiles', (req, res) => {


    pathExists('./dist/assets/tmp/' + req.body[0].folderId).then(exists => {

        if (exists) {

            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);

                /*
                rmdir('./dist/assets/tmp/'+req.body[0].TemplateId, function (err, dirs, files) {
            
                console.log('all files are removed');
                });
                */
            })


        } else {



            mkdirp('./dist/assets/tmp/' + req.body[0].folderId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName + ".json";
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);

                        /*
                        rmdir('./dist/assets/tmp/'+req.body[0].TemplateId, function (err, dirs, files) {
                    
                        console.log('all files are removed');
                        });
                        */

                    })

                }
            });
        }

    });
});


//removeDir
/*
var path = '/path/to/the/dir';

rmdir(path + '/assets', function (err, dirs, files) {
console.log(dirs);
console.log(files);
console.log('all files are removed');
});
*/


//get json
// var jsonfile = require('jsonfile')
//   var file = './tmp/data2.json'
// jsonfile.readFile(file, function (err, obj) {
//   console.log(1);
//   res.status(200).json(obj);
// })


// axios.get(`${API}/posts`)
//   .then(posts => {


//       res.status(200).json(posts.data);

//  })
//   .catch(error => {
//     res.status(500).send(error)
//   });

router.get('/getProductTmpFolders', (req, res) => {

    //List Directory
    readDirFiles.read('./dist/assets/producttmp/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);

    });
});

//tmp folders
router.get('/getTmpFolders', async (req, res) => {
    console.log("33333333333333333333")
    //add by darong
    
    mongoose.connection.db.listCollections().toArray(async (err,result)=>{
        let objA = {}
        for(var i = 0;i<result.length;i++){
            const collectionName = result[i].name
            if(collectionName.includes("TMP_")){
                const Modal = mongoose.model(collectionName,schema,collectionName)
                let document = await Modal.find({})
                const templateName=Object.values(document[0].data)[0][0]["TemplateName"]
                const templateId = Object.values(document[0].data)[0][0]["TemplateId"]
                
                let objB = {}
                objB[templateName+".json"]=JSON.stringify(Object.values(document[0].data)[0])
                objA[templateId]=objB
    
                delete mongoose.connection.models[collectionName];
            }
        }
        res.status(200).send(objA).end();
    })
    /////////////////////

    //List Directory
    // readDirFiles.read('./dist/assets/tmp/', 'UTF-8', function (err, files) {
    //     if (err) return console.dir(err);
    //     console.log(files)
    //     res.status(200).send(files).end();
    // });
});


router.get('/getPublishFolders', async (req, res) => {
    // add by darong
    console.log("777777777777")
    mongoose.connection.db.listCollections().toArray(async (err,result)=>{
        let objA = {}
        for(let i = 0 ;i<result.length;i++){
            let objB = {}
            const collectionName = result[i].name
            if(collectionName.includes("T_")){
                const Model = mongoose.model(collectionName,schema,collectionName)
                const t = await Model.find({})
                const objKeys = Object.keys(t[0].data)
                for(let j = 0 ;j<objKeys.length;j++){
                    objB[objKeys[j]+".json"]=JSON.stringify(t[0].data[objKeys[j]])
                }
                objA[collectionName.split("_")[1]]=objB
                delete mongoose.connection.models[collectionName];
            }
        }
    
        res.status(200).send(objA).end()
    })
    /////////
})


//Unused
//publish folders
// router.get('/getPublishFolders', (req, res) => {


//     //List Directory
//     readDirFiles.read('./dist/assets/publish/', 'UTF-8', function (err, files) {
//         if (err) return console.dir(err);
//         res.status(200).send(files).end();


//     });

// });


// Created by Pisith 13/11/2019 -->
// Read publish from mongodb
// router.get('/getPublishFolders', (req, res) => {
//     function getCollectionNames() {
//         return new Promise(function (resolve, reject) {
//             MongoClient.connect(mongoURL, {
//                 useNewUrlParser: true,
//                 useUnifiedTopology: true
//             }, (err, db) => {
//                 if (err) throw err;
//                 var dbo = db.db(dbTemplate);
//                 dbo.listCollections().toArray((err, collInfos) => {
//                     var collectionList = collInfos;
//                     var results = [];
//                     if (collInfos) {
//                         collectionList.forEach(element => {
//                             results.push(element.name);
//                         });
//                         resolve(results);
//                     } else {
//                         reject(err);
//                     }
//                 });
//                 db.close();
//             });
//         });
//     }
//     function getListOfCollection(collectionList) {
//         return new Promise((resolve, reject) => {
//             listOfCollections = [];
//             collectionList.forEach(element => {
//                 let getSingleCollection = new Promise((resolve, reject) => {
//                     MongoClient.connect(mongoURL, {
//                         useNewUrlParser: true,
//                         useUnifiedTopology: true
//                     }, (err, db) => {
//                         if (err) throw err;
//                         var dbo = db.db(dbTemplate);
//                         dbo.collection(element).find({}).toArray((err, resultData) => {
//                             if (resultData) {
//                                 var myJSON = JSON.stringify(resultData);                   
//                                 var obj = JSON.parse(myJSON.toString());
//                                 resolve(obj[0].data);
//                             } else {
//                                 reject(err);
//                             }
//                             db.close();
//                         });
//                     });
//                 });
//                 listOfCollections.push(getSingleCollection);
//             });
//             resolve(listOfCollections);
//         });
//     }
//     function convertToCollectionList (listOfCollectionPromise) {
//         return new Promise((resolve, reject) => {
//             Promise.all(listOfCollectionPromise).then(results => {
//                 resolve(results);
//             })
//         });
//     }
//     function getPublishTemplateResult (collectionObjectList) {
//         return new Promise((resolve, reject) => {
//             let collectionAmount = collectionObjectList.length;
//             if (collectionAmount === 1) {
//                 resolve(collectionObjectList[0]);
//             }
//             if (collectionAmount > 1) {
//                 let fullText;
//                 var i;
//                 // 1. Loop for a single collection
//                 for (i = 0; i < collectionObjectList.length; i++) {
//                     let obj = collectionObjectList[i];
//                     // 2. Convert JSON object to JSON string
//                     let str = JSON.stringify(obj);
//                     // 3. For first collection => remove the last character and add to the full JSON String variable
//                     if (i == 0) {
//                         let first = str;
//                         first = first.slice(0, -1);
//                         fullText = first;
//                     }
//                     // 4. For next collection => remove the first character, append to the full JSON String variable and remove the last character
//                     else {
//                         let after = str.substr(1);
//                         after = ','+after;
//                         fullText += after;
//                         fullText = fullText.slice(0, -1);
//                     }
//                 }
//                 // 5. append '}' to the full JSON String variable
//                 fullText += '}'
//                 // 6. Covert the full JSON String variable to a JSON Object and send
//                 let fullJson = JSON.parse(fullText);
//                 resolve(fullJson);
//             }
//             else {
//                 resolve({});
//             }
//         });
//     }
//     async function publishTemplateCollections() {
//         try {
//             let collectionNames = await getCollectionNames();
//             let collectionListPromise = await getListOfCollection(collectionNames);
//             let collectionObjectList = await convertToCollectionList(collectionListPromise);
//             let result = await getPublishTemplateResult(collectionObjectList);
//             res.status(200).send(result).end();
//         } catch(err) {
//             console.log(err);
//         }
//     }
//     publishTemplateCollections();
// });
// Created by Pisith 13/11/2019 -->



//getTempJsonFiles
router.post('/getTmpFolders/tmp', (req, res) => {
    

    ls('./dist/assets/tmp/' + req.body[0].folderId + '/', function (err, tree) {

        res.send(tree);
    });


});


// Update by Pisith 02/12/2019
// Pisith Test
//getPublishFiles
router.post('/getPublishFiles/publish', (req, res) => {

    console.log('/getPublishFiles/publish');
    console.log(req.body.folderId);
    console.log("IIIIIIIIIIIII")
    readDirFiles.read('./dist/assets/publish/' + req.body.folderId, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);


    });
})


//ReadPublishFiles
router.post('/readPublishFiles', (req, res) => {

    console.log("AAAAAAAAAAAA")

    readDirFiles.read('./dist/assets/publish/' + req.body.folderId, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);




    });

});

//ReadTemplateFiles
router.post('/readTemplateFiles', async (req, res) => {
    console.log("zzzzzzzzzzz")

    //<<add by darong
    const collectionName = "TMP_"+req.body.folderId
    const Model = mongoose.model(collectionName,schema,collectionName)
    const template = await Model.find({})
    const dataKeys = Object.keys(template[0].data)
    const dataValues = Object.values(template[0].data)
    const dataLength = dataKeys.length
    var obj = {}

    for(let i = 0 ;i<dataLength; i++){
        obj[dataKeys[i]+".json"]=JSON.stringify(dataValues[i])
    }

    delete mongoose.connection.models[collectionName];

    res.status(200).send(obj)
    //>>

    //<<comment by darong
    // readDirFiles.read('./dist/assets/tmp/' + req.body.folderId, 'UTF-8', function (err, files) {
    //     if (err) return console.dir(err);
    //     res.status(200).send(files);
    // });
    //>>
});



// Data


// Read FolderList
router.get('/readProductPublishFolders', (req, res) => {


    readDirFiles.read('./dist/assets/productpublish/', 'UTF-8', function (err, filenames) {
        if (err) return console.dir(err);
        res.status(200).send(filenames);
    });


})

//Post Data page
router.post('/dataTmpPage', async (req, res) => {
    console.log("666666666666666")

    //<<add by darong
    const collectionName = "TMPP_"+req.body[0].ProductId
    const Model = mongoose.model(collectionName,schema,collectionName)
    const m = await Model.find({})
    m[0].data[req.body[0].PageName.split(".")[0]]=req.body
    await Model.updateOne({_id:m[0]._id},{data:m[0].data})
    res.status(200).json(req.body);
    //>>

    //<< comment by darong
    // Get posts from the mock apifa
    // This should ideally be replaced with a service that connects to MongoDB


    //create folder

    // pathExists('./dist/assets/productpublish/' + req.body[0].ProductId).then(exists => {

    //     if (exists) {

    //         //Post Json
    //         //var file = './tmp/data2.json'
    //         var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].PageName;
    //         jsonfile.writeFile(file, req.body, function (err) {
    //             res.status(200).json(req.body);
    //         })


    //     }

    //     else {

    //         //var folderName=Math.floor(Math.random()*1000)+1;

    //         mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
    //             if (err) {
    //                 console.log(err);
    //             } else {
    //                 //Post Json
    //                 //var file = './tmp/data2.json'
    //                 var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].PageName;
    //                 jsonfile.writeFile(file, req.body, function (err) {
    //                     res.status(200).json(req.body);
    //                 })

    //             }
    //         });
    //     }

    // });

    //>>
});



//Post Data page
router.post('/dataProductClone', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB


    //create folder

    var s = JSON.stringify(req.body);

    /*
 var oldWordRegEx = new RegExp(req.body[0].SelectedPages[0][0].data.ProductId,'g');
 s.replace(oldWordRegEx,req.body[0].ProductId);
 */

    var ok = replaceall(req.body[0].SelectedPages[0][0].data.ProductId, req.body[0].ProductId, s);


    req.body = JSON.parse(ok);

    pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

        if (exists) {
            debugger;
            for (var i = 0; i < req.body[0].SelectedPages.length; i++) {
                for (var j = 0; j < req.body[0].SelectedPages[i].length; j++) {
                    if (req.body[0].SelectedPages[i][j].data.enableBlock) {
                        for (var k = 0; k < Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]]).length; k++) {
                            if (JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].TemplateName) {
                                delete req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]]
                            } else {
                                for (var n = 0; n < req.body[0].SelectedPages[i][j].data.PageContent.length; n++) {
                                    if (req.body[0].SelectedPages[i][j].data.PageContent[n].checked) {
                                        for (var p = 0; p < JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].PageContent.length; p++) {

                                            if (JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].PageContent[p].blockId == req.body[0].SelectedPages[i][j].data.PageContent[n].blockId) {
                                                //console.log(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])
                                                debugger;
                                                console.log(JSON.stringify(req.body[0].SelectedPages[i][j].data.PageContent[n].blockId))
                                                var f = req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]];
                                                var a = JSON.parse(f);

                                                a[0].PageContent[p] = req.body[0].SelectedPages[i][j].data.PageContent[n];
                                                //Plan Code update
                                                /*
                           a[0].PageContent[p]["ProductId"]=req.body[0].ProductId;
                          if(a[0].PageContent[p].tableObj){
                          
                              for(var az=0;az<a[0].PageContent[p].tableObj.length;az++){
                          a[0].PageContent[p].tableObj[az]["ProductId"]=req.body[0].ProductId;
                          for(var ay=0;ay<a[0].PageContent[p].tableObj[az].tableRow.length;ay++){
                          
                          //console.log(a[0].PageContent[p].tableObj[az].tableRow[ay].id)
                                  if(a[0].PageContent[p].tableObj[az].tableRow[ay].id==a[0].PageContent[p].accordId+"PlanCode"){
                          console.log(req.body[0].ProductId)
                                      a[0].PageContent[p].tableObj[az].tableRow[ay].tdObj[0].tdValue=req.body[0].ProductId;
                          
                          }
                          
                          }
                              }
                          }
                          */
                                                var changePlanCode = JSON.stringify(a);

                                                changePlanCode.replace(new RegExp(a[0].PageContent[0].ProductId, 'g'), req.body[0].ProductId);

                                                req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]] = changePlanCode;

                                                //var s=JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].PageContent[p]
                                                //console.log(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])
                                                //               var d = [];
                                                //               d.push(req.body[0].SelectedPages[i][j].data);
                                                //req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]] = JSON.stringify(d);

                                            }
                                        }
                                    }
                                }
                            }


                        }
                    }
                }

                if (i == req.body[0].SelectedPages.length - 1) {





                    for (var h = 0; h < Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]]).length; h++) {

                        var pageName = JSON.parse(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[h]])[0].PageName;
                        var pageContent = JSON.parse(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[h]])[0].PageContent;
                        /*
            try{
                                        pageContent = fnCloneAutoNullValues(pageContent);
                                    }catch(err){
                                        console.log("Exception occured while autogenerated values make null.PageName:--"+pageName);
                                    }
                                    */
                        var dataContent = [];
                        dataContent.push({ "ProductId": req.body[0].ProductId, "PageName": pageName, "PageContent": pageContent });

                        var file = './dist/assets/producttmp/' + dataContent[0].ProductId + '/' + dataContent[0].PageName;
                        jsonfile.writeFile(file, dataContent, function (err) {

                        })
                        if (h == Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]]).length - 1) {

                            var postData = { "eMailTo": req.body[0].emailData.eMailTo, "message": req.body[0].emailData.message };
                            res.status(200).json({ "status": "success" });
                            var options = {
                                method: 'post',
                                body: postData,
                                json: true,
                                url: wsUrl.web.url + "ProductBenefits/SendCloningEMail",
                                headers: {

                                }
                            }

                            request(options, function (err, resp, body) {
                                if (err) {
                                    console.log('Error :', err)

                                }
                                console.log(' Body :', body)

                            });
                            /*
                            console.log("succeess");
                            res.status(200).json({"status":"success"});
                            */
                        }
                    }
                }

                else {

                    //res.status(200).json({"status":"fail"});
                }
            }
        }
    });
});


router.get('/getProductTmpFolders', (req, res) => {

    //List Directory
    readDirFiles.read('./dist/assets/producttmp/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);




    });
});

router.get('/getProductTmpJsonFiles',async (req,res)=>{
    console.log("33333333333333")
    //<<change by darong
    mongoose.connection.db.listCollections().toArray(async (err,result)=>{
        let objA = []
        for(let i =0;i<result.length;i++){
            if(result[i].name.includes("TMPP_")) {
                const collectionName = result[i].name
                const Model = mongoose.model(collectionName,schema,collectionName)
                const tempProduct = await Model.find({})
                const objKeys = Object.keys(tempProduct[0].data)
                const product = tempProduct[0].data[objKeys[0]]
                objA.push(product[0])
            }
        }
        res.status(200).json(objA)
    })
    //>>
})



// Updated By Pisith 29/11/2019
// get temporary product json from file
// router.get('/getProductTmpJsonFiles', (req, res) => {
//     console.log('/getProductTmpJsonFiles');
//     //List Directory
//     readDirFiles.read('./dist/assets/producttmp/', 'UTF-8', function (err, files) {
//         if (err) return console.dir(err);
//         var fileName = Object.keys(files);
//         var fileNameArray = [];
//         for (var i = 0; i < fileName.length; i++) {
//             fileNameArray.push(JSON.parse(files[Object.keys(files)[i]][Object.keys(files)[i] + ".json"])[0]);
//         }
//         res.status(200).json(fileNameArray);

//     });
// });
// Updated By Pisith 29/11/2019

//publish folders
router.get('/getProductPublishFolders', (req, res) => {


    //List Directory
    readDirFiles.read('./dist/assets/productpublish/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);


    });

});

//publish folders
router.get('/getProductPublishFoldersRelease/:folderName', (req, res) => {


    //List Directory
    readDirFiles.read('./dist/assets/productpublish/' + req.params.folderName, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);


    });

});

//Unused
// router.get('/getProductPublishJsonFiles', (req, res) => {


//     //List Directory
//     readDirFiles.read('./dist/assets/productpublish/', 'UTF-8', function (err, files) {
//         if (err) return console.dir(err);
//         var fileName = Object.keys(files);
//         var fileNameArray = [];
//         for (var i = 0; i < fileName.length; i++) {
//             fileNameArray.push(JSON.parse(files[Object.keys(files)[i]][Object.keys(files)[i] + ".json"])[0]);
//         }
//         res.status(200).json(fileNameArray);


//     });

// });

// Created by Pisith 15/11/2019
// Fectching list of Published Product from mongodb
router.get('/getProductPublishJsonFiles', (req, res) => {

    function getCollectionNames() {
        return new Promise(function (resolve, reject) {
            MongoClient.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }, (err, db) => {
                if (err) throw err;
                var dbo = db.db(dbProductConfiguration);
                dbo.listCollections().toArray((err, collInfos) => {
                    var collectionList = collInfos;
                    var results = [];
                    if (collInfos) {
                        collectionList.forEach(element => {
                            // Check type of collection by collection name ('P' = Product)
                            if (element.name.charAt(0) === 'P') {
                                results.push(element.name);
                            }
                        });
                        // console.log('Product result : ');
                        // console.log(results);
                        resolve(results);
                    } else {
                        reject(err);
                    }
                });
                db.close();
            });
        });
    }

    function getListOfCollection(collectionList) {
        return new Promise((resolve, reject) => {
            listOfCollections = [];
            collectionList.forEach(element => {
                let getSingleCollection = new Promise((resolve, reject) => {
                    MongoClient.connect(mongoURL, {
                        useNewUrlParser: true,
                        useUnifiedTopology: true
                    }, (err, db) => {
                        if (err) throw err;
                        var dbo = db.db(dbProductConfiguration);
                        dbo.collection(element).find({}).toArray((err, resultData) => {
                            if (resultData) {
                                var myJSON = JSON.stringify(resultData);
                                var obj = JSON.parse(myJSON.toString());
                                var key = element;
                                resolve(obj[0].data[key][key]);
                            } else {
                                reject(err);
                            }
                            db.close();
                        });
                    });
                });
                listOfCollections.push(getSingleCollection);
            });
            resolve(listOfCollections);
        });
    }

    function convertToCollectionList(listOfCollectionPromise) {
        return new Promise((resolve, reject) => {
            Promise.all(listOfCollectionPromise).then(results => {
                resolve(results);
            })
        });
    }

    function dataArrayObject(collectionObjectList) {
        let result = [];
        return new Promise((resolve, reject) => {
            collectionObjectList.forEach(element => {
                let data = JSON.parse(element);
                result.push(data[0]);
            });
            resolve(result);
        });
    }
    async function publishedProductCollections() {
        try {
            // console.log('Product INFO');
            let collectionNames = await getCollectionNames();
            // console.log(collectionNames);
            let collectionListPromise = await getListOfCollection(collectionNames);
            // console.log(collectionListPromise);
            let collectionObjectList = await convertToCollectionList(collectionListPromise);
            // console.log(collectionObjectList);
            let result = await dataArrayObject(collectionObjectList);
            // console.log(result);
            res.status(200).json(result);
        } catch (err) {
            console.log(err);
        }
    }
    publishedProductCollections();
});
// Created by Pisith 15/11/2019

// posts data
router.post('/dataPosts', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder

    pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});


//add by darong
const mongoose = require('mongoose')
mongoose.connect(mongoURL+dbProductConfiguration, {useNewUrlParser: true,useUnifiedTopology: true,useFindAndModify:false});
const db = mongoose.connection
db.on("error",()=>{
    console.log("---error db connection---")
})
db.once("open",()=>{
    console.log("---success connect to db---")
})
const schema = new mongoose.Schema({data:{}})

router.delete('/deleteTmpTemplate/:templateId',async (req,res)=>{
    console.log("222222222222222")
    const templateId = req.params.templateId
    let result = await db.db.dropCollection("TMP_"+templateId).catch(ex=>{})
    if(result)
        res.json({message:"success"})
    else return res.status(404).json({message:"collection not exist"})
    
})

router.post('/createTmpTemplate',async (req,res)=>{
    console.log("11111111111111111111")
    const {data} = req.body
    const collectionName = "TMP_"+data[0].TemplateId
    delete mongoose.connection.models[collectionName];
    const Model = mongoose.model(collectionName,schema,collectionName)
    const obj={}
    obj[data[0].TemplateName]=data
    await Model.create({data:obj})

    delete mongoose.connection.models[collectionName];

    res.send(true)
})

//////////

//moveProductTmpFiles
router.post('/moveProductTmpFiles', (req, res) => {


    var moveFile = false;
    pathExists('./dist/assets/productpublish/' + req.body[0].folderId).then(exists => {
        if (exists) {
            moveFile = true;
        } else {


            mkdirp('./dist/assets/productpublish/' + req.body[0].folderId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    moveFile = true;
                }
            });
        }
    });

    waitUntil()
        .interval(500)
        .times(7)
        .condition(function () {
            return (moveFile ? true : false);
        })
        .done(function (result) {
            readDirFiles.read('./dist/assets/producttmp/' + req.body[0].folderId, 'UTF-8',async function (err, files) {
                var fname = Object.keys(files);
                var obj={}
                for (var i = 0; i < fname.length; i++) {
                    if (JSON.parse(files[fname[i]])[0].ProductType) {
                        var modified = JSON.parse(files[fname[i]]);
                        modified[0].Publish = true;
           
                        var file = './dist/assets/producttmp/' + req.body[0].folderId + '/' + modified[0].ProductId + '.json';
                        // jsonfile.writeFile(file, modified, function (err) {
                        //     //res.status(200).json(req.body);
                        // })

                        //add by Darong
                        await jsonfile.writeFile(file,modified)
                        var productObj = await jsonfile.readFile(file).catch(ex=>{})
                    
                        obj[modified[0].ProductId.split(".")[0]]=JSON.stringify(productObj)

                        ///////

                        var modifyDate = new Date();
                        modified[0]["ModifiedDate"] = modifyDate.toLocaleString();

                        //Post Json
                        //var file = './tmp/data2.json'
                        var file = './dist/assets/productpublish/' + req.body[0].folderId + '/' + modified[0].ProductId + '.json';
                        // jsonfile.writeFile(file, modified, function (err) {
                        //     //res.status(200).json(req.body);
                        // })

                        //add by darong

                        await jsonfile.writeFile(file, modified)

                        ////

                    } else {

                        for (var j = 0; j < req.body[0].pageRelease.length; j++) {
                            if (req.body[0].pageRelease[j].name == Object.keys(files)[i]) {
                                var tFiles = [];
                                tFiles.push({ "ProductId": req.body[0].folderId, "PageName": Object.keys(files)[i], "PageContent": JSON.parse(files[Object.keys(files)[i]]) })

                                //add by darong
                                var file = './dist/assets/productpublish/' + tFiles[0].ProductId + '/' + tFiles[0].PageName;
                                await jsonfile.writeFile(file, tFiles[0].PageContent)

                                var productObj = await jsonfile.readFile(file)
                                obj[tFiles[0].PageName.split(".")[0]]=JSON.stringify(productObj)


                                //////

                            }
                        }


                    }

                    

                    if (fname.length - 1 == i) {
                        ///add by darong
                        var a = {}
                        a[req.body[0].folderId]=obj

                        MongoClient.connect(mongoURL,{useNewUrlParser: true,useUnifiedTopology: true}, function(err, db) {
                            if (err) throw err;
                            var dbo = db.db(dbProduct);
                            dbo.collection("P_"+req.body[0].folderId).insertOne({data:a}, function(err, res) {
                              if (err) throw err;
                              db.close();
                            });
                          });
                        //////////////

                        console.log("delete tmp");
                        res.status(200).json({ "output": "done" });

                        rmdir('./dist/assets/producttmp/' + req.body[0].folderId, function (err, dirs, files) {


                        });

                    }

                }

            });

        })

});


//tmptopublishtemplate

router.post('/tmpProductPublishTemplate', (req, res) => {

    pathExists('./dist/assets/productpublish/' + req.body[0].ProductId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body[0].PageContent);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});


//tmptopublishfiles
router.post('/tmpProductToPublishFiles', (req, res) => {

    pathExists('./dist/assets/productpublish/' + req.body[0].ProductId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].PageName;
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].PageName;
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});


//publish to producttmp

//tmptopublishfiles
router.post('/producttoproducttemp', (req, res) => {

    pathExists('./dist/assets/producttmp/' + req.body.ProductId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.PageName;
            jsonfile.writeFile(file, req.body.PageContent, function (err) {
                res.status(200).json(req.body.PageContent);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/producttmp/' + req.body.ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.PageName;
                    jsonfile.writeFile(file, req.body.PageContent, function (err) {
                        res.status(200).json(req.body.PageContent);
                    })

                }
            });
        }

    });
});



//Publish
router.post('/productPublished', (req, res) => {
    console.log("publish");



    rmdir('./dist/assets/producttmp/' + req.body.FolderId, function (err, dirs, files) {

        console.log('all files are removed');
    });





});

//getPublishFiles
router.post('/getProductPublishFiles/publish', (req, res) => {



    readDirFiles.read('./dist/assets/productpublish/' + req.body.folderId, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);


    });
})

// CreateTemplate
router.post('/createPublishTemplate', async (req, res) => {
    console.log("1111111111111")
    //<<add by darong
    const obj = {}
    const collectionName = "TMPP_"+req.body[0].ProductId
    delete mongoose.connection.models[collectionName]
    const Model = mongoose.model(collectionName,schema,collectionName)
    obj[req.body[0].ProductId]=req.body
    await Model.create({data:obj})
    delete mongoose.connection.models[collectionName]
    res.status(200).json(req.body)
    //>>

    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder

    // pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

    //     if (exists) {
    //         //Post Json
    //         //var file = './tmp/data2.json'
    //         var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
    //         jsonfile.writeFile(file, req.body, function (err) {
    //             res.status(200).json(req.body);
    //         })

    //     }

    //     else {

    //         //var folderName=Math.floor(Math.random()*1000)+1;

    //         mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
    //             if (err) {
    //                 console.log(err);
    //             } else {
    //                 //Post Json
    //                 //var file = './tmp/data2.json'
    //                 var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
    //                 jsonfile.writeFile(file, req.body, function (err) {
    //                     res.status(200).json(req.body);
    //                 })

    //             }
    //         });
    //     }

    // });
});

//moveProductPublishFiles
router.post('/moveProductPublishFiles', async (req, res) => {
    console.log("55555555555555")
    console.log("xxxxxxxxxxx")
    console.log(req.body[0].ProductId)
    console.log(req.body[0].PageName)
    console.log(req.body)

    const collectionName= "TMPP_"+req.body[0].ProductId
    const Model = mongoose.model(collectionName,schema,collectionName)
    const m = await Model.find({})
    m[0].data[req.body[0].PageName.split(".")[0]]=req.body
    await Model.updateOne({_id:m[0]._id},{data:m[0].data})
    res.status(200).json(req.body)

});

//ReadProductPublishFiles
router.post('/readProductPublishFiles', (req, res) => {



    readDirFiles.read('./dist/assets/productpublish/' + req.body.folderId, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);




    });

});

//ReadProductTemplateFiles
router.post('/readProductTemplateFiles', async (req, res) => {
    console.log("44444444444444444")
    const collectionName = "TMPP_"+req.body.folderId
    const Model = mongoose.model(collectionName,schema,collectionName)
    const productTemplate = await Model.find({})
    const objA = {}
    const objKeys = Object.keys(productTemplate[0].data)
    for(let i = 0;i<objKeys.length;i++){
        objA[objKeys[i]+".json"]=JSON.stringify(productTemplate[0].data[objKeys[i]])
    }
    
    res.status(200).send(objA)

    // readDirFiles.read('./dist/assets/producttmp/' + req.body.folderId, 'UTF-8', function (err, files) {
    //     if (err) return console.dir(err);
    //     res.status(200).send(files);
    // });

});

// Get all posts
router.post('/masterCreate', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder

    pathExists('./dist/assets/master/').then(exists => {

        if (exists) {

            var file = './dist/assets/master/' + req.body[0].MasterId + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                console.log("exists master");
                res.status(200).json(req.body);
            })

        }



    });
});

router.get('/getTempMaster', (req, res) => {

    //List Directory
    readDirFiles.read('./dist/assets/master/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);




    });
});

//getPublishFiles

router.post('/getMaster', (req, res) => {
    
    var file = './dist/assets/master/' + req.body.id + '.json';
    jsonfile.readFile(file, function (err, obj) {
        if (obj) {
            res.status(200).json(obj);
        } else {
            res.status(200).json(({ "output": "notok" }));
        }

    })


})


router.post('/getMasterDatas', (req, res) => {
    
    var file = './dist/assets/master/' + req.body[0].id + '.json';
    jsonfile.readFile(file, function (err, obj) {
        if (obj) {
            res.status(200).json(obj);
        } else {
            res.status(200).json(({ "output": "notok" }));
        }

    }
    )


})
// unused
// router.post('/postGroup', (req, res) => {
    
//     var file = './dist/assets/data/groups.json';
//     jsonfile.writeFile(file, req.body, function (err) {
//         res.status(200).json(req.body);
//     })


// })

// Created By Pisith 19/11/2019
// Insert new group into mongodb
router.post('/postGroup', (req, res) => {
    MongoClient.connect(mongoURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, (err, db) => {
        if (err) throw err;
        var dbo = db.db(dbProductConfiguration);
        let length = req.body.length;
        let dataInsert = req.body[length - 1];
        dbo.collection(groupDocument).insertOne(dataInsert, function (error, response) {
            if (error) throw error;
            res.status(200).json(response.ops[0]);
            db.close();
        });
    });
})
// Created By Pisith 19/11/2019

// Created By Pisith 20/11/2019
// New API
// Fetching all groups to show in admin page
router.get('/getGroups', (req, res) => {
    function getGroupList() {
        return new Promise((resolve, reject) => {
            MongoClient.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }, (err, db) => {
                if (err) throw err;
                var dbo = db.db(dbProductConfiguration);
                dbo.collection(groupDocument).find({}).toArray((err, resultData) => {
                    if (resultData) {
                        resolve(resultData);
                    } else {
                        reject(err);
                    }
                    db.close();
                });
            });
        });
    }
    async function getGroupCollection() {
        let obj = await getGroupList();
        // console.log(obj);
        res.status(200).json(obj);
    }
    getGroupCollection();
})
// Created By Pisith 20/11/2019

// unused
// router.post('/createUser', (req, res) => {

//     var file = './dist/assets/data/user.json';
//     // jsonfile.readFile(file, function (err, obj) {
//     //     console.log(err)
//     //     if (obj) {
//     //         var usernameValid = true;
//     //         for (var i = 0; i < obj.length; i++) {
            
//     //             if (obj[i].username == req.body.username) {
//     //                 usernameValid = false;
//     //             }


//     //         }
//     //         if (usernameValid) {

//                // obj.push(req.body);
//                 jsonfile.writeFile(file, req.body, function (err) {
        
//                     res.status(200).json({ "output": "done" });
//                 })

//         //     }
//         // } else {
//         //     res.status(200).json(({ "output": "notok" }));
//         // }



   
// })

// Created By Pisith 19/11/2019
// /home/pisith/project/PCLife/Web/ProductConfigureLifeServer/dist/assets/data/user.json
// Need to encrypt the generated password before save
router.post('/createUser', (req, res) => {
    function encryptPassword(req) {
        return new Promise((resolve, reject) => {
            let password = req.body.password;
            const rounds = 10
            bcrypt.hash(password, rounds, (err, hash) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    return
                }
                console.log(hash)
                resolve(hash);
            })
        });
    }

    function saveUser(encryptedPassword) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }, (err, db) => {
                if (err) throw err;
                var dbo = db.db(dbProductConfiguration);
                req.body.password = encryptedPassword;
                dbo.collection(userDocument).insertOne(req.body, (err, result) => {
                    if (err) throw err;
                    resolve({
                        "output": "done"
                    });
                    db.close();
                });
            });
        });
    }
    async function createUser(req, res) {
        try {
            let encryptedPassword = await encryptPassword(req);
            let result = await saveUser(encryptedPassword);
            res.status(200).json(result);
        } catch (err) {
            console.log(err);
        }
    }
    createUser(req, res);
})
// Created By Pisith 19/11/2019

// Created By Pisith 20/11/2019
// New API
// Fetching all users to show in admin page
router.get('/getUsers', (req, res) => {
    function getUserList() {
        return new Promise((resolve, reject) => {
            MongoClient.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }, (err, db) => {
                if (err) throw err;
                var dbo = db.db(dbProductConfiguration);
                dbo.collection(userDocument).find({}).toArray((err, resultData) => {
                    if (resultData) {
                        resolve(resultData);
                    } else {
                        reject(err);
                    }
                    db.close();
                });
            });
        });
    }
    async function getUserCollection() {
        let obj = await getUserList();
        res.status(200).json(obj);
    }
    getUserCollection();
})
// Created By Pisith 20/11/2019

router.post('/updateUser', (req, res) => {
    var file = './dist/assets/data/user.json';
    jsonfile.readFile(file, function (err, obj) {
        if (obj) {
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].username == req.body.username) {
                    obj[i] = req.body;
                    jsonfile.writeFile(file, obj, function (err) {
                        res.status(200).json({ "output": "done" });

                    })


                }



            }
        } else {
            res.status(200).json(({ "output": "notok" }));
        }



    })
})


router.post('/addProductType', (req, res) => {

    var mainFile = './dist/assets/data/prodType.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            var availablity = false;
            if (obj.productType.length > 0) {
                for (var i = 0; i < obj.productType.length; i++) {

                    if (obj.productType[i] == req.body.productType) {
                        availablity = true;


                    } else {
                        availablity = false;
                    }
                }
                if (!availablity) {
                    obj.productType.push(req.body.productType);
                    jsonfile.writeFile(mainFile, obj, function (err) {


                        res.status(200).json({ "output": "pass", "data": obj });

                    })
                } else {
                    res.status(200).json({ "output": "fail" })
                }
            }
        }
    })

});

// Unused
// router.post('/login', async (req, res) => {
    
//     var file = './dist/assets/data/user.json';

//     jsonfile.readFile(file, function  (err, obj) {
    
//         if (obj) {
//             var available = false;
//             for (var i = 0; i < obj.length; i++) {
//                 if (obj[i].username == req.body.username && obj[i].password == req.body.pwd) {
//                     available = true;
//                     var udate = new Date(obj[i].expiredate);
//                     var tdate = new Date();
//                     var today = new Date(tdate.toISOString().substr(0, 10))
//                     if (udate < today) {
//                         console.log("Expired");
//                         available = false;
//                     } else {
//                         if (obj[i].status) {
//                             res.status(200).json(obj[i]);
//                         } else {
//                             res.status(200).json({ "output": "disabled" });

//                         }
//                     }


//                 }


//             }
//             if (!available) {
//                 res.status(200).json({ "output": "fail" });

//             }

//         } else {
//             res.status(200).json({ "output": "fail" });
//         }



//     })
// })


// Updated by Pisith 28/11/2019
// Update login
router.post('/login', (req, res) => {

    function findByUsername(req) {
        return new Promise((resolve, reject) => {
            let username = req.body.username;
            MongoClient.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }, (err, db) => {
                if (db) {
                    var dbo = db.db(dbProductConfiguration);
                    let findQuery = {
                        'username': username
                    };
                    dbo.collection(userDocument).findOne(findQuery, (err, userData) => {
                        if (userData) {
                            resolve(userData)
                        } else {
                            reject(err);
                        }
                        db.close();
                    });
                } else {
                    reject(err);
                }
            });
        });
    }

    async function loginResponse(req, userData) {
        return new Promise((resolve, reject) => {
            let password = req.body.pwd;
            if (userData) {
                let available = false;
                bcrypt.compare(password, userData.password, (err, res) => {
                    if (res) {
                        available = true;
                        var udate = new Date(userData.expiredate);
                        var tdate = new Date();
                        var today = new Date(tdate.toISOString().substr(0, 10))
                        if (udate < today) {
                            console.log("Expired");
                            available = false;
                        } else {
                            if (userData.status) {
                                resolve(userData);
                            } else {
                                resolve({
                                    "output": "disabled"
                                })
                            }
                        }
                    } else {
                    }
                    if (!available) {
                        resolve({
                            "output": "fail"
                        });
                    }
                })
            } else {
                resolve({
                    "output": "fail"
                });
            }
        })
    }

    async function login(req, res) {
        try {
            let userDataList = await findByUsername(req);
            let result = await loginResponse(req, userDataList);
            res.status(200).json(result);
        } catch (err) {
            console.log(err);
        }
    }
    login(req, res);
});
// Updated by Pisith 28/11/2019


router.post('/delTmpFolder', (req, res) => {


    rmdir('./dist/assets/tmp/' + req.body.foldername, function (err, dirs, files) {

        res.status(200).json({ "output": "Deleted" });
    });

});

router.post('/delPublishFolder', (req, res) => {

    console.log("BBBBBBBBBBBBBBBB")
    rmdir('./dist/assets/publish/' + req.body.foldername, function (err, dirs, files) {

        res.status(200).json({ "output": "Deleted" });
    });





});


router.post('/delProductPublishFolder', (req, res) => {


    rmdir('./dist/assets/productpublish/' + req.body.foldername, function (err, dirs, files) {
        console.log("Deleted");
        res.status(200).json({ "output": "Deleted" });
    });





});

router.post('/delProductTmpFolder', (req, res) => {


    rmdir('./dist/assets/producttmp/' + req.body.foldername, function (err, dirs, files) {
        console.log("Deleted");
        res.status(200).json({ "output": "Deleted" });
    });





});


router.post('/tempApprove', (req, res) => {

    var mainFile = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            if (obj[0].PageList.length > 0) {
                for (var i = 0; i < obj[0].PageList.length; i++) {

                    if (obj[0].PageList[i].name == req.body.PageName) {
                        obj[0].PageList[i].review = true;

                        jsonfile.writeFile(mainFile, obj, function (err) {
                            res.status(200).json({ "output": "pass" })
                        })

                    }
                }
            }
        }
    })


});

router.post('/readTempApproveStatus', async (req, res) => {
    const collectionName = "TMP_"+ req.body.TemplateId
    delete mongoose.connection.models[collectionName];
    const Model = mongoose.model(collectionName,schema,collectionName)
    const template = await Model.find({})
    if(template.length>0) {
        const obj = template[0].data[req.body.TemplateName]
        if(obj[0].PageList.length > 0){
            let found = false;
            for (var i = 0; i < obj[0].PageList.length; i++) {
                if (obj[0].PageList[i].name == req.body.PageName) {
                    found = true;
                    if (obj[0].PageList[i].review == true) {
                        
                        res.status(200).json({ "review": true })
                    } else {
                        
                        res.status(200).json({ "review": false })
                    }
                    if (i == obj[0].PageList.length - 1) {
                        if (!found) {
                            
                            res.status(200).json({ "review": true });
                        }
                    }
                }
            }
        }  
    }else {
    
        res.status(200).json({ "review": true });
    }

    delete mongoose.connection.models[collectionName];


    
    
    //<< comment by darong
    // var mainFile = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
    // jsonfile.readFile(mainFile, function (err, obj) {

    //     if (obj) {
    //         var found = false;
    //         debugger;
    //         if (obj[0].PageList.length > 0) {
    //             for (var i = 0; i < obj[0].PageList.length; i++) {

    //                 if (obj[0].PageList[i].name == req.body.PageName) {
    //                     found = true;
    //                     if (obj[0].PageList[i].review == true) {
    //                         res.status(200).json({ "review": true })
    //                     } else {
    //                         res.status(200).json({ "review": false })
    //                     }
    //                 }
    //                 if (i == obj[0].PageList.length - 1) {
    //                     if (!found) {
    //                         res.status(200).json({ "review": true });
    //                     }
    //                 }
    //             }
    //         }
    //     } else {
    //         res.status(200).json({ "review": true });
    //     }
    // })
    // >>>

});

router.post('/prodApprove', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            if (obj[0].PageList.length > 0) {
                for (var i = 0; i < obj[0].PageList.length; i++) {

                    if (obj[0].PageList[i].name == req.body.PageName) {
                        if (req.body.PageReview == "Approved") {
                            obj[0].PageList[i].review = true;
                            obj[0].PageList[i].rejected = false;
                            obj[0].PageList[i].approveBy = req.body.ApproveBy;
                        } else if (req.body.PageReview == "Rejected") {
                            obj[0].PageList[i].review = false;
                            obj[0].PageList[i].rejected = true;
                            obj[0].PageList[i].approveBy = '';
                        }
                        jsonfile.writeFile(mainFile, obj, function (err) {
                            if (err) {
                                res.status(200).json({ "output": "fail" })
                            } else {
                                res.status(200).json({ "output": "pass" })
                            }
                        })
                    }
                }
            }
        }
    })


});



//tmptopublishtemplate

router.post('/copyTemplate', (req, res) => {

    console.log("CCCCCCCCCCCCCCCCCCCC")
    pathExists('./dist/assets/publish/' + req.body[0].ProductId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body[0].PageContent);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});

router.post('/reviewComments', (req, res) => {

    var mainFile = './dist/assets/tmp/' + req.body.PId + '/' + req.body.PName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            obj[0].Reviews.push(req.body);

            jsonfile.writeFile(mainFile, obj, function (err) {
                res.status(200).json({ "output": "pass" })
            })
        }
    })
});
router.post('/getComments', (req, res) => {

    var mainFile = './dist/assets/tmp/' + req.body.PId + '/' + req.body.PName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {
        var result = [];
        if (obj) {

            for (var i = obj[0].Reviews.length - 1; i >= 0; i--) {
                if (obj[0].Reviews[i].PageName == req.body.PageName) {
                    result.push({
                        "User": obj[0].Reviews[i].User,
                        "Review": obj[0].Reviews[i].Review,
                        "CommentDate": obj[0].Reviews[i].CommentDate
                    });
                }
            }

            if (err) return console.dir(err);
            res.status(200).send(result).end();

        }
    });
});
router.post('/Approve', async (req, res) => {
    console.log("5555555555555555555555")
    
    const collectionName = "TMP_"+req.body.TemplateId
    delete mongoose.connection.models[collectionName]
    const Model = mongoose.model(collectionName,schema,collectionName)
    const template = await Model.find({}) 
    
    for(var i = 0; i<template[0].data[req.body.TemplateName][0].PageList.length;i++){
        if(template[0].data[req.body.TemplateName][0].PageList[i].name==req.body.PageName) {
            template[0].data[req.body.TemplateName][0].PageList[i].review = req.body.PageReview
            template[0].data[req.body.TemplateName][0].PageList[i]["ApproveBy"] = req.body.ApproveBy
        }    
    }

    await Model.updateOne({_id:template[0]._id},{data:template[0].data})
    await Model.updateOne({_id:template[0]._id},{data:template[0].data})

    delete mongoose.connection.models[collectionName];

    res.status(200).json({ "output": "pass" })

    //<<comment by darong
    // jsonfile.readFile(mainFile, function (err, obj) {    
    //     if (obj) {
    //         console.log(obj)
    //         if (obj[0].PageList.length > 0) {
    //             //debugger;
    //             for (var i = 0; i < obj[0].PageList.length; i++) {

    //                 if (obj[0].PageList[i].name == req.body.PageName) {
    //                     obj[0].PageList[i].review = req.body.PageReview;
    //                     obj[0].PageList[i]["ApproveBy"] = req.body.ApproveBy;


                       
    //                     jsonfile.writeFile(mainFile, obj, function (err) {
    //                         res.status(200).json({ "output": "pass" })
    //                     })

    //                 }
    //             }
    //         }
    //     }
    // })
    ////>>>>

});


router.post('/getTemplateReview', (req, res) => {

    var mainFile = './dist/assets/tmp/' + req.body.TId + '/' + req.body.TName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {
        var result = [];
        if (obj) {

            for (var i = 0; i < obj[0].PageList.length; i++) {

                result.push({
                    "PageName": obj[0].PageList[i].name,
                    "PageReview": obj[0].PageList[i].review,
                    "ApprovedBy": obj[0].PageList[i].ApproveBy
                });

            }
            //console.log(obj[0]);
            if (err) return console.dir(err);
            res.status(200).send(result).end();

        }
    });
});

router.post('/getProductReview', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.TId + '/' + req.body.TId + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {
        var result = [];
        if (obj) {

            for (var i = 0; i < obj[0].PageList.length; i++) {

                result.push({
                    "ProductName": obj[0].PageList[i].name,
                    "ProductReview": obj[0].PageList[i].review,
                    "ApprovedBy": obj[0].PageList[i].approveBy
                });

            }
        
            if (err) return console.dir(err);
            res.status(200).send(result).end();

        }
    });
});

router.post('/getProductReviewPublish', (req, res) => {

    var mainFile = './dist/assets/productpublish/' + req.body.TId + '/' + req.body.TId + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {
        var result = [];
        if (obj) {

            for (var i = 0; i < obj[0].PageList.length; i++) {

                result.push({
                    "ProductName": obj[0].PageList[i].name,
                    "ProductReview": obj[0].PageList[i].review,
                    "ApprovedBy": obj[0].PageList[i].approveBy
                });

            }
            
            if (err) return console.dir(err);
            res.status(200).send(result).end();

        }
    });
});

router.post('/readProdApproveStatus', async (req, res) => {
    console.log("77777777777777")
    const collectionName = "TMPP_"+req.body.ProductId
    const Model = mongoose.model(collectionName,schema,collectionName)
    const product = await Model.find({})
    if(product.length<1){

    }
    const obj = product[0].data[req.body.ProductName]
    for(let i = 0;i < obj[0].PageList.length; i++){
        if (obj[0].PageList[i].name == req.body.PageName) {
            if (obj[0].PageList[i].review == true) {
                var cloned = obj[0].PageList[i].cloned ? true : false;
                res.status(200).json({ "review": true, "cloned": cloned });
                break;
            } else {
                var cloned = obj[0].PageList[i].cloned ? true : false;
                res.status(200).json({ "review": false, "cloned": cloned });
                break;

            }
        }
    }
    
    pathExists('./dist/assets/producttmp/' + req.body.ProductId).then(exists => {
        if (!exists) {
            var mainFile = './dist/assets/productpublish/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
            jsonfile.readFile(mainFile, function (err, obj) {

                if (obj) {
                    if (obj[0].PageList.length > 0) {
                        for (var i = 0; i < obj[0].PageList.length; i++) {

                            if (obj[0].PageList[i].name == req.body.PageName) {
                                if (obj[0].PageList[i].review == true) {
                                    res.status(200).json({ "review": true });
                                    break;
                                } else {
                                    res.status(200).json({ "review": false });
                                    break;
                                }



                            }
                        }
                    }
                }
            })

        } else {


            var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
            jsonfile.readFile(mainFile, function (err, obj) {

                if (obj) {
                    if (obj[0].PageList.length > 0) {
                        for (var i = 0; i < obj[0].PageList.length; i++) {

                            if (obj[0].PageList[i].name == req.body.PageName) {
                                if (obj[0].PageList[i].review == true) {
                                    var cloned = obj[0].PageList[i].cloned ? true : false;
                                    res.status(200).json({ "review": true, "cloned": cloned });
                                    break;
                                } else {
                                    var cloned = obj[0].PageList[i].cloned ? true : false;
                                    res.status(200).json({ "review": false, "cloned": cloned });
                                    break;

                                }



                            }
                        }
                    }
                }
            })
        }
    });


});

router.post('/firstBlockStatus', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            if (obj[0].PageList.length > 0) {
                var available = false;
                for (var i = 0; i < obj[0].PageList.length; i++) {

                    if (obj[0].PageList[i].name == req.body.PageName) {
                        available = true;
                        res.status(200).json({ "pageAvailable": true })



                    }
                }
                if (!available) {
                    res.status(200).json({ "pageAvailable": false });
                }
            } else {
                res.status(200).json({ "pageAvailable": false });
            }
        }
    })


});
// unused
// router.post('/readPasswordStatus', (req, res) => {

//     var mainFile = './dist/assets/data/user.json';
//     jsonfile.readFile(mainFile, function (err, obj) {

//         if (obj) {

//             for (var i = 0; i < obj.length; i++) {
//                 if (obj[i].username == req.body.username) {
//                     obj[i].password = req.body.password;
//                     obj[i].statusFlag = req.body.statusFlag;
//                     break;
//                 }
//             }
//             jsonfile.writeFile(mainFile, obj, function (err) {
//                 res.status(200).json({ "status": "success" });
//             })
//         }
//     })


// });

// Created by Pisith 27/11/2019
// Update user password and statusFlag after change password
router.post('/readPasswordStatus', (req, res) => {

    function encryptPassword(req) {
        return new Promise((resolve, reject) => {
            let password = req.body.password;
            const rounds = 10
            bcrypt.hash(password, rounds, (err, hash) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    return
                }
                console.log(hash)
                resolve(hash);
            })
        });
    }

    function updateUserPasswordAndStatus (req, encryptedPassword) {
        return new Promise((resolve, reject) => {
            console.log(`user new password : ${encryptedPassword}`);
            MongoClient.connect(mongoURL, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }, (err, db) => {
                if (err) throw err;
                var dbo = db.db(dbProductConfiguration);
                let query = {
                    'username': req.body.username
                };
                let newValues = {
                    $set: {
                        'password': encryptedPassword,
                        'statusFlag': req.body.statusFlag
                    }
                };
                dbo.collection(userDocument).updateOne(query, newValues, function (err, result) {
                    if (err) throw err;
                    else {
                        resolve({"status": "success"});

                    }
                    db.close();
                });
            });
        })
    }

    async function updateUserData (req, res) {
        try {
            let encryptedPassword = await encryptPassword(req);
            let result = await updateUserPasswordAndStatus(req, encryptedPassword);
            res.status(200).json(result);
        } catch(err) {
            console.log(err);
        }
    }

    updateUserData(req, res);
});

// Created by Pisith 27/11/2019

router.post('/addProductType', (req, res) => {

    var mainFile = './dist/assets/data/prodType.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            var availablity = false;
            if (obj.productType.length > 0) {
                for (var i = 0; i < obj.productType.length; i++) {

                    if (obj.productType[i] == req.body.productType) {
                        availablity = true;


                    } else {
                        availablity = false;
                    }
                }
                if (!availablity) {
                    obj.productType.push(req.body.productType);
                    jsonfile.writeFile(mainFile, obj, function (err) {


                        res.status(200).json({ "output": "pass", "data": obj });

                    })
                } else {
                    res.status(200).json({ "output": "fail" })
                }
            }
        }
    })

});


router.post('/createTmpCloneTemplate', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder
    debugger;
    
    pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});

router.post('/ProductComments', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            obj[0].Reviews.push(req.body);

            jsonfile.writeFile(mainFile, obj, function (err) {
                res.status(200).json({ "output": "pass" })
            })
        }
    })
});
router.post('/getPComments', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {
        var result = [];
        if (obj) {

            for (var i = obj[0].Reviews.length - 1; i >= 0; i--) {
                if (obj[0].Reviews[i].PageName == req.body.PageName) {
                    result.push({
                        "User": obj[0].Reviews[i].User,
                        "Review": obj[0].Reviews[i].Review,
                        "CommentDate": obj[0].Reviews[i].CommentDate
                    });
                }
            }

            if (err) return console.dir(err);
            res.status(200).send(result).end();

        }
    });
});
router.post('/ProductApprove', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            if (obj[0].PageList.length > 0) {
                for (var i = 0; i < obj[0].PageList.length; i++) {

                    if (obj[0].PageList[i].name == req.body.PageName) {
                        obj[0].PageList[i].review = req.body.PageReview;
                       
                        jsonfile.writeFile(mainFile, obj, function (err) {
                            res.status(200).json({ "output": "pass" })
                        })

                    }
                }
            }
        }
    })

});

router.post('/dataEntryMonitor', async (req, res) => {
    console.log("88888888888")
    //debugger;

    const collectionName = "TMPP_"+req.body.folderId
    const Model = mongoose.model(collectionName,schema,collectionName)
    let product = await Model.find({})
    let obj = product[0].data[req.body.folderId]
    const ok = false
    if(obj[0].DataMonitor.length>0){
        for(let i = 0;i<obj[0].DataMonitor.length;i++){
            if (obj[0].DataMonitor[i].name == req.body.data.PageName) {
                ok = true
                product[0].data[req.body.folderId][0].DataMonitor[i].data = req.body.data;
                await Model.updateOne({_id:product[0]._id},{data:product[0].data})
                res.status(200).json({ "status": "done" });
                break;
            }
        }
        if(!ok){
            product[0].data[req.body.folderId][0].DataMonitor.push({ "name": req.body.data.PageName, "data": req.body.data })
            await Model.updateOne({_id:product[0]._id},{data:product[0].data})
            res.status(200).json({ "status": "done" });
        }
    }else{
        if(!ok){
            product[0].data[req.body.folderId][0].DataMonitor.push({ "name": req.body.data.PageName, "data": req.body.data })
            await Model.updateOne({_id:product[0]._id},{data:product[0].data})
            res.status(200).json({ "status": "done" });
        }
    }
    

    //<< comment by darong
    // var mainFile = './dist/assets/producttmp/' + req.body.folderId + '/' + req.body.folderId + '.json';
    // jsonfile.readFile(mainFile, function (err, obj) {

    //     if (obj) {
    //         var ok = false;
    //         if (obj[0].DataMonitor.length > 0) {
    //             for (var i = 0; i < obj[0].DataMonitor.length; i++) {

    //                 if (obj[0].DataMonitor[i].name == req.body.data.PageName) {
    //                     obj[0].DataMonitor[i].data = req.body.data;
    //                     ok = true;
    //                     jsonfile.writeFile(mainFile, obj, function (err) {
                            
    //                         res.status(200).json({ "status": "done" });

    //                     })
    //                     break;
    //                 }

    //             }
    //             if (!ok) {
               
    //                 obj[0].DataMonitor.push({ "name": req.body.data.PageName, "data": req.body.data });
    //                 jsonfile.writeFile(mainFile, obj, function (err) {
    //                     res.status(200).json({ "status": "done" });
    //                 })
    //             }
    //         } else {
                
    //             obj[0].DataMonitor.push({ "name": req.body.data.PageName, "data": req.body.data });
    //             jsonfile.writeFile(mainFile, obj, function (err) {
    //                 res.status(200).json({ "status": "done" });
    //             })
    //         }


    //         //res.status(200).json(obj);
    //     } else {
    //         //	res.status(200).json(({"output":"notok"}));
    //     }
    // })
    //>>

});

router.post('/dataEntryMonitorStatus', (req, res) => {
    debugger;

    if (!req.body.publishStatus) {
        var mainFile = './dist/assets/producttmp/' + req.body.folderId + '/' + req.body.folderId + '.json';
        jsonfile.readFile(mainFile, function (err, obj) {

            if (obj) {
                res.status(200).json(obj);

            }
        });
    } else if (req.body.publishStatus) {
        var mainFile = './dist/assets/productpublish/' + req.body.folderId + '/' + req.body.folderId + '.json';
        jsonfile.readFile(mainFile, function (err, obj) {

            if (obj) {
                res.status(200).json(obj);

            }
        });
    }
});


//oracle function
router.post('/getTableName', (req, res) => { oracle.getTableName(req, res); });
router.post('/fetchTableRows', (req, res) => { oracle.fetchTableRows(req, res); });
router.post('/saveTableRows', (req, res) => { oracle.saveTableRows(req, res); });
router.post('/DeleteTableRow', (req, res) => { oracle.DeleteTableRow(req, res); });

router.post('/AddnewLookup', (req, res) => {

    console.log('API calling..' + req.body.lookupName);
    //res.status(200).json({"status":"success","data new":req.body});
    //racle.getTableName(req,res);
    try {
        pathExists('./dist/assets/data/lookup.json').then(exists => {

            var file = './dist/assets/data/lookup.json';

            jsonfile.readFile(file, function (err, obj) {
                //console.log(obj);
                console.log("----------------------------");
                var data = JSON.parse(JSON.stringify(obj));
                //console.log('json: '+ data.lookupList[0].lookupTableColumn[0].COLUMN_NAME)
                data.lookupList.push(req.body);
                //console.log(data);
                jsonfile.writeFile(file, data, function (err) {
                    console.log("---------------write Json-------------");
                  
                    res.status(200).json({ "status": "success", "message": "Added Successfully!!" });

                })
            })

        })

    } catch (error) {
        console.log(error);
    }

});


//edit existing data
router.post('/EditLookup', (req, res) => {
    debugger;
    console.log('API calling..' + req.body.lookupName);
    console.log('lookupID API calling..' + req.body.lookupID);
    //res.status(200).json({"status":"success","data new":req.body});
    //racle.getTableName(req,res);
    try {
        pathExists('./dist/assets/data/lookup.json').then(exists => {

            var file = './dist/assets/data/lookup.json';

            jsonfile.readFile(file, function (err, obj) {
                //console.log(obj);
                console.log("----------------------------");
                var data = JSON.parse(JSON.stringify(obj));
                console.log('json: ' + data.lookupList[0].lookupID)
                //data.lookupList.push(req.body);
                //console.log(data);
                var index = '';
                for (var i = 0; i < data.lookupList.length; i++) {
                    console.log('json: ' + i + '----' + data.lookupList[i].lookupID)
                    if (req.body.lookupID == data.lookupList[i].lookupID) {
                        index = i;
                    }
                }
                console.log('have to edit item with index: ' + index);

                //assign edited data
                data.lookupList[index] = req.body;
                //write into file
                jsonfile.writeFile(file, data, function (err) {
                    console.log("---------------Edit Json-------------");
                    console.log(data);
                    res.status(200).json({ "status": "success", "message": "Edited Successfully!!" });

                })

            })

        })

    } catch (error) {
        console.log(error);
    }

});






/*updateUser
router.get('/checkdb',(req,res)=>{
console.log(req); 

var db = new JsonDB("./dist/assets/data/user.json", true, false);
var data = db.getData("./dist/assets/data/user.json/");
console.log(data);
res.status(200).json(data);
})

*/



//The second argument is used to tell the DB to save after each push 
//If you put false, you'll have to call the save() method. 
//The third argument is to ask JsonDB to save the database in an human readable format. (default false) 



router.post('/exportexceltemplate', (req, res) => {

    var jsonArr = [];
    jsonArr = req.body.data;
    debugger;
    var refId = ++dataDownloadCount;
    var xls = json2xls(jsonArr);
    var mainFile = './dist/assets/dataMonitor/' + req.body.mainDetails.ProductName + '_' + refId + '.json';

    jsonfile.writeFile(mainFile, req.body.mainDetails, function (err) {

    })
    fs.writeFileSync('./dist/assets/dataMonitor/' + req.body.mainDetails.ProductName + '_' + refId + '.xlsx', xls, 'binary');

    res.status(200).json({
        "refId": req.body.mainDetails.ProductName + '_' + refId
    });


});

router.post('/readexceldata', (req, res) => {
    debugger;
    var mainFile = './dist/assets/dataMonitor/' + req.body.fileName.split(".")[0] + '.json';
    var mainJsonFile = './dist/assets/dataUpload/' + req.body.fileName.split(".")[0] + '.json';


    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {


            const result = excelToJson({
                sourceFile: './dist/assets/dataUpload/' + req.body.fileName,
                columnToKey: {
                    '*': '{{columnHeader}}',

                }
            })

            console.log("as");
            console.log(result);
            var valid = true;
            var theadName = '';
            for (var i = 0; i < result["Sheet 1"].length; i++) {
                if (i > 0) {
                    console.log("erer")
                    for (var j = 0; j < Object.keys(result["Sheet 1"][i]).length; j++) {
                        for (var k = 0; k < obj.data[0].tableRow.length; k++) {
                            if (Object.keys(result["Sheet 1"][i])[j] == obj.data[0].tableRow[k].theadName) {
                                if (obj.data[0].tableRow[k].tdObj) {

                                    if (obj.data[0].tableRow[k].tdObj[0].tagName == 'number' && Object.values(result["Sheet 1"][i])[j] != '') {
                                        if (obj.data[0].tableRow[k].tdObj[0].tagName != (typeof Object.values(result["Sheet 1"][i])[j])) {
                                            theadName = obj.data[0].tableRow[k].theadName;
                                            valid = false;
                                        }
                                    } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'date' && Object.values(result["Sheet 1"][i])[j] != '') {
                                        var c = Object.values(result["Sheet 1"][i])[j];
                                        var ndate = new Date(c);
                                        cdate = ndate.getDate().toString().length == '1' ? '0' + ndate.getDate().toString() : ndate.getDate().toString();
                                        var incMonth = ndate.getMonth() + 1;
                                        cmonth = incMonth.toString().length == '1' ? '0' + incMonth.toString() : incMonth.toString();
                                        cyear = ndate.getFullYear().toString();
                                        wdate = cmonth + '/' + cdate + '/' + cyear;
                                        var t = new Date(wdate);
                                        if (!moment(t, 'MM/DD/YYYY', true).isValid()) {
                                            theadName = obj.data[0].tableRow[k].theadName;
                                            console.log(2)
                                            valid = false;
                                        }
                                    } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'text' && Object.values(result["Sheet 1"][i])[j] != '') {
                                        if (obj.data[0].tableRow[k].maxLength != '') {
                                            if (Number(obj.data[0].tableRow[k].maxLength) < Object.values(result["Sheet 1"][i])[j].toString().length) {
                                                theadName = obj.data[0].tableRow[k].theadName;
                                                console.log(3)
                                                valid = false;
                                            }
                                        }
                                    }

                                }
                            }
                            if (!valid) {
                                console.log("test")
                                console.log(theadName)
                                break;
                            }
                        }
                        //Insert excel data to Json
                        if (!valid) {

                            break;
                        }
                    }

                }
            }
            if (!valid) {
                var store = {
                    "data": obj,
                    "err": theadName,
                    "row": i,
                    'count': j,
                    'Length': obj.data[0].tableRow.length,
                    'user': req.body.user,
                    'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function (err) {

                })
                res.status(200).json({ 'fName': 'error' });
            }
            if (valid) {
                debugger;

                var pageArray = [];
                var accordId = obj.accordionName;
                var mainFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'

                jsonfile.readFile(mainFile, function (err, jsarray) {
                    if (jsarray) {
                        var jsarr = JSON.stringify(jsarray);
                        var jsonarray = JSON.parse(jsarr);
                        // var pageArray = jsonarray[0].PageContent;


                        var pageArray = req.body.pageData;
                        var contentArray = obj.data[0].tableRow;
                        jsonarray[0].PageContent = changeAcc(pageArray, contentArray, result, '', req.body.snArr, req.body.data.accordId);
                        //console.log("jarray" + jsonarray);
                        var postFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'


                        jsonfile.writeFile(postFile, jsonarray, function (err) {
                            console.log("done")

                            {
                                var store = {
                                    "data": obj,
                                    "err": "",
                                    "row": i,
                                    'count': j,
                                    'Length': obj.data[0].tableRow.length,
                                    'user': req.body.user,
                                    'udate': req.body.udate
                                }
                                jsonfile.writeFile(mainJsonFile, store, function (err) {

                                });

                            }
                            res.status(200).json({ 'fName': 'success', 'data': jsonarray[0].PageContent });
                        })
                    }
                });

                /*
        
                jsonfile.readFile(mainFile, function(err, jsarray) {
                if (jsarray) {
                var jsarr = JSON.stringify(jsarray);
                var jsonarray = JSON.parse(jsarr);
                var pageArray = jsonarray[0].PageContent;
                var contentArray = obj.data[0].tableRow;
                jsonarray[0].PageContent = changeAcc(pageArray, contentArray, result, '');
                console.log("jarray" + jsonarray);
                var postFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'
        
        
                jsonfile.writeFile(postFile, jsonarray, function(err) {
                console.log("done")
        
                {
                var store = {
                "data": obj,
                "err": "",
                "row": i,
                'count': j,
                'Length': obj.data[0].tableRow.length,
                'user': req.body.user,
                'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function(err) {
        
                });
        
                }
                res.status(200).json('fName :' + req.body.fileName);
                })
                }
                if (err) {
                console.log(err);
                }
        
                });
        
                */

            }
        } else {
            res.status(200).json({ 'fName': 'failure' });

            console.log(err)
        }
    });




});



/*
router.post('/readexceldata', (req, res) => {
debugger;
var mainFile = './dist/assets/dataMonitor/' + req.body.fileName.split(".")[0] + '.json';
var mainJsonFile = './dist/assets/dataUpload/' + req.body.fileName.split(".")[0] + '.json';


jsonfile.readFile(mainFile, function(err, obj) {

if (obj) {


const result = excelToJson({
sourceFile: './dist/assets/dataUpload/' + req.body.fileName,
columnToKey: {
'*': '{{columnHeader}}',

}
})

console.log("as");
console.log(result);
var valid = true;
var theadName = '';
for (var i = 0; i < result["Sheet 1"].length; i++) {
    if (i > 0) {
    console.log("erer")
    for (var j = 0; j < Object.keys(result["Sheet 1"][i]).length; j++) {
        for (var k = 0; k < obj.data[0].tableRow.length; k++) {
            if (Object.keys(result["Sheet 1"][i])[j] == obj.data[0].tableRow[k].theadName) {
            if (obj.data[0].tableRow[k].tdObj) {

            if (obj.data[0].tableRow[k].tdObj[0].tagName == 'number' && Object.values(result["Sheet 1"][i])[j] != '') {
            if (obj.data[0].tableRow[k].tdObj[0].tagName != (typeof Object.values(result["Sheet 1"][i])[j])) {
            theadName = obj.data[0].tableRow[k].theadName;
            valid = false;
            }
            } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'date' && Object.values(result["Sheet 1"][i])[j] != '') {
            if (!moment(Object.values(result["Sheet 1"][i])[j], 'MM/DD/YYYY', true).isValid()) {
            theadName = obj.data[0].tableRow[k].theadName;
            console.log(2)
            valid = false;
            }
            } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'text' && Object.values(result["Sheet 1"][i])[j] != '') {
            if (obj.data[0].tableRow[k].maxLength != '') {
            if (Number(obj.data[0].tableRow[k].maxLength) < Object.values(result["Sheet 1"][i])[j].toString().length) {
                theadName = obj.data[0].tableRow[k].theadName;
                console.log(3)
                valid = false;
                }
                }
                }

                }
                }
                if (!valid) {
                console.log("test")
                console.log(theadName)
                break;
                }
                }
                //Insert excel data to Json
                if (!valid) {

                break;
                }
                }

                }
                }
                if (!valid) {
                var store = {
                "data": obj,
                "err": theadName,
                "row": i,
                'count': j,
                'Length': obj.data[0].tableRow.length,
                'user': req.body.user,
                'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function(err) {

                })
                res.status(200).json("fName:error");

                }
                if (valid) {
                debugger;

                var pageArray = [];
                var accordId = obj.accordionName;
                var mainFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'

                jsonfile.readFile(mainFile, function(err, jsarray) {
                if (jsarray) {
                var jsarr = JSON.stringify(jsarray);
                var jsonarray = JSON.parse(jsarr);
                var pageArray = jsonarray[0].PageContent;
                var contentArray = obj.data[0].tableRow;
                jsonarray[0].PageContent = changeAcc(pageArray, contentArray, result, '');
                console.log("jarray" + jsonarray);
                var postFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'
                jsonfile.writeFile(postFile, jsonarray, function(err) {
                console.log("done")

                {
                var store = {
                "data": obj,
                "err": "",
                "row": i,
                'count': j,
                'Length': obj.data[0].tableRow.length,
                'user': req.body.user,
                'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function(err) {

                });

                }
                res.status(200).json('fName :success');
                })
                }
                if (err) {
                console.log(err);
                }

                });



                }
                }
                });




                });
                */

router.post('/readDataUploadFiles', (req, res) => {

    debugger;
    readDirFiles.read('./dist/assets/dataUpload/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        var fileName = Object.keys(files);
        var jsonArr = [];

        var count = 0;
        var readCount = 0;
        for (var i = 0; i < Object.keys(files).length; i++) {
            if (Object.keys(files)[i].includes('.json') && Object.keys(files)[i].includes(req.body.productName)) {
                var output = JSON.parse(files[Object.keys(files)[i]]);
                var details = {
                    "refid": Object.keys(files)[i].split("_")[1].split(".")[0],
                    "fileName": Object.keys(files)[i],
                    "ProductName": req.body.productName,
                    "PageName": output.data.PageName,
                    "blockId": output.data.blockId,
                    "accordionName": output.data.accordionName,
                    "err": output.err,
                    "row": output.row,
                    "count": output.Length,
                    'user': output.user,
                    'udate': output.udate
                }
                jsonArr.push(details);


            }

        }
        res.status(200).json(jsonArr);

    });




});
router.post('/readdataDownloadJsonFiles', (req, res) => {

    debugger;
    readDirFiles.read('./dist/assets/dataMonitor/', 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        var fileName = Object.keys(files);
        var jsonArr = [];

        var count = 0;
        var readCount = 0;
        for (var j = 0; j < Object.keys(files).length; j++) {
            if (Object.keys(files)[j].includes('.json') && Object.keys(files)[j].includes(req.body.productName)) {
                var output = JSON.parse(files[Object.keys(files)[j]]);
                var details = {
                    "refid": Object.keys(files)[j].split("_")[1].split(".")[0],
                    "fileName": Object.keys(files)[j],
                    "ProductName": req.body.productName,
                    "PageName": output.PageName,
                    "blockId": output.blockId,
                    "accordionName": output.accordionName,
                    "err": output.err,
                    "row": output.row,
                    "count": output.Length,
                    "user": output.user,
                    "date": output.date,
                    "mode": output.mode,
                    "recipients": output.recipients
                }
                jsonArr.push(details);




            }

        }
        res.status(200).json(jsonArr);

    });
});




function changeAcc(pageArrayOld, eData, eveValue, flag, snArr, accordId) {
    debugger;
    var check = JSON.stringify(pageArrayOld);

    var check1 = JSON.parse(check);
    eveValue["Sheet 1"].splice(0, 1);
    for (var a = 0; a < check1.length; a++) {

        if (check1[a].blockId == eData[0].blockId && check1[a].accordId == accordId) {
            if (check1[a].tableObj) {
                //table
                var tableStringify = JSON.stringify(check1[a].tableObj[0].tableRow);
                var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                for (var test = check1[a].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                    for (qa = 0; qa < tableData.length; qa++) {
                        if (tableData[qa].subPage) {
                            for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                if (tableData[qa].subPage[qb].tableObj) {
                                    for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                        if (qc == 0) {
                                            for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                    for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                        }

                                                    }

                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                    if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                    }
                                                } else {
                                                    for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                            for (var md = 0; md < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; md++) {
                                                                if (md == 0) {
                                                                    for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                            for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                }
                                                                            }
                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                        }
                                                                    }
                                                                } else {
                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, md);
                                                                }
                                                            }
                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                            for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                    for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                        }
                                                                    }
                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                        } else {
                                            tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                        }
                                    }
                                } else if (tableData[qa].subPage[qb].field) {
                                    for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                        if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                            if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                    if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                        tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                            if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                tableData[qa].subPage[qb].field[xb].tagValue = '';
                                            }
                                        }
                                    }
                                }
                            }



                        }
                    }

                    check1[a].tableObj.push({
                        "sna": test,
                        "tableRow": tableData
                    });


                }
                var s = JSON.stringify(check1);
                var s1 = JSON.parse(s);

                for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {


                    for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                        for (var tj = 0; tj < s1[a].tableObj[xcx].tableRow.length; tj++) {
                            if (s1[a].tableObj[xcx].tableRow[tj].theadName != undefined)// Modified by Bhagavathy RFA 36
                                if (s1[a].tableObj[xcx].tableRow[tj].theadName.trim() == Object.keys(eveValue["Sheet 1"][xcx])[bw]) { // Modified by Bhagavathy RFA 36
                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj) {

                                        for (var nj = 0; nj < s1[a].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                            if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {

                                                s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;
                                                //Added by Bhagavathy RFA 36
                                                if (s1[a].tableObj[xcx].tableRow[tj].id == "uq_subquestapp") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "Y") {
                                                        s1[a].tableObj[xcx].tableRow[3].navDisable = null;
                                                    }else if(s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "N"){
                                                        s1[a].tableObj[xcx].tableRow[3].navDisable = true;
                                                    }
                                                } else if (s1[a].tableObj[xcx].tableRow[tj].id == "pepo_excesspaymentoption") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "TP") {
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[2].displayAcc = true;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[3].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[0].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[1].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].navDisable = null;
                                                    } else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "TP-R") {
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[3].displayAcc = true;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[2].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[0].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[1].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].navDisable = null;
                                                    } else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "PD") {
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[0].displayAcc = true;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[2].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[3].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[1].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].navDisable = null;
                                                    } else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "FP") {
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[1].displayAcc = true;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[2].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[3].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].subPage[0].displayAcc = false;
                                                        s1[a].tableObj[xcx].tableRow[5].navDisable = null;
                                                    }
                                                } else if (s1[a].tableObj[xcx].tableRow[tj].id == "sap_sapattern") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "IF") {
                                                        s1[a].tableObj[xcx].tableRow[4].navDisable = null;
                                                    }else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "L") {
                                                        s1[a].tableObj[xcx].tableRow[4].navDisable = true;
                                                    }else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "VN") {
                                                        s1[a].tableObj[xcx].tableRow[4].navDisable = true;
                                                    }
                                                } else if (s1[a].tableObj[xcx].tableRow[tj].id == "or_laapplicable") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "V") {
                                                        s1[a].tableObj[xcx].tableRow[32].navDisable = null;
                                                    }else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "A") {
                                                        s1[a].tableObj[xcx].tableRow[32].navDisable = true;
                                                    }
                                                }
                                                else if (s1[a].tableObj[xcx].tableRow[tj].id == "or_relapplicable") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "V") {
                                                        s1[a].tableObj[xcx].tableRow[33].navDisable = null;
                                                    }else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "A") {
                                                        s1[a].tableObj[xcx].tableRow[33].navDisable = true;
                                                    }
                                                }
                                                else if (s1[a].tableObj[xcx].tableRow[tj].id == "or_prim_rider") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "Y") {
                                                        s1[a].tableObj[xcx].tableRow[30].navDisable = null;
                                                    }else if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "N") {
                                                        s1[a].tableObj[xcx].tableRow[30].navDisable = true;
                                                    }
                                                }
                                                else if (s1[a].tableObj[xcx].tableRow[tj].id == "vl_limitapplicable") {
                                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "Y") {
                                                        s1[a].tableObj[xcx].tableRow[15].navDisable = null;
                                                    }else if(s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "N"){
                                                        s1[a].tableObj[xcx].tableRow[15].navDisable = true;
                                                    }
                                                }
                                                
                                                //Added by Bhagavathy RFA 36

                                            } else {
                                                s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                            }
                                            //Added by Bhagavathy RFA 36
                                            if (s1[a].tableObj[xcx].tableRow[tj].blockId == "B61_OPN_M") {
                                                if (s1[6].blockId == "B60_1_OPN_M")
                                                    for (var premiumpay = 0; premiumpay < s1[6].field[0].optionObj.length; premiumpay++) {
                                                        if (s1[6].field[0].optionObj[premiumpay].optionValue == Object.values(eveValue["Sheet 1"])[0]["Premium Paying Term Type"])
                                                            s1[6].field[0].optionObj[premiumpay].selected = true;
                                                    }
                                            }
                                            //Added by Bhagavathy RFA 36

                                        }



                                    } //Added by Bhagavathy RFA 36
                                    else if(s1[a].tableObj[xcx].tableRow[tj].id == "dp_DeferPeriodtype"){
                                            for (var nji = 0; nji < s1[1].field[4].optionObj.length; nji++){
                                                    if (Object.values(eveValue["Sheet 1"][xcx])[bw] == s1[1].field[4].optionObj[nji].optionValue) {
                                                        s1[1].field[4].optionObj[nji].selected = true;
                                                    }
                                                }
                                    }
                                    //Ended by Bhagavathy RFA 36
                                    else {
                                        if (s1[a].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {

                                            /*
                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw].setDate(Object.values(eveValue["Sheet 1"][xcx])[bw].getDate() + 1)).toISOString().substr(0, 10);
                                                                                */

                                            var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                            var applyDate = new Date(givenDate);
                                            if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                applyDate.setDate(applyDate.getDate() + 1)
                                            }

                                            applyDate.setDate(applyDate.getDate() + 1)

                                            s1[a].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                            //s1[a].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                        } else {
                                            s1[a].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                        }
                                    }


                                }

                        }
                    }
                }
                debugger;

            }




        } else {
            if (check1[a].field) {
                for (var g = 0; g < check1[a].field.length; g++) {
                    if (check1[a].field[g].subPage) {
                        for (var za = 0; za < check1[a].field[g].subPage.length; za++) {
                            if (check1[a].field[g].subPage[za].blockId == eData[0].blockId && check1[a].field[g].subPage[za].accordId == accordId) {

                                //added  field-table
                                var slength = check1[a].field[g].subPage[za].tableObj;
                                if (check1[a].field[g].subPage[za].tableObj) {
                                    var tableStringify = JSON.stringify(check1[a].field[g].subPage[za].tableObj[0].tableRow);
                                    var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                    for (var test = check1[a].field[g].subPage[za].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                        for (qa = 0; qa < tableData.length; qa++) {
                                            if (tableData[qa].subPage) {
                                                for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                    if (tableData[qa].subPage[qb].tableObj) {
                                                        for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                            if (qc == 0) {
                                                                for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                        for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                            }

                                                                        }

                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                        if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                        }
                                                                    } else {
                                                                        for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                for (var me = 0; me < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; me++) {
                                                                                    if (me == 0) {
                                                                                        for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                    }
                                                                                                }
                                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.slice(1, mp);
                                                                                    }
                                                                                }
                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                        for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                            }
                                                                                        }
                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }

                                                            } else {
                                                                tableData[qa].subPage[qb].tableObj.splice(qc, 1);
                                                            }
                                                        }
                                                    } else if (tableData[qa].subPage[qb].field) {
                                                        for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                            if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                    for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                        if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                            tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                    tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                }
                                                            }
                                                        }
                                                    }
                                                }



                                            }
                                        }
                                        check1[a].field[g].subPage[za].tableObj.push({
                                            "snb": test,
                                            "tableRow": tableData
                                        });


                                    }

                                    debugger;
                                    var s = JSON.stringify(check1);
                                    var s1 = JSON.parse(s);

                                    for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                        console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                        for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                            for (var tj = 0; tj < s1[a].field[g].subPage[za].tableObj[xcx].tableRow.length; tj++) {
                                                if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                    if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj) {

                                                        for (var nj = 0; nj < s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {

                                                            if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;
                                                                //Added by Bhagavathy RFA 36 10/07/2019
                                                                if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].id == "pc_RelationCriteria") {
                                                                    if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "V") {
                                                                        s1[a].field[g].subPage[za].tableObj[xcx].tableRow[17].navDisable = null;
                                                                    }else if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "A") {
                                                                        s1[a].field[g].subPage[za].tableObj[xcx].tableRow[17].navDisable = true;
                                                                    }
                                                                }
                                                                //Ended  by Bhagavathy RFA 36 10/07/2019

                                                            } else {
                                                                s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                            }
                                                        }



                                                    } else {
                                                        if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                            var givenDate = Object.values(eveValue["Sheet 1"][xcx])[bw];

                                                            var applyDate = new Date(givenDate);
                                                            if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                            }


                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                            s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);



                                                        } else {
                                                            s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                        }
                                                    }


                                                }
                                            }
                                        }
                                    }
                                    debugger;


                                }



                            } else {
                                if (check1[a].field[g].subPage[za].field) {
                                    for (var h = 0; h < check1[a].field[g].subPage[za].field.length; h++) {
                                        if (check1[a].field[g].subPage[za].field[h].subPage) {
                                            for (var zc = 0; zc < check1[a].field[g].subPage[za].field[h].subPage.length; zc++) {
                                                if (check1[a].field[g].subPage[za].field[h].subPage[zc].blockId == eData[0].blockId && check1[a].field[g].subPage[za].field[h].subPage[zc].accordId == accordId) {
                                                    //added  field-subpage-field

                                                    if (check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj) {
                                                        var tableStringify = JSON.stringify(check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[0].tableRow);
                                                        var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                        for (var test = check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                            for (qa = 0; qa < tableData.length; qa++) {
                                                                if (tableData[qa].subPage) {
                                                                    for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                        if (tableData[qa].subPage[qb].tableObj) {
                                                                            for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                if (qc == 0) {
                                                                                    for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                            for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                }

                                                                                            }

                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                            if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                            }
                                                                                        } else {
                                                                                            for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                    for (var mh = 0; mh < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mh++) {
                                                                                                        if (mh == 0) {
                                                                                                            for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                    for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                }
                                                                                                            }
                                                                                                        } else {
                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mh);
                                                                                                        }
                                                                                                    }
                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                    for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                            for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                }
                                                                                                            }
                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }

                                                                                } else {
                                                                                    tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                }
                                                                            }
                                                                        } else if (tableData[qa].subPage[qb].field) {
                                                                            for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                    if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                        for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                            if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                break;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                    if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                        tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }



                                                                }
                                                            }

                                                            check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj.push({
                                                                "sna": test,
                                                                "tableRow": tableData
                                                            });


                                                        }
                                                        var s = JSON.stringify(check1);
                                                        var s1 = JSON.parse(s);

                                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                            console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                for (var tj = 0; tj < s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow.length; tj++) {
                                                                    if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                        if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj) {

                                                                            for (var nj = 0; nj < s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                    s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                } else {
                                                                                    s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                }
                                                                            }



                                                                        } else {
                                                                            if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                var applyDate = new Date(givenDate);
                                                                                if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                    applyDate.setDate(applyDate.getDate() + 1)
                                                                                }

                                                                                applyDate.setDate(applyDate.getDate() + 1)

                                                                                s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                            } else {
                                                                                s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                            }
                                                                        }


                                                                    }
                                                                }
                                                            }
                                                        }


                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else if (check1[a].field[g].subPage[za].tableObj) {
                                    for (var i = 0; i < check1[a].field[g].subPage[za].tableObj.length; i++) {
                                        if (check1[a].field[g].subPage[za].tableObj[i].snb == snArr[1].snb)
                                            for (var j = 0; j < check1[a].field[g].subPage[za].tableObj[i].tableRow.length; j++) {
                                                if (check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage) {
                                                    for (var zd = 0; zd < check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage.length; zd++) {
                                                        if (check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].blockId == eData[0].blockId && check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].accordId == accordId) {
                                                            //added field-subpage-table-subpage-table

                                                            if (check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj) {
                                                                var tableStringify = JSON.stringify(check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[0].tableRow);
                                                                var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                for (var test = check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                    for (qa = 0; qa < tableData.length; qa++) {
                                                                        if (tableData[qa].subPage) {
                                                                            for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                if (tableData[qa].subPage[qb].tableObj) {
                                                                                    for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                        if (qc == 0) {
                                                                                            for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                    for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                        }

                                                                                                    }

                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                    if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                    }
                                                                                                } else {
                                                                                                    for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                            for (var mi = 0; mi < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mi++) {
                                                                                                                if (mi == 0) {
                                                                                                                    for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                            for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mi);
                                                                                                                }
                                                                                                            }
                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                            for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                    for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                            }

                                                                                        } else {
                                                                                            tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                        }
                                                                                    }

                                                                                } else if (tableData[qa].subPage[qb].field) {
                                                                                    for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                        if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                            if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                    if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                        tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                        break;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                            if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }



                                                                        }
                                                                    }


                                                                    //check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj.push(tableData)
                                                                    check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj.push({
                                                                        "snc": test,
                                                                        "tableRow": tableData
                                                                    });


                                                                }
                                                                var s = JSON.stringify(check1);
                                                                var s1 = JSON.parse(s);

                                                                for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                    console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                    for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                        for (var tj = 0; tj < s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow.length; tj++) {

                                                                            if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                    for (var nj = 0; nj < s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                        if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                            s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                        } else {
                                                                                            s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                        }
                                                                                    }



                                                                                } else {
                                                                                    if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                        var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                        var applyDate = new Date(givenDate);
                                                                                        if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                                                        }

                                                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                                                        s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                    } else {
                                                                                        s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                    }
                                                                                }


                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                    }
                                }
                            }
                        }
                    }
                }



            } else if (check1[a].tableObj) {

                for (var b = 0; b < check1[a].tableObj.length; b++) {
                    if (check1[a].tableObj[b].sna == snArr[0].sna) {
                        for (var c = 0; c < check1[a].tableObj[b].tableRow.length; c++) {
                            if (check1[a].tableObj[b].tableRow[c].subPage) {
                                for (var zb = 0; zb < check1[a].tableObj[b].tableRow[c].subPage.length; zb++) {
                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].accordId == accordId) {
                                        //added table-subpage-table

                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj) {
                                            console.log('chk3')

                                            var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[0].tableRow);
                                            var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                            for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                for (qa = 0; qa < tableData.length; qa++) {
                                                    if (tableData[qa].subPage) {
                                                        for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                            if (tableData[qa].subPage[qb].tableObj) {
                                                                for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                    if (qc == 0) {
                                                                        for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                    }

                                                                                }

                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                }
                                                                            } else {
                                                                                for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                        for (var mp = 0; mp < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mp++) {
                                                                                            if (mp == 0) {
                                                                                                for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                        for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                            }
                                                                                                        }
                                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mp);
                                                                                            }
                                                                                        }
                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                        for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                    }
                                                                                                }
                                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        }

                                                                    } else {
                                                                        tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                    }
                                                                }

                                                            } else if (tableData[qa].subPage[qb].field) {
                                                                for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                    if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                        if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                            for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                    tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                        if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                            tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }



                                                    }
                                                }



                                                check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj.push({
                                                    "snb": test,
                                                    "tableRow": tableData
                                                });
                                                console.log(check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj)



                                            }
                                            var s = JSON.stringify(check1);
                                            var s1 = JSON.parse(s);

                                            for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {



                                                for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                    for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow.length; tj++) {
                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj) {

                                                                for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;
                                                                        //Added by Bhagavathy RFA 36 17/07/2019
                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].id == "parentProp_bt_lienapplicable") {
                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "Y") {
                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[10].navDisable = null;
                                                                            }else if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "N") {
                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[10].navDisable = true;
                                                                            }
                                                                        }
                                                                        //Ended by Bhagavathy RFA 36 17/07/2019

                                                                    } else {
                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                    }
                                                                }



                                                            } else {
                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                    var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                    var applyDate = new Date(givenDate);
                                                                    if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                                    }

                                                                    applyDate.setDate(applyDate.getDate() + 1)
                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);

                                                                    //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                } else {
                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                }
                                                            }


                                                        }
                                                    }
                                                }
                                            }
                                        }






                                    } else {
                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].field) {
                                            for (var d = 0; d < check1[a].tableObj[b].tableRow[c].subPage[zb].field.length; d++) {
                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage) {
                                                    for (var ze = 0; ze < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage.length; ze++) {
                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].accordId == accordId) {

                                                            //added table-subpage field
                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj) {

                                                                var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[0].tableRow);
                                                                var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                    for (qa = 0; qa < tableData.length; qa++) {
                                                                        if (tableData[qa].subPage) {
                                                                            for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                if (tableData[qa].subPage[qb].tableObj) {
                                                                                    for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                        if (qc == 0) {
                                                                                            for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                    for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                        }

                                                                                                    }

                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                    if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                    }
                                                                                                } else {
                                                                                                    for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                            for (var mk = 0; mk < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mk++) {
                                                                                                                if (mk == 0) {
                                                                                                                    for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                            for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mk);
                                                                                                                }
                                                                                                            }
                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                            for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                    for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                            }

                                                                                        } else {
                                                                                            tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                        }
                                                                                    }

                                                                                } else if (tableData[qa].subPage[qb].field) {
                                                                                    for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                        if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                            if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                    if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                        tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                        break;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                            if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }



                                                                        }
                                                                    }


                                                                    //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.push(tableData)
                                                                    check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.push({
                                                                        "sna": test,
                                                                        "tableRow": tableData
                                                                    });


                                                                }
                                                                var s = JSON.stringify(check1);
                                                                var s1 = JSON.parse(s);

                                                                for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                    console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                    for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                        for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow.length; tj++) {

                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                    for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                        } else {
                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                        }
                                                                                    }



                                                                                } else {
                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                        var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                        var applyDate = new Date(givenDate);
                                                                                        if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                                                        }

                                                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);



                                                                                    } else {
                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                    }
                                                                                }


                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }



                                                        } else {
                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj) {

                                                                for (var e = 0; e < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.length; e++) {
                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].snc == snArr[2].snc) {
                                                                        for (var f = 0; f < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow.length; f++) {
                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage) {
                                                                                //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow
                                                                                for (var zf = 0; zf < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage.length; zf++) {
                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].accordId == accordId) {

                                                                                        console.log('here');
                                                                                        //added table-subpage-field-table
                                                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj) {
                                                                                            var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[0].tableRow);
                                                                                            var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                                            for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                                                for (qa = 0; qa < tableData.length; qa++) {
                                                                                                    if (tableData[qa].subPage) {
                                                                                                        for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                                            if (tableData[qa].subPage[qb].tableObj) {
                                                                                                                for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                                                    if (qc == 0) {
                                                                                                                        for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                                                for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                                                    }

                                                                                                                                }

                                                                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                                                if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                                                        for (ml = 0; ml < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; ml++) {
                                                                                                                                            if (ml == 0) {
                                                                                                                                                for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                                                        for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, ml);
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                                                        for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                                                for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }

                                                                                                                        }

                                                                                                                    } else {
                                                                                                                        tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                                                    }
                                                                                                                }

                                                                                                            } else if (tableData[qa].subPage[qb].field) {
                                                                                                                for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                                                    if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                                                        if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                                            for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                                                if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                                                    tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                                                    break;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                                                        if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                                            tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }



                                                                                                    }
                                                                                                }


                                                                                                //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.push(tableData)
                                                                                                check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.push({
                                                                                                    "snd": test,
                                                                                                    "tableRow": tableData
                                                                                                });



                                                                                            }
                                                                                            var s = JSON.stringify(check1);
                                                                                            var s1 = JSON.parse(s);

                                                                                            for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                                                                console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                                                for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                                                    for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow.length; tj++) {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                                for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                                    } else {
                                                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                                    }
                                                                                                                }



                                                                                                            } else {
                                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                                    var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                                    var applyDate = new Date(givenDate);
                                                                                                                    if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                    }

                                                                                                                    applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                                    //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                } else {
                                                                                                                    console.log("comg")
                                                                                                                    console.log(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                }
                                                                                                            }


                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }


                                                                                        }



                                                                                    }
                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj) {

                                                                                        for (var ef = 0; ef < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.length; ef++) {
                                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].snd == snArr[3].snd) {
                                                                                                for (var ff = 0; ff < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow.length; ff++) {
                                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage) {
                                                                                                        //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow
                                                                                                        for (var zff = 0; zff < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage.length; zff++) {
                                                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].accordId == accordId) {



                                                                                                                //added table-subpage-field-subpage-table-subpage-table
                                                                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj) {
                                                                                                                    var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[0].tableRow);
                                                                                                                    var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                                                                    for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                                                                        for (qa = 0; qa < tableData.length; qa++) {
                                                                                                                            if (tableData[qa].subPage) {
                                                                                                                                for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                                                                    if (tableData[qa].subPage[qb].tableObj) {
                                                                                                                                        for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                                                                            if (qc == 0) {
                                                                                                                                                for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                                                                        for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                                                                            }

                                                                                                                                                        }

                                                                                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                                                                        if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                                                                                for (var mm = 0; mm < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mm++) {
                                                                                                                                                                    if (mm == 0) {
                                                                                                                                                                        for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                                                                                for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    } else {
                                                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mm);
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                                                                                for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                                                                    if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                                                                        for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                                                                            if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }

                                                                                                                                                }

                                                                                                                                            } else {
                                                                                                                                                tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                                                                            }
                                                                                                                                        }

                                                                                                                                    } else if (tableData[qa].subPage[qb].field) {
                                                                                                                                        for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                                                                            if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                                                                                if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                                                                    for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                                                                        if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                                                                            tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                                                                            break;
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                                                                                if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                                                                    tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }



                                                                                                                            }
                                                                                                                        }



                                                                                                                        //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj.push(tableData)
                                                                                                                        check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj.push({
                                                                                                                            "sne": test,
                                                                                                                            "tableRow": tableData
                                                                                                                        });



                                                                                                                    }
                                                                                                                    var s = JSON.stringify(check1);
                                                                                                                    var s1 = JSON.parse(s);

                                                                                                                    for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                                                                                        console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                                                                        for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                                                                            for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow.length; tj++) {
                                                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                                    console.log("match");
                                                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                                                        for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                                                            } else {
                                                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                                                            }
                                                                                                                                        }



                                                                                                                                    } else {
                                                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                                                            var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                                                            var applyDate = new Date(givenDate);
                                                                                                                                            if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                                            }

                                                                                                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                                                            //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                                        } else {

                                                                                                                                            console.log(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                                        }
                                                                                                                                    }


                                                                                                                                }
                                                                                                                            }
                                                                                                                        }

                                                                                                                    }
                                                                                                                }



                                                                                                            }




                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }




                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }

                                                        }


                                                    }
                                                }
                                            }




                                        } else if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj) {

                                            for (var e = 0; e < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj.length; e++) {
                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].snb == snArr[1].snb) {
                                                    for (var f = 0; f < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow.length; f++) {
                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage) {
                                                            for (var zf = 0; zf < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage.length; zf++) {
                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].accordId == accordId) {

                                                                    //added table-subpage-table
                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj) {
                                                                        var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[0].tableRow);
                                                                        var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                        for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                            for (qa = 0; qa < tableData.length; qa++) {
                                                                                if (tableData[qa].subPage) {
                                                                                    for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                        if (tableData[qa].subPage[qb].tableObj) {
                                                                                            for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                                if (qc == 0) {
                                                                                                    for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                            for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                                }

                                                                                                            }

                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                            if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                            }
                                                                                                        } else {
                                                                                                            for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                                    for (var mq = 0; mq < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mq++) {
                                                                                                                        if (mq == 0) {
                                                                                                                            for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                                    for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mq);
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                                    for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                            for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }

                                                                                                    }
                                                                                                } else {
                                                                                                    tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                                }
                                                                                            }

                                                                                        } else if (tableData[qa].subPage[qb].field) {
                                                                                            for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                                if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                                    if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                        for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                            if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                                tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                                break;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                                    if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                        tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }



                                                                                }
                                                                            }



                                                                            // check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.push(tableData)
                                                                            check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.push({
                                                                                "snc": test,
                                                                                "tableRow": tableData
                                                                            });


                                                                        }
                                                                        var s = JSON.stringify(check1);
                                                                        var s1 = JSON.parse(s);

                                                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                            console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                                for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow.length; tj++) {

                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                            for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;
                                                                                                    //Added by Bhagavathy RFA 36 12/07/2019
                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].id == "ld_la") {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "S") {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[12].navDisable = null;//FINISHED
                                                                                                        }else if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "A") {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[12].navDisable = true;//FINISHED
                                                                                                        }
                                                                                                    }
                                                                                                    //Ended by Bhagavathy RFA 36 12/07/2019
                                                                                                    //Added by Bhagavathy RFA 36 17/07/2019 RISK COVERAGE
                                                                                                    else if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].id == "childProp_bt_lienapplicable") {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "Y") {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[10].navDisable = null;
                                                                                                        }else  if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "N") {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[10].navDisable = true;
                                                                                                        }
                                                                                                    }
                                                                                                    else if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].id == "childProp_hl_limitfreq") {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "05") {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[10].navDisable = null;
                                                                                                        }else if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "01" ||
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "02" ||
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "03" ||
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == "04" )
                                                                                                        {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[10].navDisable = true;
                                                                                                        }
                                                                                                    }
                                                                                                    //Ended by Bhagavathy RFA 36 17/07/2019 RISK COVERAGE

                                                                                                } else {
                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                }
                                                                                            }



                                                                                        } else {
                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                var applyDate = new Date(givenDate);
                                                                                                if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                                    applyDate.setDate(applyDate.getDate() + 1)
                                                                                                }

                                                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                            } else {
                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                            }
                                                                                        }


                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                    }



                                                                } else {
                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field) {
                                                                        for (var vd = 0; vd < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field.length; vd++) {
                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage) {


                                                                                for (var ve = 0; ve < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage.length; ve++) {
                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj) {
                                                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].accordId == accordId) {
                                                                                            //added tavle-subpage-table-subpage-field
                                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj) {
                                                                                                var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[0].tableRow);
                                                                                                var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                                                for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                                                    for (qa = 0; qa < tableData.length; qa++) {
                                                                                                        if (tableData[qa].subPage) {
                                                                                                            for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                                                if (tableData[qa].subPage[qb].tableObj) {
                                                                                                                    for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                                                        if (qc == 0) {
                                                                                                                            for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                                                    for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                                                        }

                                                                                                                                    }

                                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                                                    if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                                                        tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                                                            for (var mw = 0; mw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mw++) {
                                                                                                                                                if (mw == 0) {
                                                                                                                                                    for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                                                            for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mw);
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                                                            for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                                                    for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }

                                                                                                                            }

                                                                                                                        } else {
                                                                                                                            tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                                                        }
                                                                                                                    }

                                                                                                                } else if (tableData[qa].subPage[qb].field) {
                                                                                                                    for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                                                        if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                                                            if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                                                for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                                                    if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                                                        tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                                                        break;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                                                            if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                                                tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }



                                                                                                        }
                                                                                                    }




                                                                                                    check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj.push({
                                                                                                        "sna": test,
                                                                                                        "tableRow": tableData
                                                                                                    });

                                                                                                }
                                                                                                var s = JSON.stringify(check1);
                                                                                                var s1 = JSON.parse(s);

                                                                                                for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                                                    console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                                                    for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                                                        for (var tj = 0; tj < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow.length; tj++) {
                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                                    for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                                        } else {
                                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                                        }
                                                                                                                    }



                                                                                                                } else {
                                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                                        var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                                        var applyDate = new Date(givenDate);
                                                                                                                        if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                        }

                                                                                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                                        //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                    } else {
                                                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                    }
                                                                                                                }


                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                }
                                                                                            }

                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    } else if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj) {

                                                                        for (var va = 0; va < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.length; va++) {
                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].snc == snArr[2].snc) {
                                                                                for (var vb = 0; vb < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow.length; vb++) {
                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage) {
                                                                                        for (var vc = 0; vc < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage.length; vc++) {
                                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj) {
                                                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].accordId == accordId) {

                                                                                                    //added table-subpage-table-subpage-table

                                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj) {
                                                                                                        var tableStringify = JSON.stringify(check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[0].tableRow);
                                                                                                        var tableData = JSON.parse(tableStringify);                                    //check1[a].field[g].subPage[za].tableObj = [];
                                                                                                        for (var test = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj.length; test < eveValue["Sheet 1"].length; test++) {
                                                                                                            for (qa = 0; qa < tableData.length; qa++) {
                                                                                                                if (tableData[qa].subPage) {
                                                                                                                    for (qb = 0; qb < tableData[qa].subPage.length; qb++) {
                                                                                                                        if (tableData[qa].subPage[qb].tableObj) {
                                                                                                                            for (var qc = 0; qc < tableData[qa].subPage[qb].tableObj.length; qc++) {
                                                                                                                                if (qc == 0) {
                                                                                                                                    for (var qd = 0; qd < tableData[qa].subPage[qb].tableObj[0].tableRow.length; qd++) {
                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj) {
                                                                                                                                            for (var qe = 0; qe < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj.length; qe++) {
                                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected) {
                                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].optionObj[qe].selected = false;

                                                                                                                                                }

                                                                                                                                            }

                                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tagName != 'button') {
                                                                                                                                            if ((tableData[qa].subPage[qb].tableObj[0].tableRow[qd].display == true && tableData[qa].subPage[qb].tableObj[0].tableRow[qd].defaultValue == undefined) || (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].autoGenerated)) {
                                                                                                                                                tableData[qa].subPage[qb].tableObj[0].tableRow[qd].tdObj[0].tdValue = '';
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            for (var qs = 0; qs < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage.length; qs++) {
                                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj) {
                                                                                                                                                    for (var mr = 0; mr < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.length; mr++) {
                                                                                                                                                        if (mr == 0) {
                                                                                                                                                            for (var qt = 0; qt < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[0].tableRow.length; qt++) {
                                                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj) {
                                                                                                                                                                    for (var qu = 0; qu < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj.length; qu++) {
                                                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected) {
                                                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].optionObj[qu].selected = false;
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tagName != 'button') {
                                                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj[qt].tdObj[0].tdValue = 0;
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        } else {
                                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].tableObj.splice(1, mr);
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field) {
                                                                                                                                                    for (var qv = 0; qv < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field.length; qv++) {
                                                                                                                                                        if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj) {
                                                                                                                                                            for (var qw = 0; qw < tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj.length; qw++) {
                                                                                                                                                                if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected) {
                                                                                                                                                                    tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].optionObj[qw].selected = false;
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        } else if (tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tagName != 'button') {
                                                                                                                                                            tableData[qa].subPage[qb].tableObj[0].tableRow[qd].subPage[qs].field[qv].tdObj[0].tdValue = '';
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }

                                                                                                                                    }

                                                                                                                                } else {
                                                                                                                                    tableData[qa].subPage[qb].tableObj.splice(1, qc);
                                                                                                                                }
                                                                                                                            }

                                                                                                                        } else if (tableData[qa].subPage[qb].field) {
                                                                                                                            for (var xb = 0; xb < tableData[qa].subPage[qb].field.length; xb++) {
                                                                                                                                if (tableData[qa].subPage[qb].field[xb].optionObj) {
                                                                                                                                    if (tableData[qa].subPage[qb].field[xb].defaultValue == undefined) {
                                                                                                                                        for (var xc = 0; xc < tableData[qa].subPage[qb].field[xb].optionObj.length; xc++) {
                                                                                                                                            if (tableData[qa].subPage[qb].field[xb].optionObj[xc].selected) {
                                                                                                                                                tableData[qa].subPage[qb].field[xb].optionObj[xc].selected = false;
                                                                                                                                                break;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                } else if (tableData[qa].subPage[qb].field[xb].tagName != 'button') {
                                                                                                                                    if ((tableData[qa].subPage[qb].field[xb].display == true && tableData[qa].subPage[qb].field[xb].defaultValue == undefined) || (tableData[qa].subPage[qb].field[xb].autoGenerated)) {
                                                                                                                                        tableData[qa].subPage[qb].field[xb].tagValue = '';
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }



                                                                                                                }
                                                                                                            }




                                                                                                            check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj.push({
                                                                                                                "snd": test,
                                                                                                                "tableRow": tableData
                                                                                                            });



                                                                                                        }
                                                                                                        var s = JSON.stringify(check1);
                                                                                                        var s1 = JSON.parse(s);

                                                                                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                                                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
                                                                                                                for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow.length; tj++) {

                                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                                            for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                                                } else {
                                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                                                }
                                                                                                                            }



                                                                                                                        } else {
                                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                                                var applyDate = new Date(givenDate);
                                                                                                                                if ((applyDate.getFullYear() == '1899' && applyDate.getMonth() == "11") || (applyDate.getFullYear() == '1900' && applyDate.getMonth() == '0')) {
                                                                                                                                    applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                                }

                                                                                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                                                //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                            } else {
                                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                            }
                                                                                                                        }


                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }

                                                                                                    }


                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    console.log(s1);
    return s1;

}


// CreateTemplate
router.post('/createPublishCloneTemplate', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder
    debugger;

    console.log("DDDDDDDDDDDDDDDDDD")
    pathExists('./dist/assets/publish/' + req.body[0].TemplateId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/publish/' + req.body[0].TemplateId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});


// CreateTemplate
router.post('/dataTemplateClone', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder


    debugger;

    var oldTemplate = JSON.stringify(req.body[0].templateFull);
    var newTemplate = JSON.parse(oldTemplate);

    for (var z = 0; z < req.body[0].selectedPlanCode.length; z++) {
        for (var y = 0; y < Object.keys(newTemplate).length; y++) {
            if (req.body[0].selectedPlanCode[z].templateId != Object.keys(newTemplate)[y]) {

                delete newTemplate[Object.keys(newTemplate)[y]];
                --y;
            }
        }
    }


    var pageList = [];
    var output = [];
    var deloutput = [];

    for (var i = 0; i < req.body[0].selectedPlanCode.length; i++) {
        for (var j = 0; j < Object.keys(newTemplate).length; j++) {
            if (req.body[0].selectedPlanCode[i].templateId == Object.keys(newTemplate)[j]) {

                for (var k = 0; k < Object.keys(newTemplate[Object.keys(newTemplate)[j]]).length; k++) {
                    for (var m = 0; m < req.body[0].selectedPages.length; m++) {
                        for (var n = 0; n < req.body[0].selectedPages[m].length; n++) {

                            if ((req.body[0].selectedPages[m][n].data.PageName == Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k])) {
                                if (req.body[0].selectedPages[m][n].data.enableBlock) {
                                    var ok = [];
                                    var pageName = "";
                                    output.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent);

                                    for (var q = 0; q < JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent.length; q++) {

                                        for (var p = 0; p < req.body[0].selectedPages[m][n].data.PageContent.length; p++) {
                                            if (req.body[0].selectedPages[m][n].data.PageContent[p].checked) {

                                                //console.log(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent)

                                                if (req.body[0].selectedPages[m][n].data.PageContent[p].blockId == JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q].blockId) {
                                                    ok.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q])
                                                    pageName = JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageName;
                                                } else {
                                                    var s = newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];
                                                    var f = JSON.parse(s);
                                                    // 							console.log(f[0].PageContent[q])
                                                    //f[0].PageContent.splice(0,q);

                                                }

                                            }


                                        }
                                    }
                                    var t = [];
                                    t.push({ "PageContent": ok, "PageName": pageName, "TemplateId": req.body[0].nTemplate[0].TemplateId })
                                    var file = './dist/assets/publish/' + req.body[0].nTemplate[0].TemplateId + '/' + t[0].PageName;
                                    jsonfile.writeFile(file, t, function (err) {
                                        //console.log("temp copied");
                                        //res.status(200).json(req.body);
                                    })
                                    //newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]]=JSON.stringify(t);



                                } else {
                                    deloutput.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0])
                                    //delete newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];


                                    //--k;

                                }

                            }

                        }

                    }
                }
            } else {

            }
            break;
        }
    }




});


router.post('/createTmpCloneTemplate', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder
    debugger;

    pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

        if (exists) {
            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })

        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});

router.post('/dataTmpTemplateClone', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB

    //create folder
    debugger;

    var oldTemplate = JSON.stringify(req.body[0].templateFull);
    var newTemplate = JSON.parse(oldTemplate);

    //   for (var z = 0; z < req.body[0].selectedPlanCode.length; z++) {
    //     for (var y = 0; y < Object.keys(newTemplate).length; y++) {
    //       if (req.body[0].selectedPlanCode[z].templateId != Object.keys(newTemplate)[y]) {

    //         delete newTemplate[Object.keys(newTemplate)[y]];
    //         --y;
    //       }
    //     }
    //   }


    var pageList = [];
    var output = [];
    var deloutput = [];

    for (var i = 0; i < req.body[0].selectedPlanCode.length; i++) {
        for (var j = 0; j < Object.keys(newTemplate).length; j++) {
            if (req.body[0].selectedPlanCode[i].templateId == Object.keys(newTemplate)[j]) {

                for (var k = 0; k < Object.keys(newTemplate[Object.keys(newTemplate)[j]]).length; k++) {
                    for (var m = 0; m < req.body[0].selectedPages.length; m++) {
                        for (var n = 0; n < req.body[0].selectedPages[m].length; n++) {

                            if ((req.body[0].selectedPages[m][n].data.PageName == Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k])) {
                                if (req.body[0].selectedPages[m][n].data.enableBlock) {
                                    var ok = [];
                                    var pageName = "";
                                    output.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent);

                                    for (var q = 0; q < JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent.length; q++) {

                                        for (var p = 0; p < req.body[0].selectedPages[m][n].data.PageContent.length; p++) {
                                            if (req.body[0].selectedPages[m][n].data.PageContent[p].checked) {

                                                //console.log(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent)

                                                if (req.body[0].selectedPages[m][n].data.PageContent[p].blockId == JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q].blockId) {
                                                    ok.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q])
                                                    pageName = JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageName;
                                                } else {
                                                    var s = newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];
                                                    var f = JSON.parse(s);
                                                    // 							console.log(f[0].PageContent[q])
                                                    //f[0].PageContent.splice(0,q);

                                                }

                                            }


                                        }
                                    }

                                    //                   var file = './dist/assets/tmp/' + req.body[0].nTemplate[0].TemplateId + '/' + t[0].PageName;
                                    //                   jsonfile.writeFile(file, t, function (err) {

                                    //                   })
                                    //newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]]=JSON.stringify(t);

                                    var t = [];
                                    t.push({ "PageContent": ok, "PageName": pageName, "TemplateId": req.body[0].nTemplate[0].TemplateId })
                                    var file = './dist/assets/tmp/' + req.body[0].nTemplate[0].TemplateId + '/' + t[0].PageName;
                                    jsonfile.writeFile(file, t[0].PageContent, function (err) {

                                    })

                                } else {
                                    deloutput.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0])
                                    //delete newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];


                                    //--k;

                                }

                            }

                        }

                    }
                }
            } else {

            }
            //break;
        }
        if (i == req.body[0].selectedPlanCode.length - 1) {
            var postData = { "eMailTo": req.body[0].emailData.eMailTo, "message": req.body[0].emailData.message };
            var options = {
                method: 'post',
                body: postData,
                json: true,
                url: wsUrl.web.url + "ProductBenefits/SendCloningEMail",
                headers: {

                }
            }

            request(options, function (err, res, body) {
                if (err) {
                    console.log('Error :', err)
                    return
                }
                console.log(' Body :', body)

            });
        }
    }




});
function fnCloneAutoNullValues(fPageArray) {
    try {
        for (var a = 0; a < fPageArray.length; a++) {
            if (fPageArray[a].field) {
                //sna- (field)
                for (var na = 0; na < fPageArray[a].field.length; na++) {
                    if (fPageArray[a].field[na].tagName != 'button') {
                        if (fPageArray[a].field[na].autoGenerated != undefined
                            && fPageArray[a].field[na].autoGenerated) {
                            fPageArray[a].field[na].tagValue = '';
                        }
                    } else if (fPageArray[a].field[na].subPage) {
                        for (var zb = 0; zb < fPageArray[a].field[na].subPage.length; zb++) {
                            if (fPageArray[a].field[na].subPage[zb].field) {
                                //snb - (field-field)
                                for (var nb = 0; nb < fPageArray[a].field[na].subPage[zb].field.length; nb++) {
                                    if (fPageArray[a].field[na].subPage[zb].field[nb].tagName != 'button') {
                                        if (fPageArray[a].field[na].subPage[zb].field[nb].autoGenerated != undefined
                                            && fPageArray[a].field[na].subPage[zb].field[nb].autoGenerated) {
                                            fPageArray[a].field[na].subPage[zb].field[nb].tagValue = '';
                                        }
                                    }
                                }
                            } else if (fPageArray[a].field[na].subPage[zb].tableObj) {
                                //snb - (field-table)
                                for (var nb = 0; nb < fPageArray[a].field[na].subPage[zb].tableObj.length; nb++) {
                                    for (var nbr = 0; nbr < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow.length; nbr++) {
                                        if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].tdObj[0].tagName != 'button') {
                                            if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].autoGenerated != undefined
                                                && fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].autoGenerated) {
                                                fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].tdObj[0].tdValue = '';
                                            }
                                        } else if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage) {
                                            for (var zc = 0; zc < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage.length; zc++) {
                                                if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].field) {
                                                    //snc-- (Field-Table-Field)
                                                } else if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj) {
                                                    //snc-- (Field-Table-Table)
                                                    for (var nc = 0; nc < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj.length; nc++) {
                                                        for (var ncr = 0; ncr < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow.length; ncr++) {
                                                            if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].tdObj[0].tagName != 'button') {
                                                                if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].autoGenerated != undefined
                                                                    && fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].autoGenerated) {
                                                                    fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].tdObj[0].tdValue = '';
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (fPageArray[a].tableObj) {
                //sna-Table
                for (var na = 0; na < fPageArray[a].tableObj.length; na++) {
                    for (var nar = 0; nar < fPageArray[a].tableObj[na].tableRow.length; nar++) {
                        if (fPageArray[a].tableObj[na].tableRow[nar].tdObj[0].tagName != 'button') {
                            if (fPageArray[a].tableObj[na].tableRow[nar].autoGenerated != undefined
                                && fPageArray[a].tableObj[na].tableRow[nar].autoGenerated) {
                                fPageArray[a].tableObj[na].tableRow[nar].tdObj[0].tdValue = '';
                            }
                        } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage) {
                            for (var za = 0; za < fPageArray[a].tableObj[na].tableRow[nar].subPage.length; za++) {
                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field) {
                                    //snb-- (Table-Field)
                                    for (var m = 0; m < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field.length; m++) {
                                        if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].tagName != 'button') {
                                            if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].autoGenerated != undefined
                                                && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].autoGenerated) {
                                                fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].tagValue = '';
                                            }
                                        } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage) {
                                            for (var zb = 0; zb < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage.length; zb++) {
                                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].field) {
                                                    //snc-- (Table-Field-Field)
                                                } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj) {
                                                    //snc-- (Table-Field-Table)
                                                    for (var nc = 0; nc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj.length; nc++) {
                                                        for (var ncr = 0; ncr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow.length; ncr++) {
                                                            if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tagName != 'button') {
                                                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated != undefined
                                                                    && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated) {
                                                                    fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tdValue = '';
                                                                }
                                                            } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage) {
                                                                for (var zc = 0; zc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage.length; zc++) {
                                                                    if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].field) {
                                                                        //snd-- (Table-Field-Table-Field)
                                                                        //console.log("Snd Table-Field-Table-Field Enters");
                                                                    } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj) {
                                                                        //snd-- (Table-Field-Table-Table)
                                                                        for (var nd = 0; nd < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj.length; nd++) {
                                                                            for (var ndr = 0; ndr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow.length; ndr++) {
                                                                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tagName != 'button') {
                                                                                    if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated != undefined
                                                                                        && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated) {
                                                                                        fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tdValue = '';
                                                                                    }
                                                                                } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage) {
                                                                                    for (var zd = 0; zd < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage.length; zd++) {
                                                                                        if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].field) {
                                                                                            //sne-- (Table-Field-Table-Table-Field)
                                                                                            //console.log("Sne Table-Field-Table-Table-Field Enters");
                                                                                        } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj) {
                                                                                            //sne-- (Table-Field-Table-Table-Table)
                                                                                            for (var ne = 0; ne < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj.length; ne++) {
                                                                                                for (var ner = 0; ner < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow.length; ner++) {
                                                                                                    if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].tdObj[0].tagName != 'button') {
                                                                                                        if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].autoGenerated != undefined
                                                                                                            && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].autoGenerated) {
                                                                                                            fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].tdObj[0].tdValue = '';
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj) {
                                    //snb-- (Table-Table)
                                    for (var nb = 0; nb < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj.length; nb++) {
                                        for (var nbr = 0; nbr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow.length; nbr++) {
                                            if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].tdObj[0].tagName != 'button') {
                                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].autoGenerated != undefined
                                                    && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].autoGenerated) {
                                                    fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].tdObj[0].tdValue = '';
                                                }
                                            } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage) {
                                                for (var zb = 0; zb < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage.length; zb++) {
                                                    if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field) {
                                                        //snc-- (Table-Table-field)
                                                        for (var nc = 0; nc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field.length; nc++) {
                                                            if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].tagName != 'button') {
                                                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].autoGenerated != undefined
                                                                    && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].autoGenerated) {
                                                                    fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].tagValue = '';
                                                                }
                                                            }
                                                        }
                                                    } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj) {
                                                        //snc-- (Table-Table-table)
                                                        for (var nc = 0; nc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj.length; nc++) {
                                                            for (var ncr = 0; ncr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow.length; ncr++) {
                                                                if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tagName != 'button') {
                                                                    if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated != undefined
                                                                        && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated) {
                                                                        fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tdValue = '';
                                                                    }
                                                                } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage) {
                                                                    for (var zc = 0; zc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage.length; zc++) {
                                                                        if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].field) {
                                                                            //snd -- (Table-Table-Table-Field)
                                                                        } else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj) {
                                                                            //snd -- (Table-Table-Table-Table)
                                                                            for (var nd = 0; nd < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj.length; nd++) {
                                                                                for (var ndr = 0; ndr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow.length; ndr++) {
                                                                                    if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tagName != 'button') {
                                                                                        if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated != undefined
                                                                                            && fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated) {
                                                                                            fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tdValue = '';
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } catch (err) {
        throw err;
    }
    return fPageArray;
}
router.post('/prodPageInsertMultiple', (req, res) => {

    req.body.forEach(
        function (body) {
            var mainFile = './dist/assets/productpublish/' + body.ProductId + '/' + body.ProductName + '.json';
            jsonfile.readFile(mainFile, function (err, obj) {
                if (obj) {
                    if (obj[0].ProductId == body.ProductId && obj[0].ProductId == body.ProductName) {
                        obj[0] = body.Svalue;
                        jsonfile.writeFile(mainFile, obj, function (err) {
                            res.status(200).json(obj);
                        });
                    }

                } else {

                }
            });
        }
    );
});
router.post('/prodTmpInsertMultiple', (req, res) => {

    pathExists('./dist/assets/productpublish/' + req.body.ProductId).then(exists => {
        if (exists) {
            //Post Json	
            var file = './dist/assets/productpublish/' + req.body.ProductId + '/' + req.body.PageName;
            jsonfile.readFile(file, function (err, obj) {
                if (obj) {
                    if (obj[0].ProductId == req.body.ProductId && obj[0].PageName == req.body.PageName) {
                        obj[0].PageContent = req.body.PageContent;
                        jsonfile.writeFile(file, obj, function (err) {
                            res.status(200).json(obj);
                        })
                    }
                } else {
                    //	res.status(200).json(({"output":"notok"}));
                }
            });
        } else {
            mkdirp('./dist/assets/productpublish/' + req.body.ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json	
                    var file = './dist/assets/productpublish/' + req.body.ProductId + '/' + req.body.PageName;
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })
                }
            });
        }
    });
});

router.post('/dataPublishPage', (req, res) => {
    // Get posts from the mock api
    // This should ideally be replaced with a service that connects to MongoDB


    //create folder

    pathExists('./dist/assets/productpublish/' + req.body[0].ProductId).then(exists => {

        if (exists) {

            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].PageName;
            jsonfile.writeFile(file, req.body, function (err) {
                res.status(200).json(req.body);
            })


        }

        else {

            //var folderName=Math.floor(Math.random()*1000)+1;

            mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    //Post Json
                    //var file = './tmp/data2.json'
                    var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].PageName;
                    jsonfile.writeFile(file, req.body, function (err) {
                        res.status(200).json(req.body);
                    })

                }
            });
        }

    });
});
router.post('/productpublishPageNameInsert', (req, res) => {

    var mainFile = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            var ok = false;
            if (obj[0].PageList.length > 0) {
                for (var i = 0; i < obj[0].PageList.length; i++) {
                    console.log(obj[0].PageList[i].name);
                    console.log(req.body[0].PageName);
                    if (obj[0].PageList[i].name === req.body[0].PageName) {
                        console.log("match");
                        obj[0].PageList[i].review = false;
                        obj[0].PageList[i].rejected = false;
                        obj[0].PageList[i].approveBy = '';
                        obj[0].PageList[i].modifiedBy = req.body[0].ModifiedBy;
                        obj[0].PageList[i].ModifiedDate = req.body[0].ModifiedDate;
                        obj[0].PageList[i].SaveValue = req.body[0].Svalue;
                        obj[0].PageList[i].Updated = true;

                        ok = true;
                        jsonfile.writeFile(mainFile, obj, function (err) {
                            res.status(200).json({ "review": false });
                        })
                    }

                }
                if (!ok) {
                    obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "rejected": false, "approveBy": '', "modifiedBy": req.body[0].ModifiedBy, "ModifiedDate": req.body[0].ModifiedDate, "SaveValue": req.body[0].Svalue, "Updated": false });
                    jsonfile.writeFile(mainFile, obj, function (err) {
                        res.status(200).json({ "review": false });
                    })
                }
            } else {
                var modifyDate = new Date();
                obj[0]["ModifiedDate"] = modifyDate.toLocaleString();
                obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "rejected": false, "approveBy": '', "modifiedBy": req.body[0].ModifiedBy, "ModifiedDate": req.body[0].ModifiedDate, "SaveValue": req.body[0].Svalue, "Updated": req.body[0].Updated });
                jsonfile.writeFile(mainFile, obj, function (err) {
                    res.status(200).json({ "review": false });
                })
            }


            //res.status(200).json(obj);
        } else {
            //	res.status(200).json(({"output":"notok"}));
        }

    })

})

router.post('/prodpublishApprove', (req, res) => {


    var mainFile = './dist/assets/productpublish/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            if (obj[0].PageList.length > 0) {
                var count = 0;
                for (var i = 0; i < obj[0].PageList.length; i++) {

                    if (obj[0].PageList[i].name == req.body.PageName) {
                        if (req.body.PageReview == "Approved") {
                            ++count;
                            obj[0].PageList[i].review = true;
                            obj[0].PageList[i].rejected = false;
                            obj[0].PageList[i].approveBy = req.body.ApproveBy;
                        } else if (req.body.PageReview == "Rejected") {
                            obj[0].PageList[i].review = false;
                            obj[0].PageList[i].rejected = true;
                            obj[0].PageList[i].approveBy = '';
                        }

                    }
                    if (i == obj[0].PageList.length - 1) {

                        jsonfile.writeFile(mainFile, obj, function (err) {
                            if (err) {
                                res.status(200).json({ "output": "fail" })
                            } else {
                                res.status(200).json({ "output": "pass" })
                            }
                        })
                    }
                }
            }
        }
    })


});
router.post('/readProductPublishTemplateFiles', (req, res) => {



    readDirFiles.read('./dist/assets/productpublish/' + req.body.folderId, 'UTF-8', function (err, files) {
        if (err) return console.dir(err);
        res.status(200).send(files);




    });

});

router.post('/productpublishupdateStatus', (req, res) => {

    var mainFile = './dist/assets/productpublish/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            var ok = false;
            if (obj[0].PageList.length > 0) {
                for (var i = 0; i < obj[0].PageList.length; i++) {
                    console.log(obj[0].PageList[i].name);
                    console.log(req.body.PageName);
                    console.log("match");
                    obj[0].PageList[i]["Updated"] = false;


                    ok = true;


                    if (i == obj[0].PageList.length - 1) {
                        jsonfile.writeFile(mainFile, obj, function (err) {
                            res.status(200).json({ "status": 'pass' });
                            console.log('pass');
                        })
                    }

                }

            }


        } else {
        }

    })

})


router.post('/productRefreshTmp', (req, res) => {

    var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            debugger;
            console.log(obj)
            res.status(200).json({ "latestData": obj });


        } else {
        }

    })

})

router.post('/productRefreshPublish', (req, res) => {

    var mainFile = './dist/assets/productpublish/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

        if (obj) {
            debugger;
            console.log(obj)
            res.status(200).json({ "latestData": obj });


        } else {
        }

    })

})
//Added by Bhagavathy 30/05/2019
//Post Data page
router.post('/dataUpdateTmpPage', (req, res) => {

    console.log('TEST............');
    pathExists('./dist/assets/productUpdatepublish/' + req.body[0].ProductId).then(exists => {
        rmdir('./dist/assets/productUpdatepublish/' + req.body[0].ProductId, function (err, dirs, files) {
            if (err) {
                console.log(err);
            } else {
                mkdirp('./dist/assets/productUpdatepublish/' + req.body[0].ProductId, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        //Post Json
                        //var file = './tmp/data2.json'
                        var obj;
                        var mainFile = './dist/assets/productUpdatepublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
                        // obj[0].Reviews.push(req.body);
                        jsonfile.writeFile(mainFile, req.body, function (err) {
                            res.status(200).json({ "ProductId": "TEST", "Status": "UPDATED" })
                        })
                    }
                });

            }
        });

    });

})

//Added by Bhagavathy 30/05/2019
//Post Data page
router.post('/dataInsertTmpPage', (req, res) => {
    pathExists('./dist/assets/productUpdatepublish/' + req.body[0].ProductId).then(exists => {

        mkdirp('./dist/assets/productUpdatepublish/' + req.body[0].ProductId, function (err) {
            if (err) {
                console.log(err);
            } else {
                //Post Json

                var obj;
                var mainFile = './dist/assets/productUpdatepublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';

                // obj[0].Reviews.push(req.body);

                jsonfile.writeFile(mainFile, req.body, function (err) {
                    res.status(200).json({ "output": "pass" })
                })

            }
        })




    })
})


// Added by Bhagavathy 29/05/2019
//ReadProductPublishFiles
router.post('/readProductUpdateStatus', (req, res) => {

    console.log(req.body[0].folderId);
    var file = './dist/assets/productUpdatepublish/' + req.body[0].folderId + '/' + req.body[0].folderId + '.json';
    jsonfile.readFile(file, function (err, obj) {
        if (obj) {
            res.status(200).json(obj);
        } else {
            res.status(200).json(({ "ProductId": "TEST", "Status": "UPDATED" }));
        }

    })
});



module.exports = router;
