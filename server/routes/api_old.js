const express = require('express');
const router = express.Router();
var mkdirp = require('mkdirp');
var fs = require('fs');
const pathExists = require('path-exists');
var jsonfile = require('jsonfile');
var ls = require('list-directory-contents');
var readDirFiles = require('read-dir-files');
var rmdir = require('rmdir');
var waitUntil = require('wait-until');
var find = require('arraysearch').Finder;
var oracle = require('../../oracleDb/lookUpConn');
var wsUrl = require('../../dist/assets/data/property.json');
var saveAs = require('file-saver');
var XLSX = require("xlsx");
var json2xls = require('json2xls');
var multer = require('multer');
var xlsxj = require("xlsx-to-json-depfix");
const excelToJson = require('convert-excel-to-json');
var request = require('request');
var upload = multer({
  dest: './dist/assets/font-image/'
});
var moment = require('moment');

dataDownloadCount = 0;
//var mkdirp = require('mkdirp');

// declare axios for making http requests
const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';

var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, './dist/assets/dataUpload/');
  },
  filename: function (req, file, cb) {
    console.log(file);
    cb(null, file.originalname);
    /*
    var datetimestamp = Date.now();
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    */
  }
});
var upload = multer({ //multer settings
  storage: storage
}).single('file');

/* API path that will upload the files */
router.post('/upload', function (req, res) {
  console.log(1);
  upload(req, res, function (err) {
    console.log(req.file);
    if (err) {
      res.json({
        error_code: 1,
        err_desc: err
      });
      return;
    }
    res.json({
      error_code: 0,
      err_desc: null
    });
  });
});



// Template

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});


// Read FolderList
router.get('/readFolders', (req, res) => {


  readDirFiles.read('./dist/assets/publish/', 'UTF-8', function (err, filenames) {
    if (err) return console.dir(err);
    res.status(200).send(filenames);
  });


})

// Read directory fileSize
router.get('/readFiles', (req, res) => {



  readDirFiles.read('./dist/assets/publish/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);


  });
})

// Get all posts
router.post('/posts', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder

  pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});



// CreateTemplate
router.post('/createTemplate', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder

  pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});







//Post Template page
router.post('/tempPage', (req, res) => {

  pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

    if (exists) {


      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].PageName + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].PageName + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});

router.post('/tempPageNameInsert', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      var ok = false;
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name === req.body[0].PageName) {
            obj[0].PageList[i].review = false;
            ok = true;
            jsonfile.writeFile(mainFile, obj, function (err) {
              console.log(1);
              res.status(200).json({ "review": false });

            })
            break;
          }

        }
        if (!ok) {
          console.log(2);
          obj[0].PageList.push({ "name": req.body[0].PageName, "review": false });
          jsonfile.writeFile(mainFile, obj, function (err) {
            res.status(200).json({ "review": false });
          })
        }
      } else {
        console.log(3);
        obj[0].PageList.push({ "name": req.body[0].PageName, "review": false });
        jsonfile.writeFile(mainFile, obj, function (err) {
          res.status(200).json({ "review": false });
        })
      }


      //res.status(200).json(obj);
    } else {
      //	res.status(200).json(({"output":"notok"}));
    }

  })

})



router.post('/productPageNameInsert', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      var ok = false;
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {
          console.log(obj[0].PageList[i].name);
          console.log(req.body[0].PageName);
          if (obj[0].PageList[i].name === req.body[0].PageName) {
            console.log("match");
            obj[0].PageList[i].review = false;
            obj[0].PageList[i].rejected = false;
            obj[0].PageList[i].approveBy = '';
            obj[0].PageList[i].modifiedBy = req.body[0].ModifiedBy;
            obj[0].PageList[i].ModifiedDate = req.body[0].ModifiedDate;
            obj[0].PageList[i].SaveValue = req.body[0].Svalue;
            ok = true;
            jsonfile.writeFile(mainFile, obj, function (err) {
              res.status(200).json({ "review": false });
            })
          }

        }
        if (!ok) {
          obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "rejected": false, "approveBy": '', "modifiedBy": req.body[0].ModifiedBy, "ModifiedDate": req.body[0].ModifiedDate, "SaveValue": req.body[0].Svalue });
          jsonfile.writeFile(mainFile, obj, function (err) {
            res.status(200).json({ "review": false });
          })
        }
      } else {
        var modifyDate = new Date();
        obj[0]["ModifiedDate"] = modifyDate.toLocaleString();
        obj[0].PageList.push({ "name": req.body[0].PageName, "review": false, "rejected": false, "approveBy": '', "modifiedBy": req.body[0].ModifiedBy, "ModifiedDate": req.body[0].ModifiedDate, "SaveValue": req.body[0].Svalue });
        jsonfile.writeFile(mainFile, obj, function (err) {
          res.status(200).json({ "review": false });
        })
      }


      //res.status(200).json(obj);
    } else {
      //	res.status(200).json(({"output":"notok"}));
    }

  })

})


//Path exists
router.post('/tempPageExists', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB


  //create folder
  var file = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
  jsonfile.readFile(file, function (err, obj) {
    if (obj) {
      res.status(200).json(obj);
    } else {
      res.status(200).json(({ "output": "notok" }));
    }

  })

});





//moveTmpFiles
router.post('/moveTmpFiles', (req, res) => {

  var moveFile = false;
  pathExists('./dist/assets/publish/' + req.body[0].folderId).then(exists => {
    if (exists) {
      moveFile = true;
    } else {


      mkdirp('./dist/assets/publish/' + req.body[0].folderId, function (err) {
        if (err) {
          console.log(err);
        } else {
          moveFile = true;
        }
      });
    }
  });

  waitUntil()
    .interval(500)
    .times(7)
    .condition(function () {
      return (moveFile ? true : false);
    })
    .done(function (result) {
      readDirFiles.read('./dist/assets/tmp/' + req.body[0].folderId, 'UTF-8', function (err, files) {
        var fname = Object.keys(files);
        for (var i = 0; i < fname.length; i++) {
          if (JSON.parse(files[fname[i]])[0].ProductType) {
            var modified = JSON.parse(files[fname[i]]);
            var modifyDate = new Date();
            modified[0]["ModifiedDate"] = modifyDate.toLocaleString();

            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/publish/' + req.body[0].folderId + '/' + modified[0].TemplateName + '.json';
            jsonfile.writeFile(file, modified, function (err) {
              //res.status(200).json(req.body);
            })

          } else {

            var tFiles = [];
            tFiles.push({ "TemplateId": req.body[0].folderId, "PageName": Object.keys(files)[i], "PageContent": JSON.parse(files[Object.keys(files)[i]]) })
            var file = './dist/assets/publish/' + tFiles[0].TemplateId + '/' + tFiles[0].PageName;
            jsonfile.writeFile(file, tFiles, function (err) {
              //res.status(200).json(req.body);
            })

          }
          if (fname.length - 1 == i) {
            rmdir('./dist/assets/tmp/' + req.body[0].folderId, function (err, dirs, files) {

              res.status(200).json({ "output": "done" });

            });
          }
        }

      });

    })
});





//tmptopublishtemplate

router.post('/tmptopublishTemplate', (req, res) => {

  pathExists('./dist/assets/publish/' + req.body[0].TemplateId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + '.json';
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/publish/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + '.json';
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});


//tmptopublishfiles
router.post('/tmptopublish', (req, res) => {

  pathExists('./dist/assets/publish/' + req.body[0].TemplateId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].PageName;
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/publish/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].PageName;
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});



//movePublishFiles
router.post('/movePublishFiles', (req, res) => {



  pathExists('./dist/assets/tmp/' + req.body[0].folderId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName;
      jsonfile.writeFile(file, req.body[0].PageContent, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/tmp/' + req.body[0].folderId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName;
          jsonfile.writeFile(file, req.body[0].PageContent, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });

});



//Publish
router.post('/published', (req, res) => {
  console.log("publish");



  rmdir('./dist/assets/tmp/' + req.body.TemplateId, function (err, dirs, files) {

    console.log('all files are removed');
  });





});



//copyPublishFiles
router.post('/copyPublishFiles', (req, res) => {


  pathExists('./dist/assets/tmp/' + req.body[0].folderId).then(exists => {

    if (exists) {

      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);

        /*
        rmdir('./dist/assets/tmp/'+req.body[0].TemplateId, function (err, dirs, files) {
    
        console.log('all files are removed');
        });
        */
      })


    } else {



      mkdirp('./dist/assets/tmp/' + req.body[0].folderId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/tmp/' + req.body[0].folderId + '/' + req.body[0].PageName + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);

            /*
            rmdir('./dist/assets/tmp/'+req.body[0].TemplateId, function (err, dirs, files) {
        
            console.log('all files are removed');
            });
            */

          })

        }
      });
    }

  });
});


//removeDir
/*
var path = '/path/to/the/dir';

rmdir(path + '/assets', function (err, dirs, files) {
console.log(dirs);
console.log(files);
console.log('all files are removed');
});
*/


//get json
// var jsonfile = require('jsonfile')
//   var file = './tmp/data2.json'
// jsonfile.readFile(file, function (err, obj) {
//   console.log(1);
//   res.status(200).json(obj);
// })


// axios.get(`${API}/posts`)
//   .then(posts => {


//       res.status(200).json(posts.data);

//  })
//   .catch(error => {
//     res.status(500).send(error)
//   });



//tmp folders
router.get('/getTmpFolders', (req, res) => {

  //List Directory
  readDirFiles.read('./dist/assets/tmp/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files).end();




  });
});

//publish folders
router.get('/getPublishFolders', (req, res) => {


  //List Directory
  readDirFiles.read('./dist/assets/publish/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files).end();


  });

});





//getTempJsonFiles
router.post('/getTmpFolders/tmp', (req, res) => {
  console.log(req.body[0].folderId);

  ls('./dist/assets/tmp/' + req.body[0].folderId + '/', function (err, tree) {

    res.send(tree);
  });


});



//getPublishFiles
router.post('/getPublishFiles/publish', (req, res) => {



  readDirFiles.read('./dist/assets/publish/' + req.body.folderId, 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);


  });
})


//ReadPublishFiles
router.post('/readPublishFiles', (req, res) => {



  readDirFiles.read('./dist/assets/publish/' + req.body.folderId, 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);




  });

});

//ReadTemplateFiles
router.post('/readTemplateFiles', (req, res) => {



  readDirFiles.read('./dist/assets/tmp/' + req.body.folderId, 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);




  });

});



// Data


// Read FolderList
router.get('/readProductPublishFolders', (req, res) => {


  readDirFiles.read('./dist/assets/productpublish/', 'UTF-8', function (err, filenames) {
    if (err) return console.dir(err);
    res.status(200).send(filenames);
  });


})

//Post Data page
router.post('/dataTmpPage', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB


  //create folder

  pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

    if (exists) {

      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].PageName;
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })


    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].PageName;
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});



//Post Data page
router.post('/dataProductClone', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB


  //create folder

  pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      debugger;
       for (var i = 0; i < req.body[0].SelectedPages.length; i++) {
      for (var j = 0; j < req.body[0].SelectedPages[i].length; j++) {
        if (req.body[0].SelectedPages[i][j].data.enableBlock) {
          for (var k = 0; k < Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]]).length; k++) {
            if (JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].TemplateName) {
              delete req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]]
            } else {
              for (var n = 0; n < req.body[0].SelectedPages[i][j].data.PageContent.length; n++) {
                if (req.body[0].SelectedPages[i][j].data.PageContent[n].checked) {
                  for (var p = 0; p < JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].PageContent.length; p++) {

                    if (JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].PageContent[p].blockId == req.body[0].SelectedPages[i][j].data.PageContent[n].blockId) {
                      //console.log(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])
                      console.log(JSON.stringify(req.body[0].SelectedPages[i][j].data.PageContent[n].blockId))
                      var f = req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]];
                      var a = JSON.parse(f);

                      a[0].PageContent[p] = req.body[0].SelectedPages[i][j].data.PageContent[n];

                      req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]] = JSON.stringify(a);

                      //var s=JSON.parse(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])[0].PageContent[p]
                      //console.log(req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]])
                      //               var d = [];
                      //               d.push(req.body[0].SelectedPages[i][j].data);
                      //req.body[0].TemplateContent[0][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[k]] = JSON.stringify(d);

                    }
                  }
                }
              }
            }


          }
        }
      }
    
        if (i == req.body[0].SelectedPages.length - 1) {





          for (var h = 0; h < Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]]).length; h++) {

            var pageName = JSON.parse(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[h]])[0].PageName;
            var pageContent = JSON.parse(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]][Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]])[h]])[0].PageContent;
			/*
try{
							pageContent = fnCloneAutoNullValues(pageContent);
						}catch(err){
							console.log("Exception occured while autogenerated values make null.PageName:--"+pageName);
						}
						*/
            var dataContent = [];
            dataContent.push({ "ProductId": req.body[0].ProductId, "PageName": pageName, "PageContent": pageContent });

            var file = './dist/assets/producttmp/' + dataContent[0].ProductId + '/' + dataContent[0].PageName;
            jsonfile.writeFile(file, dataContent, function (err) {

            })
            if (h == Object.keys(req.body[0].TemplateContent[Object.keys(req.body[0].TemplateContent)[0]]).length - 1) {
              var postData = { "eMailTo": req.body[0].emailData.eMailTo, "message": req.body[0].emailData.message };
              var options = {
                method: 'post',
                body: postData,
                json: true,
                url: wsUrl.web.url + "ProductBenefits/SendCloningEMail",
                headers: {

                }
              }

              request(options, function (err, res, body) {
                if (err) {
                  console.log('Error :', err)
                  return
                }
                console.log(' Body :', body)

              });
              /*
              console.log("succeess");
              res.status(200).json({"status":"success"});
              */
            }
          }
        }

        else {

          //res.status(200).json({"status":"fail"});
        }
      }
    }
  });
});


router.get('/getProductTmpFolders', (req, res) => {

  //List Directory
  readDirFiles.read('./dist/assets/producttmp/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);




  });
});

router.get('/getProductTmpJsonFiles', (req, res) => {

  //List Directory
  readDirFiles.read('./dist/assets/producttmp/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    var fileName = Object.keys(files);
    var fileNameArray = [];
    for (var i = 0; i < fileName.length; i++) {
      fileNameArray.push(JSON.parse(files[Object.keys(files)[i]][Object.keys(files)[i] + ".json"])[0]);
    }
    res.status(200).json(fileNameArray);

  });
});

//publish folders
router.get('/getProductPublishFolders', (req, res) => {


  //List Directory
  readDirFiles.read('./dist/assets/productpublish/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);


  });

});

router.get('/getProductPublishJsonFiles', (req, res) => {


  //List Directory
  readDirFiles.read('./dist/assets/productpublish/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    var fileName = Object.keys(files);
    var fileNameArray = [];
    for (var i = 0; i < fileName.length; i++) {
      fileNameArray.push(JSON.parse(files[Object.keys(files)[i]][Object.keys(files)[i] + ".json"])[0]);
    }
    res.status(200).json(fileNameArray);


  });

});

// posts data
router.post('/dataPosts', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder

  pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});



//moveProductTmpFiles
router.post('/moveProductTmpFiles', (req, res) => {


  var moveFile = false;
  pathExists('./dist/assets/productpublish/' + req.body[0].folderId).then(exists => {
    if (exists) {
      moveFile = true;
    } else {


      mkdirp('./dist/assets/productpublish/' + req.body[0].folderId, function (err) {
        if (err) {
          console.log(err);
        } else {
          moveFile = true;
        }
      });
    }
  });

  waitUntil()
    .interval(500)
    .times(7)
    .condition(function () {
      return (moveFile ? true : false);
    })
    .done(function (result) {
      readDirFiles.read('./dist/assets/producttmp/' + req.body[0].folderId, 'UTF-8', function (err, files) {
        var fname = Object.keys(files);
        for (var i = 0; i < fname.length; i++) {
          if (JSON.parse(files[fname[i]])[0].ProductType) {
            var modified = JSON.parse(files[fname[i]]);
            modified[0].Publish = true;
            var file = './dist/assets/producttmp/' + req.body[0].folderId + '/' + modified[0].ProductId + '.json';
            jsonfile.writeFile(file, modified, function (err) {
              //res.status(200).json(req.body);
            })


            var modifyDate = new Date();
            modified[0]["ModifiedDate"] = modifyDate.toLocaleString();

            //Post Json
            //var file = './tmp/data2.json'
            var file = './dist/assets/productpublish/' + req.body[0].folderId + '/' + modified[0].ProductId + '.json';
            jsonfile.writeFile(file, modified, function (err) {
              //res.status(200).json(req.body);
            })

          } else {

            for (var j = 0; j < req.body[0].pageRelease.length; j++) {
              if (req.body[0].pageRelease[j].name == Object.keys(files)[i]) {
                var tFiles = [];
                tFiles.push({ "ProductId": req.body[0].folderId, "PageName": Object.keys(files)[i], "PageContent": JSON.parse(files[Object.keys(files)[i]]) })

                var file = './dist/assets/productpublish/' + tFiles[0].ProductId + '/' + tFiles[0].PageName;
                jsonfile.writeFile(file, tFiles[0].PageContent, function (err) {

                })
              }
            }


          }

          if (fname.length - 1 == i) {
            console.log("delete tmp");
            res.status(200).json({ "output": "done" });

            rmdir('./dist/assets/producttmp/' + req.body[0].folderId, function (err, dirs, files) {


            });

          }

        }

      });

    })

});


//tmptopublishtemplate

router.post('/tmpProductPublishTemplate', (req, res) => {

  pathExists('./dist/assets/productpublish/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body[0].PageContent);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});


//tmptopublishfiles
router.post('/tmpProductToPublishFiles', (req, res) => {

  pathExists('./dist/assets/productpublish/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].PageName;
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].PageName;
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});


//publish to producttmp

//tmptopublishfiles
router.post('/producttoproducttemp', (req, res) => {

  pathExists('./dist/assets/producttmp/' + req.body.ProductId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.PageName;
      jsonfile.writeFile(file, req.body.PageContent, function (err) {
        res.status(200).json(req.body.PageContent);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/producttmp/' + req.body.ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.PageName;
          jsonfile.writeFile(file, req.body.PageContent, function (err) {
            res.status(200).json(req.body.PageContent);
          })

        }
      });
    }

  });
});



//Publish
router.post('/productPublished', (req, res) => {
  console.log("publish");



  rmdir('./dist/assets/producttmp/' + req.body.FolderId, function (err, dirs, files) {

    console.log('all files are removed');
  });





});

//getPublishFiles
router.post('/getProductPublishFiles/publish', (req, res) => {



  readDirFiles.read('./dist/assets/productpublish/' + req.body.folderId, 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);


  });
})

// CreateTemplate
router.post('/createPublishTemplate', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder

  pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].ProductId + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});

//moveProductPublishFiles
router.post('/moveProductPublishFiles', (req, res) => {



  pathExists('./dist/assets/producttmp/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      //Post Json	
      //var file = './tmp/data2.json'
      var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].PageName;
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/producttmp/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/producttmp/' + req.body[0].ProductId + '/' + req.body[0].PageName;
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });

});

//ReadProductPublishFiles
router.post('/readProductPublishFiles', (req, res) => {



  readDirFiles.read('./dist/assets/productpublish/' + req.body.folderId, 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);




  });

});


//ReadProductTemplateFiles
router.post('/readProductTemplateFiles', (req, res) => {



  readDirFiles.read('./dist/assets/producttmp/' + req.body.folderId, 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);




  });

});

// Get all posts
router.post('/masterCreate', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder

  pathExists('./dist/assets/master/').then(exists => {

    if (exists) {

      var file = './dist/assets/master/' + req.body[0].MasterId + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        console.log("exists master");
        res.status(200).json(req.body);
      })

    }



  });
});

router.get('/getTempMaster', (req, res) => {

  //List Directory
  readDirFiles.read('./dist/assets/master/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    res.status(200).send(files);




  });
});

//getPublishFiles

router.post('/getMaster', (req, res) => {
  console.log(req);
  var file = './dist/assets/master/' + req.body.id + '.json';
  jsonfile.readFile(file, function (err, obj) {
    if (obj) {
      res.status(200).json(obj);
    } else {
      res.status(200).json(({ "output": "notok" }));
    }

  })


})


router.post('/getMasterDatas', (req, res) => {
  console.log(req);
  var file = './dist/assets/master/' + req.body[0].id + '.json';
  jsonfile.readFile(file, function (err, obj) {
    if (obj) {
      res.status(200).json(obj);
    } else {
      res.status(200).json(({ "output": "notok" }));
    }

  }
  )


})

router.post('/postGroup', (req, res) => {
  console.log(req);
  var file = './dist/assets/data/groups.json';
  jsonfile.writeFile(file, req.body, function (err) {
    res.status(200).json(req.body);
  })


})

router.post('/createUser', (req, res) => {
  var file = './dist/assets/data/user.json';
  jsonfile.readFile(file, function (err, obj) {
    if (obj) {
      var usernameValid = true;
      for (var i = 0; i < obj.length; i++) {
        console.log(obj[i].username);
        if (obj[i].username == req.body.username) {
          usernameValid = false;
        }


      }
      if (usernameValid) {

        obj.push(req.body);
        jsonfile.writeFile(file, obj, function (err) {
          res.status(200).json({ "output": "done" });
        })


      }
    } else {
      res.status(200).json(({ "output": "notok" }));
    }



  })
})

router.post('/updateUser', (req, res) => {
  var file = './dist/assets/data/user.json';
  jsonfile.readFile(file, function (err, obj) {
    if (obj) {
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].username == req.body.username) {
          obj[i] = req.body;
          jsonfile.writeFile(file, obj, function (err) {
            res.status(200).json({ "output": "done" });

          })


        }



      }
    } else {
      res.status(200).json(({ "output": "notok" }));
    }



  })
})


router.post('/login', (req, res) => {
  var file = './dist/assets/data/user.json';
  jsonfile.readFile(file, function (err, obj) {
    if (obj) {
      var available = false;
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].username == req.body.username && obj[i].password == req.body.pwd) {
          available = true;
          var udate = new Date(obj[i].expiredate);
          var tdate = new Date();
          var today = new Date(tdate.toISOString().substr(0, 10))
          if (udate < today) {
            console.log("Expired");
            available = false;
          } else {
            if (obj[i].status == "Enabled") {
              res.status(200).json(obj[i]);
            } else {
              res.status(200).json({ "output": "disabled" });

            }
          }


        }


      }
      if (!available) {
        res.status(200).json({ "output": "fail" });

      }

    } else {
      res.status(200).json({ "output": "fail" });
    }



  })
})



router.post('/delTmpFolder', (req, res) => {


  rmdir('./dist/assets/tmp/' + req.body.foldername, function (err, dirs, files) {

    res.status(200).json({ "output": "Deleted" });
  });





});

router.post('/delPublishFolder', (req, res) => {


  rmdir('./dist/assets/publish/' + req.body.foldername, function (err, dirs, files) {

    res.status(200).json({ "output": "Deleted" });
  });





});


router.post('/delProductPublishFolder', (req, res) => {


  rmdir('./dist/assets/productpublish/' + req.body.foldername, function (err, dirs, files) {
    console.log("Deleted");
    res.status(200).json({ "output": "Deleted" });
  });





});

router.post('/delProductTmpFolder', (req, res) => {


  rmdir('./dist/assets/producttmp/' + req.body.foldername, function (err, dirs, files) {
    console.log("Deleted");
    res.status(200).json({ "output": "Deleted" });
  });





});


router.post('/tempApprove', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            obj[0].PageList[i].review = true;

            jsonfile.writeFile(mainFile, obj, function (err) {
              res.status(200).json({ "output": "pass" })
            })

          }
        }
      }
    }
  })


});

router.post('/readTempApproveStatus', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            if (obj[0].PageList[i].review == true) {
              res.status(200).json({ "review": true })
            } else {
              res.status(200).json({ "review": false })
            }



          }
        }
      }
    }
  })


});

router.post('/prodApprove', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            if (req.body.PageReview == "Approved") {
              obj[0].PageList[i].review = true;
              obj[0].PageList[i].rejected = false;
              obj[0].PageList[i].approveBy = req.body.ApproveBy;
            } else if (req.body.PageReview == "Rejected") {
              obj[0].PageList[i].review = false;
              obj[0].PageList[i].rejected = true;
              obj[0].PageList[i].approveBy = '';
            }
            jsonfile.writeFile(mainFile, obj, function (err) {
              if (err) {
                res.status(200).json({ "output": "fail" })
              } else {
                res.status(200).json({ "output": "pass" })
              }
            })
          }
        }
      }
    }
  })


});



//tmptopublishtemplate

router.post('/copyTemplate', (req, res) => {

  pathExists('./dist/assets/publish/' + req.body[0].ProductId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body[0].PageContent);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/productpublish/' + req.body[0].ProductId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/productpublish/' + req.body[0].ProductId + '/' + req.body[0].ProductId + '.json';
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});

router.post('/reviewComments', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body.PId + '/' + req.body.PName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      obj[0].Reviews.push(req.body);

      jsonfile.writeFile(mainFile, obj, function (err) {
        res.status(200).json({ "output": "pass" })
      })
    }
  })
});
router.post('/getComments', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body.PId + '/' + req.body.PName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {
    var result = [];
    if (obj) {

      for (var i = obj[0].Reviews.length - 1; i >= 0; i--) {
        if (obj[0].Reviews[i].PageName == req.body.PageName) {
          result.push({
            "User": obj[0].Reviews[i].User,
            "Review": obj[0].Reviews[i].Review,
            "CommentDate": obj[0].Reviews[i].CommentDate
          });
        }
      }

      if (err) return console.dir(err);
      res.status(200).send(result).end();

    }
  });
});
router.post('/Approve', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body.TemplateId + '/' + req.body.TemplateName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            obj[0].PageList[i].review = req.body.PageReview;
            console.log(req.body);
            jsonfile.writeFile(mainFile, obj, function (err) {
              res.status(200).json({ "output": "pass" })
            })

          }
        }
      }
    }
  })

});
router.post('/getTemplateReview', (req, res) => {

  var mainFile = './dist/assets/tmp/' + req.body.TId + '/' + req.body.TName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {
    var result = [];
    if (obj) {

      for (var i = 0; i < obj[0].PageList.length; i++) {

        result.push({
          "PageName": obj[0].PageList[i].name,
          "PageReview": obj[0].PageList[i].review
        });

      }
      //console.log(obj[0]);
      if (err) return console.dir(err);
      res.status(200).send(result).end();

    }
  });
});

router.post('/getProductReview', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.TId + '/' + req.body.TId + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {
    var result = [];
    if (obj) {

      for (var i = 0; i < obj[0].PageList.length; i++) {

        result.push({
          "ProductName": obj[0].PageList[i].name,
          "ProductReview": obj[0].PageList[i].review
        });

      }
      console.log(result);
      if (err) return console.dir(err);
      res.status(200).send(result).end();

    }
  });
});

router.post('/readProdApproveStatus', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            if (obj[0].PageList[i].review == true) {
              res.status(200).json({ "review": true })
            } else {
              res.status(200).json({ "review": false })
            }



          }
        }
      }
    }
  })


});

router.post('/firstBlockStatus', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        var available = false;
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            available = true;
            res.status(200).json({ "pageAvailable": true })



          }
        }
        if (!available) {
          res.status(200).json({ "pageAvailable": false });
        }
      } else {
        res.status(200).json({ "pageAvailable": false });
      }
    }
  })


});

router.post('/readPasswordStatus', (req, res) => {

  var mainFile = './dist/assets/data/user.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {

      for (var i = 0; i < obj.length; i++) {
        if (obj[i].username == req.body.username) {
          obj[i].password = req.body.password;
          obj[i].statusFlag = req.body.statusFlag;
          break;
        }
      }
      jsonfile.writeFile(mainFile, obj, function (err) {
        res.status(200).json({ "status": "success" });
      })
    }
  })


});


router.post('/addProductType', (req, res) => {

  var mainFile = './dist/assets/data/prodType.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      var availablity = false;
      if (obj.productType.length > 0) {
        for (var i = 0; i < obj.productType.length; i++) {

          if (obj.productType[i] == req.body.productType) {
            availablity = true;


          } else {
            availablity = false;
          }
        }
        if (!availablity) {
          obj.productType.push(req.body.productType);
          jsonfile.writeFile(mainFile, obj, function (err) {


            res.status(200).json({ "output": "pass", "data": obj });

          })
        } else {
          res.status(200).json({ "output": "fail" })
        }
      }
    }
  })

});
router.post('/ProductComments', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      obj[0].Reviews.push(req.body);

      jsonfile.writeFile(mainFile, obj, function (err) {
        res.status(200).json({ "output": "pass" })
      })
    }
  })
});
router.post('/getPComments', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {
    var result = [];
    if (obj) {

      for (var i = obj[0].Reviews.length - 1; i >= 0; i--) {
        if (obj[0].Reviews[i].PageName == req.body.PageName) {
          result.push({
            "User": obj[0].Reviews[i].User,
            "Review": obj[0].Reviews[i].Review,
            "CommentDate": obj[0].Reviews[i].CommentDate
          });
        }
      }

      if (err) return console.dir(err);
      res.status(200).send(result).end();

    }
  });
});
router.post('/ProductApprove', (req, res) => {

  var mainFile = './dist/assets/producttmp/' + req.body.ProductId + '/' + req.body.ProductName + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      if (obj[0].PageList.length > 0) {
        for (var i = 0; i < obj[0].PageList.length; i++) {

          if (obj[0].PageList[i].name == req.body.PageName) {
            obj[0].PageList[i].review = req.body.PageReview;
            console.log(req.body);
            jsonfile.writeFile(mainFile, obj, function (err) {
              res.status(200).json({ "output": "pass" })
            })

          }
        }
      }
    }
  })

});

router.post('/dataEntryMonitor', (req, res) => {
  debugger;


  var mainFile = './dist/assets/producttmp/' + req.body.folderId + '/' + req.body.folderId + '.json';
  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {
      var ok = false;
      if (obj[0].DataMonitor.length > 0) {
        for (var i = 0; i < obj[0].DataMonitor.length; i++) {

          if (obj[0].DataMonitor[i].name == req.body.data.PageName) {
            obj[0].DataMonitor[i].data = req.body.data;
            ok = true;
            jsonfile.writeFile(mainFile, obj, function (err) {
              console.log(1);
              res.status(200).json({ "status": "done" });

            })
            break;
          }

        }
        if (!ok) {
          console.log(2);
          obj[0].DataMonitor.push({ "name": req.body.data.PageName, "data": req.body.data });
          jsonfile.writeFile(mainFile, obj, function (err) {
            res.status(200).json({ "status": "done" });
          })
        }
      } else {
        console.log(3);
        obj[0].DataMonitor.push({ "name": req.body.data.PageName, "data": req.body.data });
        jsonfile.writeFile(mainFile, obj, function (err) {
          res.status(200).json({ "status": "done" });
        })
      }


      //res.status(200).json(obj);
    } else {
      //	res.status(200).json(({"output":"notok"}));
    }
  })

});

router.post('/dataEntryMonitorStatus', (req, res) => {
  debugger;

  if (!req.body.publishStatus) {
    var mainFile = './dist/assets/producttmp/' + req.body.folderId + '/' + req.body.folderId + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

      if (obj) {
        res.status(200).json(obj);

      }
    });
  } else if (req.body.publishStatus) {
    var mainFile = './dist/assets/productpublish/' + req.body.folderId + '/' + req.body.folderId + '.json';
    jsonfile.readFile(mainFile, function (err, obj) {

      if (obj) {
        res.status(200).json(obj);

      }
    });
  }
});


//oracle function
router.post('/getTableName', (req, res) => { oracle.getTableName(req, res); });
router.post('/fetchTableRows', (req, res) => { oracle.fetchTableRows(req, res); });
router.post('/saveTableRows', (req, res) => { oracle.saveTableRows(req, res); });
router.post('/DeleteTableRow', (req, res) => { oracle.DeleteTableRow(req, res); });

router.post('/AddnewLookup', (req, res) => {

  console.log('API calling..' + req.body.lookupName);
  //res.status(200).json({"status":"success","data new":req.body});
  //racle.getTableName(req,res);
  try {
    pathExists('./dist/assets/data/lookup.json').then(exists => {

      var file = './dist/assets/data/lookup.json';

      jsonfile.readFile(file, function (err, obj) {
        //console.log(obj);
        console.log("----------------------------");
        var data = JSON.parse(JSON.stringify(obj));
        //console.log('json: '+ data.lookupList[0].lookupTableColumn[0].COLUMN_NAME)
        data.lookupList.push(req.body);
        //console.log(data);
        jsonfile.writeFile(file, data, function (err) {
          console.log("---------------write Json-------------");
          console.log(data);
          res.status(200).json({ "status": "success", "message": "Added Successfully!!" });

        })
      })

    })

  } catch (error) {
    console.log(error);
  }

});


//edit existing data
router.post('/EditLookup', (req, res) => {
debugger;
  console.log('API calling..' + req.body.lookupName);
  console.log('lookupID API calling..' + req.body.lookupID);
  //res.status(200).json({"status":"success","data new":req.body});
  //racle.getTableName(req,res);
  try {
    pathExists('./dist/assets/data/lookup.json').then(exists => {

      var file = './dist/assets/data/lookup.json';

      jsonfile.readFile(file, function (err, obj) {
        //console.log(obj);
        console.log("----------------------------");
        var data = JSON.parse(JSON.stringify(obj));
        console.log('json: ' + data.lookupList[0].lookupID)
        //data.lookupList.push(req.body);
        //console.log(data);
        var index = '';
        for (var i = 0; i < data.lookupList.length; i++) {
          console.log('json: ' + i + '----' + data.lookupList[i].lookupID)
          if (req.body.lookupID == data.lookupList[i].lookupID) {
            index = i;
          }
        }
        console.log('have to edit item with index: ' + index);

        //assign edited data
        data.lookupList[index] = req.body;
        //write into file
        jsonfile.writeFile(file, data, function (err) {
          console.log("---------------Edit Json-------------");
          console.log(data);
          res.status(200).json({ "status": "success", "message": "Edited Successfully!!" });

        })

      })

    })

  } catch (error) {
    console.log(error);
  }

});






/*updateUser
router.get('/checkdb',(req,res)=>{
console.log(req); 

var db = new JsonDB("./dist/assets/data/user.json", true, false);
var data = db.getData("./dist/assets/data/user.json/");
console.log(data);
res.status(200).json(data);
})

*/



//The second argument is used to tell the DB to save after each push 
//If you put false, you'll have to call the save() method. 
//The third argument is to ask JsonDB to save the database in an human readable format. (default false) 



router.post('/exportexceltemplate', (req, res) => {

  var jsonArr = [];
  jsonArr = req.body.data;
  debugger;
  var refId = ++dataDownloadCount;
  var xls = json2xls(jsonArr);
  var mainFile = './dist/assets/dataMonitor/' + req.body.mainDetails.ProductName + '_' + refId + '.json';

  jsonfile.writeFile(mainFile, req.body.mainDetails, function (err) {

  })
  fs.writeFileSync('./dist/assets/dataMonitor/' + req.body.mainDetails.ProductName + '_' + refId + '.xlsx', xls, 'binary');

  res.status(200).json({
    "refId": req.body.mainDetails.ProductName + '_' + refId
  });


});

router.post('/readexceldata', (req, res) => {
  debugger;
  var mainFile = './dist/assets/dataMonitor/' + req.body.fileName.split(".")[0] + '.json';
  var mainJsonFile = './dist/assets/dataUpload/' + req.body.fileName.split(".")[0] + '.json';


  jsonfile.readFile(mainFile, function (err, obj) {

    if (obj) {


      const result = excelToJson({
        sourceFile: './dist/assets/dataUpload/' + req.body.fileName,
        columnToKey: {
          '*': '{{columnHeader}}',

        }
      })

      console.log("as");
      console.log(result);
      var valid = true;
      var theadName = '';
      for (var i = 0; i < result["Sheet 1"].length; i++) {
        if (i > 0) {
          console.log("erer")
          for (var j = 0; j < Object.keys(result["Sheet 1"][i]).length; j++) {
            for (var k = 0; k < obj.data[0].tableRow.length; k++) {
              if (Object.keys(result["Sheet 1"][i])[j] == obj.data[0].tableRow[k].theadName) {
                if (obj.data[0].tableRow[k].tdObj) {

                  if (obj.data[0].tableRow[k].tdObj[0].tagName == 'number' && Object.values(result["Sheet 1"][i])[j] != '') {
                    if (obj.data[0].tableRow[k].tdObj[0].tagName != (typeof Object.values(result["Sheet 1"][i])[j])) {
                      theadName = obj.data[0].tableRow[k].theadName;
                      valid = false;
                    }
                  } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'date' && Object.values(result["Sheet 1"][i])[j] != '') {
                    if (!moment(Object.values(result["Sheet 1"][i])[j], 'MM/DD/YYYY', true).isValid()) {
                      theadName = obj.data[0].tableRow[k].theadName;
                      console.log(2)
                      valid = false;
                    }
                  } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'text' && Object.values(result["Sheet 1"][i])[j] != '') {
                    if (obj.data[0].tableRow[k].maxLength != '') {
                      if (Number(obj.data[0].tableRow[k].maxLength) < Object.values(result["Sheet 1"][i])[j].toString().length) {
                        theadName = obj.data[0].tableRow[k].theadName;
                        console.log(3)
                        valid = false;
                      }
                    }
                  }

                }
              }
              if (!valid) {
                console.log("test")
                console.log(theadName)
                break;
              }
            }
            //Insert excel data to Json
            if (!valid) {

              break;
            }
          }

        }
      }
      if (!valid) {
        var store = {
          "data": obj,
          "err": theadName,
          "row": i,
          'count': j,
          'Length': obj.data[0].tableRow.length,
          'user': req.body.user,
          'udate': req.body.udate
        }
        jsonfile.writeFile(mainJsonFile, store, function (err) {

        })
        res.status(200).json({ 'fName': 'error' });
      }
      if (valid) {
        debugger;

        var pageArray = [];
        var accordId = obj.accordionName;
        var mainFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'

        jsonfile.readFile(mainFile, function (err, jsarray) {
          if (jsarray) {
            var jsarr = JSON.stringify(jsarray);
            var jsonarray = JSON.parse(jsarr);
            // var pageArray = jsonarray[0].PageContent;


            var pageArray = req.body.pageData;
            var contentArray = obj.data[0].tableRow;
            jsonarray[0].PageContent = changeAcc(pageArray, contentArray, result, '',req.body.snArr,req.body.data.accordId);
            //console.log("jarray" + jsonarray);
            var postFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'


            jsonfile.writeFile(postFile, jsonarray, function (err) {
              console.log("done")

              {
                var store = {
                  "data": obj,
                  "err": "",
                  "row": i,
                  'count': j,
                  'Length': obj.data[0].tableRow.length,
                  'user': req.body.user,
                  'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function (err) {

                });

              }
              res.status(200).json({ 'fName': 'success', 'data': jsonarray[0].PageContent });
            })
          }
        });

        /*

        jsonfile.readFile(mainFile, function(err, jsarray) {
        if (jsarray) {
        var jsarr = JSON.stringify(jsarray);
        var jsonarray = JSON.parse(jsarr);
        var pageArray = jsonarray[0].PageContent;
        var contentArray = obj.data[0].tableRow;
        jsonarray[0].PageContent = changeAcc(pageArray, contentArray, result, '');
        console.log("jarray" + jsonarray);
        var postFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'


        jsonfile.writeFile(postFile, jsonarray, function(err) {
        console.log("done")

        {
        var store = {
        "data": obj,
        "err": "",
        "row": i,
        'count': j,
        'Length': obj.data[0].tableRow.length,
        'user': req.body.user,
        'udate': req.body.udate
        }
        jsonfile.writeFile(mainJsonFile, store, function(err) {

        });

        }
        res.status(200).json('fName :' + req.body.fileName);
        })
        }
        if (err) {
        console.log(err);
        }

        });

        */

      }
    }
  });




});



/*
router.post('/readexceldata', (req, res) => {
debugger;
var mainFile = './dist/assets/dataMonitor/' + req.body.fileName.split(".")[0] + '.json';
var mainJsonFile = './dist/assets/dataUpload/' + req.body.fileName.split(".")[0] + '.json';


jsonfile.readFile(mainFile, function(err, obj) {

if (obj) {


const result = excelToJson({
sourceFile: './dist/assets/dataUpload/' + req.body.fileName,
columnToKey: {
'*': '{{columnHeader}}',

}
})

console.log("as");
console.log(result);
var valid = true;
var theadName = '';
for (var i = 0; i < result["Sheet 1"].length; i++) {
    if (i > 0) {
    console.log("erer")
    for (var j = 0; j < Object.keys(result["Sheet 1"][i]).length; j++) {
        for (var k = 0; k < obj.data[0].tableRow.length; k++) {
            if (Object.keys(result["Sheet 1"][i])[j] == obj.data[0].tableRow[k].theadName) {
            if (obj.data[0].tableRow[k].tdObj) {

            if (obj.data[0].tableRow[k].tdObj[0].tagName == 'number' && Object.values(result["Sheet 1"][i])[j] != '') {
            if (obj.data[0].tableRow[k].tdObj[0].tagName != (typeof Object.values(result["Sheet 1"][i])[j])) {
            theadName = obj.data[0].tableRow[k].theadName;
            valid = false;
            }
            } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'date' && Object.values(result["Sheet 1"][i])[j] != '') {
            if (!moment(Object.values(result["Sheet 1"][i])[j], 'MM/DD/YYYY', true).isValid()) {
            theadName = obj.data[0].tableRow[k].theadName;
            console.log(2)
            valid = false;
            }
            } else if (obj.data[0].tableRow[k].tdObj[0].tagName == 'text' && Object.values(result["Sheet 1"][i])[j] != '') {
            if (obj.data[0].tableRow[k].maxLength != '') {
            if (Number(obj.data[0].tableRow[k].maxLength) < Object.values(result["Sheet 1"][i])[j].toString().length) {
                theadName = obj.data[0].tableRow[k].theadName;
                console.log(3)
                valid = false;
                }
                }
                }

                }
                }
                if (!valid) {
                console.log("test")
                console.log(theadName)
                break;
                }
                }
                //Insert excel data to Json
                if (!valid) {

                break;
                }
                }

                }
                }
                if (!valid) {
                var store = {
                "data": obj,
                "err": theadName,
                "row": i,
                'count': j,
                'Length': obj.data[0].tableRow.length,
                'user': req.body.user,
                'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function(err) {

                })
                res.status(200).json("fName:error");

                }
                if (valid) {
                debugger;

                var pageArray = [];
                var accordId = obj.accordionName;
                var mainFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'

                jsonfile.readFile(mainFile, function(err, jsarray) {
                if (jsarray) {
                var jsarr = JSON.stringify(jsarray);
                var jsonarray = JSON.parse(jsarr);
                var pageArray = jsonarray[0].PageContent;
                var contentArray = obj.data[0].tableRow;
                jsonarray[0].PageContent = changeAcc(pageArray, contentArray, result, '');
                console.log("jarray" + jsonarray);
                var postFile = './dist/assets/producttmp/' + obj.ProductName + '/' + obj.PageName + '.json'
                jsonfile.writeFile(postFile, jsonarray, function(err) {
                console.log("done")

                {
                var store = {
                "data": obj,
                "err": "",
                "row": i,
                'count': j,
                'Length': obj.data[0].tableRow.length,
                'user': req.body.user,
                'udate': req.body.udate
                }
                jsonfile.writeFile(mainJsonFile, store, function(err) {

                });

                }
                res.status(200).json('fName :success');
                })
                }
                if (err) {
                console.log(err);
                }

                });



                }
                }
                });




                });
                */

router.post('/readDataUploadFiles', (req, res) => {

  debugger;
  readDirFiles.read('./dist/assets/dataUpload/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    var fileName = Object.keys(files);
    var jsonArr = [];

    var count = 0;
    var readCount = 0;
    for (var i = 0; i < Object.keys(files).length; i++) {
      if (Object.keys(files)[i].includes('.json') && Object.keys(files)[i].includes(req.body.productName)) {
        var output = JSON.parse(files[Object.keys(files)[i]]);
        var details = {
          "refid": Object.keys(files)[i].split("_")[1].split(".")[0],
          "ProductName": req.body.productName,
          "PageName": output.data.PageName,
          "blockId": output.data.blockId,
          "accordionName": output.data.accordionName,
          "err": output.err,
          "row": output.row,
          "count": output.Length,
          'user': output.user,
          'udate': output.udate
        }
        jsonArr.push(details);


      }

    }
    res.status(200).json(jsonArr);

  });




});
router.post('/readdataDownloadJsonFiles', (req, res) => {

  debugger;
  readDirFiles.read('./dist/assets/dataMonitor/', 'UTF-8', function (err, files) {
    if (err) return console.dir(err);
    var fileName = Object.keys(files);
    var jsonArr = [];

    var count = 0;
    var readCount = 0;
    for (var j = 0; j < Object.keys(files).length; j++) {
      if (Object.keys(files)[j].includes('.json') && Object.keys(files)[j].includes(req.body.productName)) {
        var output = JSON.parse(files[Object.keys(files)[j]]);
        var details = {
          "refid": Object.keys(files)[j].split("_")[1].split(".")[0],
          "ProductName": req.body.productName,
          "PageName": output.PageName,
          "blockId": output.blockId,
          "accordionName": output.accordionName,
          "err": output.err,
          "row": output.row,
          "count": output.Length,
          "user": output.user,
          "date": output.date,
          "mode": output.mode,
          "recipients": output.recipients
        }
        jsonArr.push(details);




      }

    }
    res.status(200).json(jsonArr);

  });
});




function changeAcc(pageArrayOld, eData, eveValue, flag,snArr,accordId) {
    debugger;
      var check = JSON.stringify(pageArrayOld);

    var check1 = JSON.parse(check);
     eveValue["Sheet 1"].splice(0, 1);
    for (var a = 0; a < check1.length; a++) {
	
        if (check1[a].blockId == eData[0].blockId && check1[a].accordId==accordId) {
            if (check1[a].tableObj) {
                //table
                var tableData = check1[a].tableObj[0].tableRow;
                check1[a].tableObj = [];
                for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                    check1[a].tableObj.push({
                        "sna": test,
                        "tableRow": tableData
                    });


                }
                var s = JSON.stringify(check1);
                var s1 = JSON.parse(s);

                for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                  
					
                    for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
						for (var tj = 0; tj < s1[a].tableObj[xcx].tableRow.length; tj++) 
					{

                        if (s1[a].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                            if (s1[a].tableObj[xcx].tableRow[tj].optionObj) {

                                for (var nj = 0; nj < s1[a].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                    if (s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                        s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                    } else {
                                        s1[a].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                    }
                                }



                            } else {
                                if (s1[a].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {

                                    /*
                                                                        var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw].setDate(Object.values(eveValue["Sheet 1"][xcx])[bw].getDate() + 1)).toISOString().substr(0, 10);
                                    									*/
                                    var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                    var applyDate = new Date(givenDate);
                                    applyDate.setDate(applyDate.getDate() + 1)
                                    s1[a].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                    //s1[a].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                } else {
                                    s1[a].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                }
                            }


                        }
                    
					}}
                }


            }




        } else {
            if (check1[a].field) {
                for (var g = 0; g < check1[a].field.length; g++) {
                    if (check1[a].field[g].subPage) {
                        for (var za = 0; za < check1[a].field[g].subPage.length; za++) {
                            if (check1[a].field[g].subPage[za].blockId == eData[0].blockId && check1[a].field[g].subPage[za].accordId==accordId) {

                                //added  field-table
                                if (check1[a].field[g].subPage[za].tableObj) {
                                    var tableData = check1[a].field[g].subPage[za].tableObj[0].tableRow;
                                    check1[a].field[g].subPage[za].tableObj = [];
                                    for (var test = 0; test < eveValue["Sheet 1"].length; test++) {


                                        check1[a].field[g].subPage[za].tableObj.push({
                                            "sna": test,
                                            "tableRow": tableData
                                        });


                                    }
                                    var s = JSON.stringify(check1);
                                    var s1 = JSON.parse(s);

                                    for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                        console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                        for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
for (var tj = 0; tj < s1[a].field[g].subPage[za].tableObj[xcx].tableRow.length; tj++) {
                                            if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj) {

                                                    for (var nj = 0; nj < s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                        if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                            s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                        } else {
                                                            s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                        }
                                                    }



                                                } else {
                                                    if (s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                        var givenDate = Object.values(eveValue["Sheet 1"][xcx])[bw];

                                                        var applyDate = new Date(givenDate);
                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                        s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);



                                                    } else {
                                                        s1[a].field[g].subPage[za].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                    }
                                                }


                                            }
                                        }
                                    }
									}

                                }



                            } else {
                                if (check1[a].field[g].subPage[za].field) {
                                    for (var h = 0; h < check1[a].field[g].subPage[za].field.length; h++) {
                                        if (check1[a].field[g].subPage[za].field[h].subPage) {
                                            for (var zc = 0; zc < check1[a].field[g].subPage[za].field[h].subPage.length; zc++) {
                                                if (check1[a].field[g].subPage[za].field[h].subPage[zc].blockId == eData[0].blockId && check1[a].field[g].subPage[za].field[h].subPage[zc].accordId==accordId) {
                                                    //added  field-subpage-field

                                                    if (check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj) {
                                                        var tableData = check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[0].tableRow;
                                                        check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj = [];
                                                        for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                            check1[a].field[g].subPage[za].field[h].subPage[zc].tableObj.push({
                                                                "sna": test,
                                                                "tableRow": tableData
                                                            });


                                                        }
                                                        var s = JSON.stringify(check1);
                                                        var s1 = JSON.parse(s);

                                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                            console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
 for (var tj = 0; tj < s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow.length; tj++) {
                                                                if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                    if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj) {

                                                                        for (var nj = 0; nj < s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                            if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                            } else {
                                                                                s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                            }
                                                                        }



                                                                    } else {
                                                                        if (s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                            var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                            var applyDate = new Date(givenDate);
                                                                            applyDate.setDate(applyDate.getDate() + 1)

                                                                            s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                        } else {
                                                                            s1[a].field[g].subPage[za].field[h].subPage[zc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                        }
                                                                    }


                                                                }
                                                            }
                                                        }}


                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else if (check1[a].field[g].subPage[za].tableObj) {
                                    for (var i = 0; i < check1[a].field[g].subPage[za].tableObj.length; i++) {
									if(check1[a].field[g].subPage[za].tableObj[i].snb == snArr[1].snb)
                                        for (var j = 0; j < check1[a].field[g].subPage[za].tableObj[i].tableRow.length; j++) {
                                            if (check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage) {
                                                for (var zd = 0; zd < check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage.length; zd++) {
                                                    if (check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].blockId == eData[0].blockId && check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].accordId==accordId) {
                                                        //added field-subpage-table-subpage-table

                                                        if (check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj) {
                                                            var tableData = check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[0].tableRow;
                                                            check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj = [];
                                                            for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                //check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj.push(tableData)
                                                                check1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj.push({
                                                                    "snc": test,
                                                                    "tableRow": tableData
                                                                });


                                                            }
                                                            var s = JSON.stringify(check1);
                                                            var s1 = JSON.parse(s);

                                                            for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
 for (var tj = 0; tj < s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow.length; tj++) {

                                                                    if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                        if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj) {

                                                                            for (var nj = 0; nj < s1[a].field[g].subPage[za].tableObj[i].tableRow[t].subPage[zd].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                    s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                } else {
                                                                                    s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                }
                                                                            }



                                                                        } else {
                                                                            if (s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                var applyDate = new Date(givenDate);
                                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                                                s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                            } else {
                                                                                s1[a].field[g].subPage[za].tableObj[i].tableRow[j].subPage[zd].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                            }
                                                                        }


                                                                    }
                                                                }
                                                            }
															}

                                                        }

                                                    }
                                                }
                                            }
                                          }
                                        }
                                    }
                                }
                            }
                        }
                    }
                
				

            } else if (check1[a].tableObj) {

                for (var b = 0; b < check1[a].tableObj.length; b++) {
				 if (check1[a].tableObj[b].sna == snArr[0].sna) {
                    for (var c = 0; c < check1[a].tableObj[b].tableRow.length; c++) {
                        if (check1[a].tableObj[b].tableRow[c].subPage) {
                            for (var zb = 0; zb < check1[a].tableObj[b].tableRow[c].subPage.length; zb++) {
                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].accordId==accordId) {
                                    //added table-subpage-table
                                       
                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj) {
console.log('chk3')

                                        var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[0].tableRow;
                                        check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj = [];
                                        for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                            check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj.push({"snb": test,
                                            "tableRow": tableData});
console.log(check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj)



                                        }
                                        var s = JSON.stringify(check1);
                                        var s1 = JSON.parse(s);

                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                               

                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow.length; tj++) {
                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj) {

                                                        for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                            } else {
                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                            }
                                                        }



                                                    } else {
                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                            var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                            var applyDate = new Date(givenDate);
                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);

                                                            //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                        } else {
                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                        }
                                                    }


                                                }
                                            }}
                                        }
										}

                                    




                                } else {
                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field) {
                                        for (var d = 0; d < check1[a].tableObj[b].tableRow[c].subPage[zb].field.length; d++) {
                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage) {
                                                for (var ze = 0; ze < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage.length; ze++) {
                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].accordId==accordId) {

                                                        //added table-subpage field
                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj) {
                                                            var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[0].tableRow;
                                                            check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj = [];
                                                            for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.push(tableData)
                                                                check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.push({
                                                                    "sna": test,
                                                                    "tableRow": tableData
                                                                });


                                                            }
                                                            var s = JSON.stringify(check1);
                                                            var s1 = JSON.parse(s);

                                                            for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
   for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow.length; tj++) {

                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj) {

                                                                            for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                } else {
                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                }
                                                                            }



                                                                        } else {
                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                var applyDate = new Date(givenDate);
                                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);



                                                                            } else {
                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                            }
                                                                        }


                                                                    }
                                                                }
                                                            }
															}

                                                        }



                                                    } else {
                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj) {

                                                            for (var e = 0; e < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj.length; e++) {
															if ( check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].snc == snArr[2].snc) {
                                                                for (var f = 0; f < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow.length; f++) {
                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage) {
                                                                        //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow
                                                                        for (var zf = 0; zf < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage.length; zf++) {
                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].accordId==accordId) {
                    
                                                                             console.log('here');
                                                                                //added table-subpage-field-table
                                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj) {
                                                                                    var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[0].tableRow;
                                                                                    check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj = [];
                                                                                    for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                                        //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.push(tableData)
                                                                                        check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.push({
                                                                                            "snd": test,
                                                                                            "tableRow": tableData
                                                                                        });



                                                                                    }
                                                                                    var s = JSON.stringify(check1);
                                                                                    var s1 = JSON.parse(s);

                                                                                    for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                                                        console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                                        for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
 for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow.length; tj++) {
                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                    for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                        } else {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                        }
                                                                                                    }



                                                                                                } else {
                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                        var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                        var applyDate = new Date(givenDate);
                                                                                                        applyDate.setDate(applyDate.getDate() + 1)
                                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                        //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                    } else {
                                                                                                        console.log("comg")
                                                                                                        console.log(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                    }
                                                                                                }


                                                                                            }
                                                                                        }
                                                                                    }}


                                                                                }



                                                                            }
                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj) {

                                                                                for (var ef = 0; ef < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj.length; ef++) {
																				if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].snd == snArr[3].snd) {
                                                                                    for (var ff = 0; ff < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow.length; ff++) {
                                                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage) {
                                                                                            //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow[13].subPage[0].tableObj[0].tableRow
                                                                                            for (var zff = 0; zff < check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage.length; zff++) {
                                                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].accordId==accordId) {
                                                                                                 
																								  

                                                                                                    //added table-subpage-field-subpage-table-subpage-table
                                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj) {
                                                                                                        var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[0].tableRow;
                                                                                                        check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[e].tableRow[f].subPage[zff].tableObj = [];
                                                                                                        for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                                                            //check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj.push(tableData)
                                                                                                            check1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj.push({
                                                                                                                "sne": test,
                                                                                                                "tableRow": tableData
                                                                                                            });



                                                                                                        }
                                                                                                        var s = JSON.stringify(check1);
                                                                                                        var s1 = JSON.parse(s);

                                                                                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                                                                            console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
  for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow.length; tj++) {
                                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                    console.log("match");
                                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                                        for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                                            } else {
                                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                                            }
                                                                                                                        }



                                                                                                                    } else {
                                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                                            var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                                            var applyDate = new Date(givenDate);
                                                                                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                                            //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                        } else {

                                                                                                                            console.log(Object.values(eveValue["Sheet 1"][xcx])[bw]);
                                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].field[d].subPage[ze].tableObj[e].tableRow[f].subPage[zf].tableObj[ef].tableRow[ff].subPage[zff].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                                        }
                                                                                                                    }


                                                                                                                }
                                                                                                            }
                                                                                                        }

																										}
                                                                                                    }



                                                                                                }




                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
}

                                                                            }




                                                                        }
                                                                    }
                                                                }
                                                            }
}

                                                        }

                                                    }


                                                }
                                            }
                                        }




                                    } else if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj) {

                                        for (var e = 0; e < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj.length; e++) {
										  if ( check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].snb == snArr[1].snb) {
                                            for (var f = 0; f < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow.length; f++) {
                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage) {
                                                    for (var zf = 0; zf < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage.length; zf++) {
                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].accordId==accordId) {

                                                            //added table-subpage-table
                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj) {
                                                                var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[0].tableRow;
                                                                check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj = [];
                                                                for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                    // check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.push(tableData)
                                                                    check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.push({
                                                                        "snc": test,
                                                                        "tableRow": tableData
                                                                    });


                                                                }
                                                                var s = JSON.stringify(check1);
                                                                var s1 = JSON.parse(s);

                                                                for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                    console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                    for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
  for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow.length; tj++) {

                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                    } else {
                                                                                        s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                    }
                                                                                }



                                                                            } else {
                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                    var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                    var applyDate = new Date(givenDate);
                                                                                    applyDate.setDate(applyDate.getDate() + 1)
                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                    //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                } else {
                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                }
                                                                            }


                                                                        }
                                                                    }
                                                                }
																}

                                                            }



                                                        } else {
                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field) {
                                                                for (var vd = 0; vd < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field.length; vd++) {
                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage) {


                                                                        for (var ve = 0; ve < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage.length; ve++) {
                                                                            if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj) {
                                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].accordId==accordId) {
                                                                                    //added tavle-subpage-table-subpage-field
                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj) {
                                                                                        var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[0].tableRow;
                                                                                        check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj = [];
                                                                                        for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                                            //check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.push(tableData)
                                                                                            check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj.push({
                                                                                                "sna": test,
                                                                                                "tableRow": tableData
                                                                                            });

                                                                                        }
                                                                                        var s = JSON.stringify(check1);
                                                                                        var s1 = JSON.parse(s);

                                                                                        for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {
                                                                                            console.log(Object.keys(eveValue["Sheet 1"][xcx]))
                                                                                            for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
 for (var tj = 0; tj < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow.length; tj++) {
                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                        for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                            } else {
                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                            }
                                                                                                        }



                                                                                                    } else {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                            var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                            var applyDate = new Date(givenDate);
                                                                                                            applyDate.setDate(applyDate.getDate() + 1)
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                            //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                        } else {
                                                                                                            s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].field[vd].subPage[ve].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                        }
                                                                                                    }


                                                                                                }
                                                                                            }
                                                                                        }

																						}
                                                                                    }

                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj) {

                                                                for (var va = 0; va < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj.length; va++) {
																   if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].snc == snArr[2].snc) {
                                                                    for (var vb = 0; vb < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow.length; vb++) {
                                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage) {
                                                                            for (var vc = 0; vc < check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage.length; vc++) {
                                                                                if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj) {
                                                                                    if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].blockId == eData[0].blockId && check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].accordId==accordId) {

                                                                                        //added table-subpage-table-subpage-table

                                                                                        if (check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj) {
                                                                                            var tableData = check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[0].tableRow;
                                                                                            check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj = [];
                                                                                            for (var test = 0; test < eveValue["Sheet 1"].length; test++) {

                                                                                                check1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj.push({
                                                                                                    "snd": test,
                                                                                                    "tableRow": tableData
                                                                                                });



                                                                                            }
                                                                                            var s = JSON.stringify(check1);
                                                                                            var s1 = JSON.parse(s);

                                                                                            for (var xcx = 0; xcx < eveValue["Sheet 1"].length; xcx++) {

                                                                                                for (var bw = 0; bw < Object.keys(eveValue["Sheet 1"][xcx]).length; bw++) {
   for (var tj = 0; tj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow.length; tj++) {

                                                                                                    if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].theadName == Object.keys(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                        if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj) {

                                                                                                            for (var nj = 0; nj < s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj.length; nj++) {
                                                                                                                if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj[nj].optionValue == Object.values(eveValue["Sheet 1"][xcx])[bw]) {
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = true;

                                                                                                                } else {
                                                                                                                    s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].optionObj[nj].selected = false;
                                                                                                                }
                                                                                                            }



                                                                                                        } else {
                                                                                                            if (s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].tdObj[0].tagName == "date" && Object.values(eveValue["Sheet 1"][xcx])[bw] != '') {
                                                                                                                var givenDate = new Date(Object.values(eveValue["Sheet 1"][xcx])[bw]);

                                                                                                                var applyDate = new Date(givenDate);
                                                                                                                applyDate.setDate(applyDate.getDate() + 1)
                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = applyDate.toISOString().substr(0, 10);


                                                                                                                //s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[bw].tdObj[0].tdValue=Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                            } else {
                                                                                                                s1[a].tableObj[b].tableRow[c].subPage[zb].tableObj[e].tableRow[f].subPage[zf].tableObj[va].tableRow[vb].subPage[vc].tableObj[xcx].tableRow[tj].tdObj[0].tdValue = Object.values(eveValue["Sheet 1"][xcx])[bw];
                                                                                                            }
                                                                                                        }


                                                                                                    }
                                                                                                }
                                                                                            }
																							}

                                                                                        }


                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                                    }
                                                                }
																}
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                      
}									  }
                                    }
                                }
                            }
                        }
						}
                    }
                }

            }
        }
		}
   
    console.log(s1);
    return s1;

}


// CreateTemplate
router.post('/createPublishCloneTemplate', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder
  debugger;

  pathExists('./dist/assets/publish/' + req.body[0].TemplateId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/publish/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/publish/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});


// CreateTemplate
router.post('/dataTemplateClone', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder
  debugger;

  var oldTemplate = JSON.stringify(req.body[0].templateFull);
  var newTemplate = JSON.parse(oldTemplate);

  for (var z = 0; z < req.body[0].selectedPlanCode.length; z++) {
    for (var y = 0; y < Object.keys(newTemplate).length; y++) {
      if (req.body[0].selectedPlanCode[z].templateId != Object.keys(newTemplate)[y]) {

        delete newTemplate[Object.keys(newTemplate)[y]];
        --y;
      }
    }
  }


  var pageList = [];
  var output = [];
  var deloutput = [];

  for (var i = 0; i < req.body[0].selectedPlanCode.length; i++) {
    for (var j = 0; j < Object.keys(newTemplate).length; j++) {
      if (req.body[0].selectedPlanCode[i].templateId == Object.keys(newTemplate)[j]) {

        for (var k = 0; k < Object.keys(newTemplate[Object.keys(newTemplate)[j]]).length; k++) {
          for (var m = 0; m < req.body[0].selectedPages.length; m++) {
            for (var n = 0; n < req.body[0].selectedPages[m].length; n++) {

              if ((req.body[0].selectedPages[m][n].data.PageName == Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k])) {
                if (req.body[0].selectedPages[m][n].data.enableBlock) {
                  var ok = [];
                  var pageName = "";
                  output.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent);

                  for (var q = 0; q < JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent.length; q++) {

                    for (var p = 0; p < req.body[0].selectedPages[m][n].data.PageContent.length; p++) {
                      if (req.body[0].selectedPages[m][n].data.PageContent[p].checked) {

                        //console.log(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent)

                        if (req.body[0].selectedPages[m][n].data.PageContent[p].blockId == JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q].blockId) {
                          ok.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q])
                          pageName = JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageName;
                        } else {
                          var s = newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];
                          var f = JSON.parse(s);
                          // 							console.log(f[0].PageContent[q])
                          //f[0].PageContent.splice(0,q);

                        }

                      }


                    }
                  }
                  var t = [];
                  t.push({ "PageContent": ok, "PageName": pageName, "TemplateId": req.body[0].nTemplate[0].TemplateId })
                  var file = './dist/assets/publish/' + req.body[0].nTemplate[0].TemplateId + '/' + t[0].PageName;
                  jsonfile.writeFile(file, t, function (err) {
                    //console.log("temp copied");
                    //res.status(200).json(req.body);
                  })
                  //newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]]=JSON.stringify(t);



                } else {
                  deloutput.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0])
                  //delete newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];


                  //--k;

                }

              }

            }

          }
        }
      } else {

      }
      break;
    }
  }




});


router.post('/createTmpCloneTemplate', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder
  debugger;

  pathExists('./dist/assets/tmp/' + req.body[0].TemplateId).then(exists => {

    if (exists) {
      //Post Json
      //var file = './tmp/data2.json'
      var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
      jsonfile.writeFile(file, req.body, function (err) {
        res.status(200).json(req.body);
      })

    }

    else {

      //var folderName=Math.floor(Math.random()*1000)+1;

      mkdirp('./dist/assets/tmp/' + req.body[0].TemplateId, function (err) {
        if (err) {
          console.log(err);
        } else {
          //Post Json
          //var file = './tmp/data2.json'
          var file = './dist/assets/tmp/' + req.body[0].TemplateId + '/' + req.body[0].TemplateName + ".json";
          jsonfile.writeFile(file, req.body, function (err) {
            res.status(200).json(req.body);
          })

        }
      });
    }

  });
});

router.post('/dataTmpTemplateClone', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB

  //create folder
  debugger;

  var oldTemplate = JSON.stringify(req.body[0].templateFull);
  var newTemplate = JSON.parse(oldTemplate);

//   for (var z = 0; z < req.body[0].selectedPlanCode.length; z++) {
//     for (var y = 0; y < Object.keys(newTemplate).length; y++) {
//       if (req.body[0].selectedPlanCode[z].templateId != Object.keys(newTemplate)[y]) {

//         delete newTemplate[Object.keys(newTemplate)[y]];
//         --y;
//       }
//     }
//   }


  var pageList = [];
  var output = [];
  var deloutput = [];

  for (var i = 0; i < req.body[0].selectedPlanCode.length; i++) {
    for (var j = 0; j < Object.keys(newTemplate).length; j++) {
      if (req.body[0].selectedPlanCode[i].templateId == Object.keys(newTemplate)[j]) {

        for (var k = 0; k < Object.keys(newTemplate[Object.keys(newTemplate)[j]]).length; k++) {
          for (var m = 0; m < req.body[0].selectedPages.length; m++) {
            for (var n = 0; n < req.body[0].selectedPages[m].length; n++) {

              if ((req.body[0].selectedPages[m][n].data.PageName == Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k])) {
                if (req.body[0].selectedPages[m][n].data.enableBlock) {
                  var ok = [];
                  var pageName = "";
                  output.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent);

                  for (var q = 0; q < JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent.length; q++) {

                    for (var p = 0; p < req.body[0].selectedPages[m][n].data.PageContent.length; p++) {
                      if (req.body[0].selectedPages[m][n].data.PageContent[p].checked) {

                        //console.log(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent)

                        if (req.body[0].selectedPages[m][n].data.PageContent[p].blockId == JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q].blockId) {
                          ok.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageContent[q])
                          pageName = JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0].PageName;
                        } else {
                          var s = newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];
                          var f = JSON.parse(s);
                          // 							console.log(f[0].PageContent[q])
                          //f[0].PageContent.splice(0,q);

                        }

                      }


                    }
                  }
          
//                   var file = './dist/assets/tmp/' + req.body[0].nTemplate[0].TemplateId + '/' + t[0].PageName;
//                   jsonfile.writeFile(file, t, function (err) {
                  
//                   })
                  //newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]]=JSON.stringify(t);

 var t = [];
                  t.push({ "PageContent": ok, "PageName": pageName, "TemplateId": req.body[0].nTemplate[0].TemplateId })
                   var file = './dist/assets/tmp/' + req.body[0].nTemplate[0].TemplateId + '/' + t[0].PageName;
                   jsonfile.writeFile(file, t[0].PageContent, function (err) {
                  
                   })

                } else {
                  deloutput.push(JSON.parse(newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]])[0])
                  //delete newTemplate[Object.keys(newTemplate)[j]][Object.keys(newTemplate[Object.keys(newTemplate)[j]])[k]];


                  //--k;

                }

              }

            }

          }
        }
      } else {

      }
      //break;
    }
       if(i==req.body[0].selectedPlanCode.length-1){
		     var postData = { "eMailTo": req.body[0].emailData.eMailTo, "message": req.body[0].emailData.message };
              var options = {
                method: 'post',
                body: postData,
                json: true,
                url: wsUrl.web.url + "ProductBenefits/SendCloningEMail",
                headers: {

                }
              }

              request(options, function (err, res, body) {
                if (err) {
                  console.log('Error :', err)
                  return
                }
                console.log(' Body :', body)

              });
	   }
  }




});
function fnCloneAutoNullValues(fPageArray){
	try {
	for (var a = 0; a < fPageArray.length; a++) {
		if (fPageArray[a].field) {
			//sna- (field)
			for (var na = 0; na < fPageArray[a].field.length; na++) {
				if(fPageArray[a].field[na].tagName != 'button'){
					if(fPageArray[a].field[na].autoGenerated != undefined 
						&& fPageArray[a].field[na].autoGenerated){
						fPageArray[a].field[na].tagValue = '';
					}
				}else if(fPageArray[a].field[na].subPage){
					for (var zb = 0; zb < fPageArray[a].field[na].subPage.length; zb++) {
						if (fPageArray[a].field[na].subPage[zb].field) {
							//snb - (field-field)
							for(var nb=0; nb< fPageArray[a].field[na].subPage[zb].field.length;nb++){
								if(fPageArray[a].field[na].subPage[zb].field[nb].tagName != 'button'){
									if(fPageArray[a].field[na].subPage[zb].field[nb].autoGenerated != undefined 
										&& fPageArray[a].field[na].subPage[zb].field[nb].autoGenerated){
										fPageArray[a].field[na].subPage[zb].field[nb].tagValue = '';
									}
								}
							}
						} else if (fPageArray[a].field[na].subPage[zb].tableObj) {
							//snb - (field-table)
							for (var nb = 0; nb < fPageArray[a].field[na].subPage[zb].tableObj.length; nb++) {
								for (var nbr = 0; nbr < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow.length; nbr++) {
									if(fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].tdObj[0].tagName != 'button'){
										if(fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].autoGenerated != undefined 
											&& fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].autoGenerated){
											fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].tdObj[0].tdValue = '';	
										}
									}else if(fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage){
										for (var zc = 0; zc < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage.length; zc++) {
											if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].field) {
												//snc-- (Field-Table-Field)
											}else if (fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj) {
												//snc-- (Field-Table-Table)
												for (var nc = 0; nc < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj.length; nc++) {
													for (var ncr = 0; ncr < fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow.length; ncr++) {
														if(fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].tdObj[0].tagName != 'button'){
															if(fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].autoGenerated != undefined 
																&& fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].autoGenerated){
																fPageArray[a].field[na].subPage[zb].tableObj[nb].tableRow[nbr].subPage[zc].tableObj[nc].tableRow[ncr].tdObj[0].tdValue = '';	
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}else if(fPageArray[a].tableObj){
			//sna-Table
			for (var na = 0; na < fPageArray[a].tableObj.length; na++) {
				for (var nar = 0;nar < fPageArray[a].tableObj[na].tableRow.length; nar++) {
					if(fPageArray[a].tableObj[na].tableRow[nar].tdObj[0].tagName != 'button'){
						if(fPageArray[a].tableObj[na].tableRow[nar].autoGenerated != undefined 
							&& fPageArray[a].tableObj[na].tableRow[nar].autoGenerated){
							fPageArray[a].tableObj[na].tableRow[nar].tdObj[0].tdValue = '';	
						}
					}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage){
						for (var za = 0; za < fPageArray[a].tableObj[na].tableRow[nar].subPage.length; za++) {
							if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field){
								//snb-- (Table-Field)
								for (var m = 0; m < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field.length;m++){
									if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].tagName != 'button'){
										if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].autoGenerated != undefined 
											&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].autoGenerated){
											fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].tagValue = '';
										}
									}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage){
										for(var zb = 0; zb < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage.length; zb++){
											if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].field){
												//snc-- (Table-Field-Field)
											}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj){
												//snc-- (Table-Field-Table)
												for (var nc = 0; nc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj.length; nc++) {
													for (var ncr = 0; ncr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow.length; ncr++) {
														if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tagName != 'button'){
															if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated != undefined 
																&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated){
																fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tdValue = '';	
															}
														}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage){
															for (var zc=0; zc< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage.length; zc++){
																if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].field){
																	//snd-- (Table-Field-Table-Field)
																	//console.log("Snd Table-Field-Table-Field Enters");
																}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj){
																	//snd-- (Table-Field-Table-Table)
																	for (var nd=0; nd< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj.length; nd++){
																		for (var ndr=0; ndr< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow.length; ndr++){
																			if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tagName != 'button'){
																				if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated != undefined 
																					&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated){
																					fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tdValue = '';	
																				}
																			}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage){
																				for (var zd=0; zd< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage.length; zd++){
																					if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].field){
																						//sne-- (Table-Field-Table-Table-Field)
																						//console.log("Sne Table-Field-Table-Table-Field Enters");
																					}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj){
																						//sne-- (Table-Field-Table-Table-Table)
																						for (var ne=0; ne< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj.length; ne++){
																							for (var ner=0; ner< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow.length; ner++){
																								if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].tdObj[0].tagName != 'button'){
																									if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].autoGenerated != undefined 
																										&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].autoGenerated){
																										fPageArray[a].tableObj[na].tableRow[nar].subPage[za].field[m].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].subPage[zd].tableObj[ne].tableRow[ner].tdObj[0].tdValue = '';	
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}else if (fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj) {
								//snb-- (Table-Table)
								for (var nb = 0; nb < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj.length; nb++) {
									for (var nbr = 0; nbr < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow.length; nbr++) {
										if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].tdObj[0].tagName != 'button'){
											if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].autoGenerated != undefined 
												&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].autoGenerated){
												fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].tdObj[0].tdValue = '';	
											}
										}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage){
											for (var zb=0; zb< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage.length; zb++){
												if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field){
													//snc-- (Table-Table-field)
													for (var nc = 0; nc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field.length; nc++){
														if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].tagName != 'button'){
															if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].autoGenerated != undefined 
																&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].autoGenerated){
																fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].field[nc].tagValue = '';
															}
														}
													}
												}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj){
													//snc-- (Table-Table-table)
													for (var nc=0; nc< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj.length; nc++){
														for (var ncr=0; ncr< fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow.length; ncr++){
															if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tagName != 'button'){
																if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated != undefined 
																	&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].autoGenerated){
																	fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].tdObj[0].tdValue = '';	
																}
															}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage){
																for(var zc = 0; zc < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage.length; zc++){
																	if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].field){
																		//snd -- (Table-Table-Table-Field)
																	}else if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj){
																		//snd -- (Table-Table-Table-Table)
																		for(var nd=0;nd < fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj.length;nd++){
																			for(var ndr=0;ndr<fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow.length;ndr++){
																				if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tagName != 'button'){
																					if(fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated != undefined 
																						&& fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].autoGenerated){
																						fPageArray[a].tableObj[na].tableRow[nar].subPage[za].tableObj[nb].tableRow[nbr].subPage[zb].tableObj[nc].tableRow[ncr].subPage[zc].tableObj[nd].tableRow[ndr].tdObj[0].tdValue = '';
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	}catch(err){
		throw err;
	}
	return fPageArray;
}
module.exports = router;