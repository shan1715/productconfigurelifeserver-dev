// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
var bodyParser = require('body-parser');
// Get our API routes
const api = require('./server/routes/api');
const cors = require('cors');

//var oracle = require('./oracleDb/lookUpConn');
const app = express();

// Parsers for POST data
/*
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use( bodyParser.json({limit: '50mb'}) );
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit:50000
}));

*/

// var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost:27017/product_configuration_db', {useNewUrlParser: true});
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function() {
//   console.log("---connect to mongoDB---")
// });

app.use(cors());
app.use(bodyParser.json({limit: '200mb'}));
app.use(bodyParser.urlencoded({limit: '200mb', extended: true}));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api', api);


// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '4777';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));